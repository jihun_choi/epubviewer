package ybm.EpubViewer;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class UserGuide extends Activity {
	
	WebView m_WebView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    setContentView(R.layout.user_guide);
	    
	    m_WebView = (WebView) findViewById(R.id.webview); 
        m_WebView.getSettings().setJavaScriptEnabled(true);  // 웹뷰에서 자바스크립트실행가능
        m_WebView.loadUrl("file:///android_asset/usagewbook.htm");  // 구글홈페이지 지정
        m_WebView.setWebViewClient(new HelloWebViewClient());  // WebViewClient 지정          
        
	}
	
    private class HelloWebViewClient extends WebViewClient { 
        @Override 
        public boolean shouldOverrideUrlLoading(WebView view, String url) { 
            view.loadUrl(url); 
            return true; 
        } 
    }

}
