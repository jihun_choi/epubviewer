package ybm.EpubViewer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class FontManagerView
{
	static Context m_context;
	static Activity m_activity;
	//static
	
	static int nFontRate;
	
	static ProgressDialog m_openProgressDialog = null;
	static Handler m_openProgressHandler = null;
	
	
	public static void RefreshWithFontSize(int nFontSize)
	{
		Global.g_FontRate = nFontSize;
		Global.g_FontMgr.ClearCache();
		Global.g_FontMgr.UpdateFontList();
		
		float ratio = (float)Global.g_CurEpubBook.GetCurPage() / (float)Global.g_CurEpubBook.GetTotalPage();
		
		Global.g_CurEpubBook.ProcessPaging();
		
		int p = (int)(Global.g_CurEpubBook.GetTotalPage() * ratio);
		if (p < 0)
			p = 0;
		else if (p >= Global.g_CurEpubBook.GetTotalPage())
			p = Global.g_CurEpubBook.GetTotalPage();
		
		Global.g_CurEpubBook.curPage = p;
		
		if(Global.g_bShowTwoPages == false){
			Global.g_curOnePageViewer.refreshPage();
		}
	}
	
	public static View OpenFontMenu(Context context)
	{
		if(Global.g_autoState == true){
			AlertDialog.Builder aDialog = new AlertDialog.Builder(context);
			aDialog.setTitle("메뉴 열기 실패");
			aDialog.setMessage("자동 넘김 중에는 이 기능을 사용할 수 없습니다.");
			aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

			AlertDialog ad = aDialog.create();
			ad.show();
			
			return null;
		}
		
		m_openProgressHandler = new Handler();
			
		m_context = context;
		m_activity = (Activity)context;
		View m_topbar_menu;
		
		final LayoutInflater inflater = (LayoutInflater)m_context.getSystemService(m_activity.LAYOUT_INFLATER_SERVICE);
		final RelativeLayout back = (RelativeLayout)m_activity.findViewById(R.id.MM_rel_back);
		
		//RemoveTopBarMenu();
		
		//m_topbar_menu_type = 1;
		
		m_topbar_menu = inflater.inflate(R.layout.fontmenu, null);
		back.addView(m_topbar_menu);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)m_topbar_menu.getLayoutParams();
		params.topMargin = 75;
		if(Global.g_bShowTwoPages == false)
			params.leftMargin = 0;
		else
			params.leftMargin = 650;
		
		nFontRate = Global.g_FontRate;
		TextView textView = (TextView)m_activity.findViewById(R.id.FM_example_text);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (9 * nFontRate) / 100);
		
		// final SeekBar bar = (SeekBar)m_activity.findViewById(R.id.FM_seekBar);				
		
		// bar.setMax(400); // 최대 500%
		// bar.setProgress((int)(Global.g_LineHeight * 100.0f) - 100); // 1.6 -> 160% -> 60 // 최소가 100임
		
		final Button button_plus = (Button)m_activity.findViewById(R.id.FM_button_plus);
		button_plus.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_plus.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_font_size_b));
				}
				else if( event.getAction() == MotionEvent.ACTION_UP ){
					TextView textView = (TextView)m_activity.findViewById(R.id.FM_example_text);
					
					if( nFontRate < 500 )
					{
						nFontRate += 50;
						textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (9 * nFontRate) / 100);
					}
					
					button_plus.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_font_size_a));
				}
				
				return true;
			}
		});
		
		final Button button_minus = (Button)m_activity.findViewById(R.id.FM_button_minus);
		button_minus.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_minus.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_font_size_d));
				}
				else{
					TextView textView = (TextView)m_activity.findViewById(R.id.FM_example_text);
					if ( nFontRate > 100) 
					{
						nFontRate -= 50;
						textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (9 * nFontRate) / 100);
					}
					
					button_minus.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_font_size_c));
				}
				
				return true;
			}
		});
		
		final Button button_apply = (Button)m_activity.findViewById(R.id.FM_apply);
		button_apply.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){
		
					m_openProgressDialog = new ProgressDialog(m_activity);
					m_openProgressDialog.setMessage("글꼴 변경 중... \n잠시만 기다려 주세요.");
					m_openProgressDialog.show();
				
					new Thread( new Runnable() {
						public void run() {     
							
							m_openProgressHandler.post(new Runnable() {
								public void run() {
									if( !m_openProgressDialog.isShowing() ) m_openProgressDialog.show();
									RefreshWithFontSize( nFontRate );
									
									m_openProgressHandler.post(new Runnable() { 
										public void run() {
											if(m_openProgressDialog.isShowing()){
												m_openProgressDialog.hide();
												m_openProgressDialog.dismiss();
											}
										}
									});
								}
							});
							
						}
					}).start();
				}
				
				return true;
			}
		});
		
		
		/*bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				
				int lineHeight = seekBar.getProgress() + 100;
				Global.g_LineHeight = (float)lineHeight / 100.0f;
				
				float ratio = (float)Global.g_CurEpubBook.GetCurPage() / (float)Global.g_CurEpubBook.GetTotalPage();
				
				Global.g_CurEpubBook.ProcessPaging();
				
				int p = (int)(Global.g_CurEpubBook.GetTotalPage() * ratio);
				if (p < 0)
					p = 0;
				else if (p >= Global.g_CurEpubBook.GetTotalPage())
					p = Global.g_CurEpubBook.GetTotalPage();
				
				Global.g_CurEpubBook.curPage = p;
				
				if(Global.g_bShowTwoPages == false){
					Global.g_curOnePageViewer.refreshPage();
				}
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
			}
		});*/
		
		addFontFamily("ArialMT");
		addFontFamily("sans");
		addFontFamily("monospace");
		
		return m_topbar_menu;
	}
	
	public static void addFontFamily(final String font)
	{
		final TableLayout table = (TableLayout)m_activity.findViewById(R.id.FM_table);

	    RelativeLayout rel = new RelativeLayout(m_context);
	    RelativeLayout.LayoutParams params;
	    
	    TextView word = new TextView(m_context);
	    word.setText(font);
	    word.setTextSize(20);
	    word.setTextColor(Color.BLACK);
	    word.setTypeface(Typeface.create(font, Typeface.NORMAL));
	    
	    params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
	    
	    rel.addView(word, params);

	    TableRow tr = new TableRow(m_context);
	    
	    tr.addView(rel, new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
	    
	    table.addView(tr, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
	    
	    // 클릭시 이동?
	    rel.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				Log.i("debug", "글씨체 변경 " + font);
				
				Global.g_FontFace = font;
				
				Global.g_FontMgr.ClearCache();
				Global.g_FontMgr.UpdateFontList();			    
			    
				float ratio = (float)Global.g_CurEpubBook.GetCurPage() / (float)Global.g_CurEpubBook.GetTotalPage();
				
				Global.g_CurEpubBook.ProcessPaging();
				
				int p = (int)(Global.g_CurEpubBook.GetTotalPage() * ratio);
				if (p < 0)
					p = 0;
				else if (p >= Global.g_CurEpubBook.GetTotalPage())
					p = Global.g_CurEpubBook.GetTotalPage();
				
				Global.g_CurEpubBook.curPage = p;
				
				if(Global.g_bShowTwoPages == false){
					Global.g_curOnePageViewer.refreshPage();
				}
				else{
					if(p % 2 > 0)
						Global.g_CurEpubBook.curPage = p+1;
					Global.g_curTwoPageViewer.refreshPage();
				}
			}
		});
	}
	
}