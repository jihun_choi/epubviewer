package ybm.EpubViewer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class SearchBook extends Activity 
{
	  private static final float DEFAULT_HDIP_DENSITY_SCALE = 1.5f;
	  BookForLibrary m_curbook = null;
	
	 int PixelFromDP(int DP)
	 {
	    float scale = getResources().getDisplayMetrics().density;
	    
	    return (int)(DP / scale * DEFAULT_HDIP_DENSITY_SCALE);
	 }
	 
	RelativeLayout relativeLayout1 = null, relativeLayout2 = null;
	
	LinearLayout newLinearRow()
	{
		LinearLayout linearRow = new LinearLayout(this);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
	            LinearLayout.LayoutParams.WRAP_CONTENT,
	            LinearLayout.LayoutParams.WRAP_CONTENT);
		
		linearRow.setOrientation(LinearLayout.HORIZONTAL);
		linearRow.setGravity(Gravity.CENTER);
		linearRow.setBackgroundDrawable(getResources().getDrawable(R.drawable.row));
		linearRow.setLayoutParams(layoutParams);
		
		relativeLayout1 = new RelativeLayout(this);
		RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT, 
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		relativeLayout1.setLayoutParams(layoutParams2);
		
		relativeLayout2 = new RelativeLayout(this);
		relativeLayout2.setLayoutParams(layoutParams2);
		
		relativeLayout2.setPadding(50, 0,0,0);
		
		linearRow.addView(relativeLayout1);
		linearRow.addView(relativeLayout2);
		
		return linearRow;
	}
	
	public void OpenBook(BookForLibrary book) // 원본은 LibraryView에 그대로 놓아둠.
	{
    	//책 파일이 있는지 검사
    	byte []bookUrl = book.bookUrl.getBytes();
    	for(int i = book.bookUrl.length() - 1; i >=0; --i)
    	{
    		if (bookUrl[i] =='/')
    		{
    			String filename = new String(bookUrl, i + 1, book.bookUrl.length() - i - 1);
    			
    			final String path = Util.absolutePath + "/" + filename;
    			File file = new File(path);
    		
    			{
    				//Log.i("DEBUG", "책을 로컬에서 읽음");
    				
    				// TEST 도중, 책을 지우는 경우. 캐시 파일도 함께 지움.
    				/*file.delete();
    				String decFilename = path + ".cache";
    				File cfile = new File(decFilename);
    				if(cfile.exists())
    					cfile.delete();*/
    				
    				Intent intent = new Intent(SearchBook.this, EPubViewer.class);
    				intent.putExtra("bookFile", path);
    				intent.putExtra("bookId", book.bookId);
    				startActivity(intent);
    				
    			}
    			break;
    		}
    	}
    }


	void SearchBookByTitle( String strTitle )
	{
		int i,j;
		int nFoundItems = 0;
		
		RelativeLayout relBackGround = (RelativeLayout)findViewById(R.id.search_book_background);
		relBackGround.setBackgroundDrawable( null );
		
		LinearLayout searchBackground = (LinearLayout)findViewById(R.id.search_background);
		LinearLayout linearRow = null;
		
		searchBackground.getChildAt(0).setVisibility(View.GONE);
		searchBackground.getChildAt(1).setVisibility(View.GONE);
		searchBackground.getChildAt(2).setVisibility(View.GONE);
		
		for(i = 0; i < Global.books.books.size(); i++)
		{
			final BookForLibrary book = Global.books.books.get(i);
			String bookName = book.bookName;
			
			if( bookName.matches( ".*" + strTitle + ".*") == true ) 
			{
				nFoundItems++;
				if( nFoundItems % 2 == 1 )
				{
					if( linearRow != null ) searchBackground.addView( linearRow );
					linearRow = newLinearRow();
				}

				ImageView image = new ImageView(this);
				image.setImageBitmap(book.bookCoverImage);
				
				if( nFoundItems % 2 == 1 ) relativeLayout1.addView(image);
				else relativeLayout2.addView(image);
				

				image.setOnClickListener(new View.OnClickListener() {							
					public void onClick(View v) {
						
						m_curbook = book;
						
						String dirPath = getFilesDir().getAbsolutePath();
						String filename = dirPath + "/recbk";
										
						try {
							File file = new File(filename);
							FileOutputStream fout = new FileOutputStream(file);
							String bookId = m_curbook.bookId;
							
							byte[] buf = bookId.getBytes();
							fout.write(buf);
							fout.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}catch (IOException e){
							e.printStackTrace();
						}
						
						//file.delete();
						
						String bookFilename = book.bookUrl.substring(book.bookUrl.lastIndexOf('/'));
						final String bookPath = Util.absolutePath + bookFilename;
						File bookFile = new File(bookPath);
						
						if( bookFile.exists()){
							OpenBook(book);
						}
						else{
							new AlertDialog.Builder(SearchBook.this)
    						.setTitle("알림")
    						.setMessage("보관함에서 책을 구매해 주세요.")
    						.setPositiveButton("확인", null)
    						.show();
						}
					}
				});
				
			}
		}
		
		if( linearRow != null ) searchBackground.addView( linearRow );
		
		for(i = 0; i < 3;i++ ){
			linearRow = newLinearRow();
			searchBackground.addView( linearRow );
		}
		
		if( nFoundItems == 0 )
		{
			new AlertDialog.Builder(SearchBook.this)
			.setTitle("알림")
			.setMessage("검색하신 도서가 없습니다.")
			.setPositiveButton("확인", null)
			.show();
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setContentView(R.layout.search_book);
	    
	    EditText bookName = (EditText)findViewById(R.id.edit_book_search);
	    
	    bookName.setOnKeyListener(new OnKeyListener() {
	    	
	        public boolean onKey(View v, int keyCode, KeyEvent event) {
	            if (keyCode == KeyEvent.KEYCODE_ENTER) {
	            	String searchText = ((EditText)v).getText().toString();
	            	
	            	InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	            	mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
	            	v.setVisibility(View.GONE);
	            	
	            	SearchBookByTitle( searchText );
	                
	                return true;
	            }
	            return false;
	        }
	    });
	}
}