package ybm.EpubViewer.EPubBook;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ybm.EpubViewer.Global;
import ybm.EpubViewer.Util;

public class SelectedParagraphInfo {
	public HtmlContent content;
	public int paragraphIdx;
	public int	textIdx;
	public int charIdx;
	
	public Page page; // TwoPage에서 쓰기 위해서 임시로 추가. 원래 라이브러리에는 없음.
	public int margin; // 역시 임시 추가. 원래 라이브러리에는 없음.

	void Save(OutputStream out)
	{
		int contentIdx = content.contentIdx;

		try {
			out.write(Util.intToByteArray(contentIdx));
			out.write(Util.intToByteArray(paragraphIdx));
			out.write(Util.intToByteArray(textIdx));
			out.write(Util.intToByteArray(charIdx));
		} catch (IOException e) {
			e.printStackTrace();

		}
	
	}

	void Load(InputStream in)
	{
		int contentIdx = 0;	
		try {
			byte[] buffer = new byte[4];
			
			in.read(buffer, 0, 4);
			contentIdx = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			paragraphIdx = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			textIdx = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			charIdx = Util.byteArrayToInt(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			/* error 처리!!*/
		}
		content = Global.g_CurEpubBook.GetHtmlContentAtIdx(contentIdx);
	}

	public int compare(SelectedParagraphInfo other)
	{
		return compare(other.content.contentIdx, other.paragraphIdx, other.textIdx, other.charIdx);
	}

	int compare(int a, int b, int c, int d)
	{
		if (content.contentIdx > a)
			return Global.NSOrderedDescending;
		else if (content.contentIdx < a)
			return Global.NSOrderedAscending;
		
		if (paragraphIdx > b)
			return Global.NSOrderedDescending;
		else if (paragraphIdx < b)
			return Global.NSOrderedAscending;
		
		if (textIdx > c)
			return Global.NSOrderedDescending;
		else if (textIdx < c)
			return Global.NSOrderedAscending;
		
		if (charIdx > d)
			return Global.NSOrderedDescending;
		else if (charIdx < d)
			return Global.NSOrderedAscending;
		return Global.NSOrderedSame;
	};

	boolean isEqual(SelectedParagraphInfo src)
	{
		if (content != src.content)
			return false;
		if (paragraphIdx != src.paragraphIdx)
			return false;
		if (textIdx != src.textIdx)
			return false;
		if (charIdx != src.charIdx)
			return false;
		return true;
	}

	void Swap(SelectedParagraphInfo dest)
	{
		HtmlContent _content = dest.content;
		int _paragraphIdx = dest.paragraphIdx;
		int	_textIdx = dest.textIdx;
		int _charIdx = dest.charIdx;
		
		dest.content = content;
		dest.textIdx = textIdx;
		dest.paragraphIdx = paragraphIdx;
		dest.charIdx = charIdx;
		
		content = _content;
		paragraphIdx = _paragraphIdx;
		textIdx = _textIdx;
		charIdx = _charIdx;
	}

	public void Copy(SelectedParagraphInfo src)
	{
		content = src.content;
		paragraphIdx = src.paragraphIdx;
		textIdx = src.textIdx;
		charIdx = src.charIdx;
	}

	public SelectedParagraphInfo(SelectedParagraphInfo src)
	{
		Copy(src);
	}
	
	public SelectedParagraphInfo(HtmlContent a, int b, int c, int d)
	{
		content = a;
		paragraphIdx = b;
		textIdx = c;
		charIdx = d;
	}

	public SelectedParagraphInfo() 
	{
		content = null;
		paragraphIdx = 0;
		textIdx = 0;
		charIdx = 0;
	}
}
