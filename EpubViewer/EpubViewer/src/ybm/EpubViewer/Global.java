package ybm.EpubViewer;

import ybm.EpubViewer.EPubBook.EpubBook;
import ybm.EpubViewer.EPubBook.FontManager;
import ybm.EpubViewer.EPubBook.MemoInfo;
import ybm.EpubViewer.EPubBook.Paragraph;
import ybm.EpubViewer.EPubBook.SelectedParagraphInfo;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

public class Global {
	public static BooksForLibrary books = null;
	
	public static Boolean g_bLogin = false;//로그인 여부
	public static Boolean g_bIsImageView = true;
	public static String g_LoginName = null;
	public static String g_LoginID = null;
	public static String g_LoginEmail = null;
	public static String g_LoginPwd = null;

	public static boolean g_bShippingVersion = true; // true라면 결제 체크를 함
	public static boolean g_bChangeView = false; // true - 보여주는 페이지를 바꾸는 중
	public static ProgressDialog g_loadingDialog = null;
	
	public static OnePageViewer g_curOnePageViewer = null;
	public static TwoPageViewer g_curTwoPageViewer = null;
	
	public static View g_curWordMenu = null;
	public static MediaRecorder g_curRecorder = null; // 녹음 메모시 사용하는 레코더
	public static String g_curRecordingPath = null;
	
	public static int g_curSelectedWordTop = 0;
	public static int g_curSelectedWordLeft = 0;
	
	public static Activity g_curActivity = null; // 현재 보고있는 Activity
	public static View g_curMp3Bar = null; // 현재 페이지에 나타난 mp3play 메뉴
	public static View g_curMp3Play = null; // 현재 페이지에 나타난 mp3play 버튼
	
	public static SeekBar g_curMp3SeekBar = null;
	public static Button g_curMp3PlayButton = null;
	public static Button g_curMp3RemoveButton = null;
	
	public static Rect g_curMp3PlayPosition = null; // 현재 페이지에 나타난 mp3play 버튼 위치
	public static String g_curMp3PlayURL = null; // 현재 페이지에 나타난 mp3play 버턴에 연결 된 mp3 주소
	public static Paragraph g_curMp3PlayParagraph = null; // 현재 페이지에 나타난 mp3play 버튼의 패러그라프
	public static MediaPlayer g_curMp3Player = null; // 스트리밍하는 mediaplayer	
	
	public static String g_BooksXMLHost = "www.ybmmobile.com"; // 소스 코드에서 사용 안하고 있음
	public static String g_BooksXMLURL = "/appstore/epub/product_list.asp"; // 마찬가지
	
	public static String g_PaymentXMLHost = "www.ybmmobile.com";
	public static String g_PaymentXMLURL =  "/appstore/epub/inipay/mobilestore_order_id.asp";

	public static String g_LoginXMLHost = "certify.ybmsisa.com:443"; // 마찬가지
	public static String g_LoginXMLURL = "brandapp/wbook/ybmlogingw.asp"; // 마찬가지

	public static String g_PurchasedBook = "http://www.ybmmobile.com/appstore/epub/purchase_list.asp?user_id="; // 마찬가지

	public static float g_LeftMargin = 0;
	public static float g_RightMargin = 0;
	public static float g_TopMargin = 0;
	public static float g_BottomMargin = 0;
	public static int g_ParagraphIndent = 10;

	public static float g_LineHeight = 1.6f; // 기본 줄 간격은 160%
	public static int SpaceBetweenLines = 0; // 이제는 px로 줄 간격을 정하지 않고 폰트 크기에 따라 %로 처리
	public static Boolean		 g_bSameFontSize = false;
	public static int g_FontSize = 20;
	public static int g_FontRate = 200;
	public static String  g_FontFace = null;
	public static int g_autoTime = 20;
	public static boolean g_autoState = false;
	

	public static Rect g_ScreenFrame;
	public static Rect g_PageFrame;
	public static Boolean	  g_bShowTwoPages = false;//TRUE:가로 FALSE:세로

	public static Integer g_BgColor = null;

	public static EpubBook g_CurEpubBook = null;
	public static FontManager g_FontMgr =  null;

	public static SelectedParagraphInfo g_SelMin;
	public static SelectedParagraphInfo g_SelMax;
	
	public static MemoInfo g_curMemo = null;
	
	public static int g_OldCurPage = -1;
	public static int g_OldTotalPage = -1;

/*	public static eLanguage g_curLanguage;
	public static AudioPlayerView* g_AudioView;//mp3 play를 위한 뷰이다. epubView에 붙이니 메모리 정리당할때 사라져 버리는 아픔..ㅠㅠ  
*/
	public static String g_DeviceToken = null;
	
	public static int UITextAlignmentLeft = 0;
	public static int UITextAlignmentCenter = 1;
	public static int UITextAlignmentRight = 2;
	
	public static int NSOrderedAscending = -1;
	public static int NSOrderedSame = 0;
	public static int NSOrderedDescending = 1;
	
	public static boolean isInRect(Rect r, Point p)
	{
		return (p.x >= r.left && p.y >= r.top && p.x <= r.right && p.y <= r.bottom);
	}
	
	public static int GetColorFromString(String strBGColor, int defColor)
	{
		int bgColor = 0;
		if (strBGColor != null)
		{
			String bgcolor = strBGColor.replace(" ", "");
			
			if (bgcolor.equalsIgnoreCase("black"))
				bgColor = Color.BLACK;
			else if (bgcolor.equalsIgnoreCase("darkgray"))
				bgColor = Color.DKGRAY;
			else if (bgcolor.equalsIgnoreCase("lightgray"))
				bgColor = Color.LTGRAY;
			else if (bgcolor.equalsIgnoreCase("white"))
				bgColor = Color.WHITE;      // 1.0 white 
			else if (bgcolor.equalsIgnoreCase("gray"))
				bgColor = Color.GRAY;       // 0.5 white 
			else if (bgcolor.equalsIgnoreCase("red"))
				bgColor = Color.RED;        // 1.0, 0.0, 0.0 RGB 
			else if (bgcolor.equalsIgnoreCase("green"))
				bgColor = Color.GREEN;      // 0.0, 1.0, 0.0 RGB 
			else if (bgcolor.equalsIgnoreCase("blue"))
				bgColor = Color.BLUE;       // 0.0, 0.0, 1.0 RGB 
			else if (bgcolor.equalsIgnoreCase("cyan"))
				bgColor = Color.CYAN;       // 0.0, 1.0, 1.0 RGB 
			else if (bgcolor.equalsIgnoreCase("yellow"))
				bgColor = Color.YELLOW;     // 1.0, 1.0, 0.0 RGB 
			else if (bgcolor.equalsIgnoreCase("magenta"))
				bgColor = Color.MAGENTA;    // 1.0, 0.0, 1.0 RGB 
			else if (bgcolor.equalsIgnoreCase("orange"))
				bgColor = Color.argb(1, 255, 165, 0);
			else if (bgcolor.equalsIgnoreCase("purple"))
				bgColor = Color.argb(1, 128, 0, 128);     // 0.5, 0.0, 0.5 RGB 
			else if (bgcolor.equalsIgnoreCase("brown"))
				bgColor = Color.argb(1, 128, 0, 0);
			else if (bgcolor.indexOf("rgb") != -1 || bgcolor.indexOf("RGB") != -1)
			{
				//rgb(1,1,1);
				int r = 0, g = 0, b = 0; 
				int idx = 0;
				int mode = 0;
				int st = 0, end = 0;
				for(int i = 0; i< bgcolor.length(); ++i)
				{
					char ch = bgcolor.charAt(i); 
					if (mode == 0 && ch >= '0' && ch <= '9')
					{
						st = i;
						mode = 1;
					}
					else if (ch == ',' || ch == ')')
					{
						end = i - 1;
						mode = 0;
						if (idx == 0)
							r = Integer.parseInt(bgcolor.substring(st, end));
						else if (idx == 1)
							g = Integer.parseInt(bgcolor.substring(st, end));
						else if (idx == 2)
							b = Integer.parseInt(bgcolor.substring(st, end));
						idx ++;
					}
				}
				bgColor = Color.argb(1, r, g, b);
			}
			else if (bgcolor.startsWith("#"))
			{
				int r = Integer.parseInt(bgcolor.substring(1, 2), 16);
				int g = Integer.parseInt(bgcolor.substring(3, 4), 16);
				int b = Integer.parseInt(bgcolor.substring(5, 6), 16);
				bgColor = Color.argb(1, r, g, b);	
			}
			if (bgColor != 0)
				return bgColor;
		}
		return defColor;
	}
	
	
};

