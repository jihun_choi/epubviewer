package ybm.EpubViewer.EPubBook;

import java.util.Vector;

public class EpubSpine {
	String toc;
	Vector<String> itemrefs = new Vector<String>();

	public void AddItemRef(String itemref)
	{
		itemrefs.add(itemref);
	}

	public int GetItemCount()
	{
		return itemrefs.size();
	}

	public String GetItemAtIndex(int idx)
	{
		return itemrefs.get(idx);
	}
}	
