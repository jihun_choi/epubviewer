package ybm.EpubViewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LibraryListView extends Activity {
	
	public ProgressDialog proDialog1;
	public Handler handler = new Handler();
	
	int []m_buy_id;
	int []m_bookname_id;
	int m_maxBookCount;
	
	boolean m_optionMenuOpened = false;
	BookForLibrary m_curbook = null;
	
	SharedPreferences prefs; 
	
	/*
	 * =스레드안에서= 1) 책리스트가 없으면 책 리스트를 다운받는다. =>파일로 저장한다 2) 책리스트 파일 로딩 파싱해서
	 * bookslist를 만든다 3) 커버이미지가 없으면 다운받는다 4) 화면에 그려준다.
	 */
	void CheckPurchasedBooklist()
	{
		if (Global.g_bLogin)
		{
			String url = String.format("http://www.ybmmobile.com/appstore/epub/purchase_list.asp?user_id=%s",Global.g_LoginID);
			String str = HttpUtil.DownloadHtml(url);

			if (str != null)
			{
				for(int i = 0; i < Global.books.books.size(); ++i)
				{
					BookForLibrary book = Global.books.books.get(i);
					String bookId = String.format("\"%s\"",book.bookId);
					book.isPurchased = (str.indexOf(bookId) > 0);
				}
			}
		}
	}

	boolean IsPurchasedBook(String bookId)
	{
		for(int i = 0; i < Global.books.books.size(); ++i)
		{
			BookForLibrary book = Global.books.books.get(i);
			if (book.bookId.equalsIgnoreCase(bookId))
				return book.isPurchased;
		}
		return false;
	}
	
	void LoginOK(String userid, String pwd, boolean autologin) // 로그인 완료되는 경우 
	{
		final Context thisContext = this;
		
		String _id = NFEncode.nfencode(userid);
		String _pwd = NFEncode.nfencode(pwd);
		String loginUrl = String.format("https://certify.ybmsisa.com/brandapp/wbook/ybmlogingw.asp?id=%s&pw=%s&domain=",_id, _pwd);
		String str = HttpUtil.DownloadHtml(loginUrl);
	
		if(str != null)
		{
			final String rCode = "<ResultCode>";
			int r1 = str.indexOf(rCode) + rCode.length();
			int retCode = Integer.parseInt(str.substring(r1, r1 + 1));
			if (retCode == 1)
			{
				final String kName = "<KName>";
				int r2 = str.indexOf(kName);
				int r3 = str.indexOf("</KName>");
				Global.g_LoginName = str.substring(r2 + kName.length(), r3); 
			
				final String email = "<email>";
				r2 = str.indexOf(email);
				r3 = str.indexOf("</email>");
				Global.g_LoginEmail = str.substring(r2 + email.length(), r3); 
				
				Global.g_LoginID = userid;
				Global.g_LoginPwd = pwd;
				Global.g_bLogin = true;
				
				final Button button2 = (Button)findViewById(R.id.BS_button_list_login_bottom);
				button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.logout));
					
				final Button button = (Button)findViewById(R.id.BS_button_list_login);
				int wid = button.getWidth();
				int hgt = button.getHeight();
				
				// 로그인이 완료되어 로그아웃 버튼으로 바꿈
				button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_a));
				button.setWidth(wid);
				button.setHeight(hgt);
				
				SharedPreferences.Editor editor = prefs.edit();
				editor.clear();
				editor.putString("g_LoginID", userid);
				editor.putString("g_LoginPwd", pwd);
								
				if (autologin)
				{
					editor.putBoolean("g_bLogin", true);
					SaveID();
				}
				else
					editor.putBoolean("g_bLogin", false);

				editor.commit();
				
				CheckPurchasedBooklist();
				/*
				 *To do : 구매한 책과, 그렇지 않은 책의 정보가 들어왔으니 이에 맞게 다시 그려준다 !!!
				 * */
				
				SortPurchased();
				
				
			}
			else
			{
					AlertDialog.Builder aDialog = new AlertDialog.Builder(thisContext);
					aDialog.setTitle("Login 실패");
					aDialog.setMessage("아이디와 비밀번호를 다시 한 번 확인해주세요.");
					aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								((LibraryListView)thisContext).PopupLoginDialog();
							}
						});
	
					AlertDialog ad = aDialog.create();
					ad.show();		
			}
		}
		else
		{
			AlertDialog.Builder aDialog = new AlertDialog.Builder(thisContext);
			aDialog.setTitle("Login 실패");
			aDialog.setMessage("서버에 접속 실패 했습니다.");
			aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {}
				});

			AlertDialog ad = aDialog.create();
			ad.show();		
		}		
	}
	
	void PopupLoginDialog()
	{
		//id : ybmaster
		//pwd: 1111
		
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.login, null);//(ViewGroup) findViewById(R.id.main));
						
		AlertDialog.Builder aDialog = new AlertDialog.Builder(this);
		aDialog.setView(layout);
								
		aDialog.setPositiveButton("로그인", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				String userid = ((EditText)layout.findViewById(R.id.editId)).getText().toString();
				String pwd = ((EditText)layout.findViewById(R.id.editPwd)).getText().toString();
				boolean autologin = ((CheckBox)layout.findViewById(R.id.checkAutoLogin)).isChecked();
				
				LoginOK(userid, pwd, autologin);
				if (autologin == false)
				{// 로긴 정보가 있는 파일을 지운다 
					String dirPath = getFilesDir().getAbsolutePath();
					String filename = dirPath + "/mmmmm";
					File file = new File(filename);
					file.delete();
				}
			}
		});
		aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		AlertDialog ad = aDialog.create();
		ad.show();		
	}
	
	void SaveID()
	{
		String dirPath = getFilesDir().getAbsolutePath();
		String filename = dirPath + "/mmmmm";
		try {

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(filename));
			byte[] eId = Base64Util.decode(Global.g_LoginID);//.getBytes();
			byte[] ePwd = Base64Util.decode(Global.g_LoginPwd);//.getBytes();

			out.putNextEntry(new ZipEntry("/books1"));
			out.write(Global.g_LoginID.length());
			out.write(eId, 0, eId.length);
			out.closeEntry();

			out.putNextEntry(new ZipEntry("/books2"));
			out.write(Global.g_LoginPwd.length());
			out.write(ePwd, 0, ePwd.length);
			out.closeEntry();
			out.close();

		} catch (IOException e) {
		}
	}

	void LoadID()
	{
		String dirPath = getFilesDir().getAbsolutePath();
		String filename = dirPath + "/mmmmm";
		File file = new File(filename);

		if (file.exists()) {
			try {
				FileInputStream fis = new FileInputStream(filename);

				ZipDecryptInputStream zdis = new ZipDecryptInputStream(fis, "eoqkrsktp.");
				ZipInputStream zis = new ZipInputStream(zdis);
				ZipEntry ze;
				while ((ze = zis.getNextEntry()) != null) 
				{
					if (ze.getName().compareToIgnoreCase("/books1") == 0 || ze.getName().compareToIgnoreCase("/books2") == 0) 
					{
						ByteArrayBuffer buf = new ByteArrayBuffer(5000);
						int BUFFER = 2048;
						byte data[] = new byte[BUFFER];

						int len = zis.read();
						int count;
						while ((count = zis.read(data, 0, BUFFER)) != -1)
							buf.append(data, 0, count);
						
						if (ze.getName().compareToIgnoreCase("/books1") == 0)
							Global.g_LoginID = Base64Util.encode(buf.buffer()).toString().substring(0, len);
						else
							Global.g_LoginPwd = Base64Util.encode(buf.buffer()).toString().substring(0, len);
			
					}
					zis.closeEntry();
				}
				zis.close();
				
				LoginOK(Global.g_LoginID, Global.g_LoginPwd, false);
				
			} catch (Exception e) {
				file.delete();
				Logger.d("dd", "Error in unzip books");
			}
		}
	}
	
	void Logout( MotionEvent event)
	{
		final Button button = (Button)findViewById(R.id.BS_button_list_login);
	
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			int wid = button.getWidth();
			int hgt = button.getHeight();
			button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_b));
			button.setWidth((int)(wid * 0.7));
			button.setHeight((int)(hgt * 0.7));
		}
		else if(event.getAction() == MotionEvent.ACTION_UP){
			/*
			AlertDialog.Builder aDialog = new AlertDialog.Builder(this);
			aDialog.setTitle("로그 아웃");
			aDialog.setMessage("로그아웃 하시겠습니까?");
			
			aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						
						button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
						Global.g_bLogin = false;			
						Global.g_LoginName = null;
						Global.g_LoginEmail = null;
						Global.g_LoginID = null;
						Global.g_LoginPwd = null;
						
						String dirPath = getFilesDir().getAbsolutePath();
						String filename = dirPath + "/mmmmm";
						File file = new File(filename);
						file.delete();
					}
				});
			
			aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_a));
				}
			});

			AlertDialog ad = aDialog.create();
			ad.show();		
			*/
			
			PopupLogoutDialog();
		}
	}
	
	void PopupLogoutDialog()
	{
		AlertDialog.Builder aDialog = new AlertDialog.Builder(this);
		aDialog.setTitle("로그 아웃");
		aDialog.setMessage("로그아웃 하시겠습니까?");
		
		aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					final Button button = (Button)findViewById(R.id.BS_button_list_login);
					button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
					
					final Button button2 = (Button)findViewById(R.id.BS_button_list_login_bottom);
					button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.loginbutton));
							
					Global.g_bLogin = false;			
					Global.g_LoginName = null;
					Global.g_LoginEmail = null;
					Global.g_LoginID = null;
					Global.g_LoginPwd = null;
					
					String dirPath = getFilesDir().getAbsolutePath();
					String filename = dirPath + "/mmmmm";
					File file = new File(filename);
					file.delete();
					
					SharedPreferences.Editor editor = prefs.edit();
					editor.clear();
					editor.commit();
					
					int i;
					for(i=0;i<=Global.books.books.size()-1;i++){
						
						if(i >= m_maxBookCount) break;
						
						ImageView buybook = (ImageView)findViewById(m_buy_id[i]);
						buybook.setImageDrawable(getResources().getDrawable(R.drawable.btn_buy_none));
						
						/*
						ImageView overlay = (ImageView)findViewById(m_overlays[i]);
						ImageView purchase = (ImageView)findViewById(m_purid[i]);
						
						overlay.setVisibility(ImageView.VISIBLE);
						purchase.setVisibility(ImageView.INVISIBLE);*/
					}
					
				}
			});
		
		aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				final Button button = (Button)findViewById(R.id.BS_button_list_login);
				button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_a));
				
				final Button button2 = (Button)findViewById(R.id.BS_button_list_login_bottom);
				button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.logout));
			}
		});

		AlertDialog ad = aDialog.create();
		ad.show();
		
	}
	
	void initMenuButtons()
	{
		final Button button_storage = (Button)findViewById(R.id.BS_button_list_storage);
		button_storage.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){
					Intent myIntent = new Intent(v.getContext(), Library.class);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(myIntent);
		            overridePendingTransition(0, 0);
				}
				return false;
			}
		});
		
		final Button button_mylibrary = (Button)findViewById(R.id.BS_button_list_mylibrary);
		button_mylibrary.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){
					Intent myIntent = new Intent(v.getContext(), MyLibrary.class);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(myIntent);
		            overridePendingTransition(0, 0);
				}
				return false;
			}
		});
		
		final Button buttonSearch = (Button)findViewById(R.id.BS_button_list_booksearch);
		buttonSearch.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){
					if( Global.g_bLogin == false ) {
						new AlertDialog.Builder(LibraryListView.this)
						.setTitle("알림")
						.setMessage("로그인 하신 후, 이용하실 수 있습니다.")
						.setPositiveButton("확인", null)
						.show();
						return false;
					}
				
					Intent myIntent = new Intent(v.getContext(), SearchBook.class);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(myIntent);
					overridePendingTransition(0, 0);
				}
				
				return false;
			}
		});
		
		final Button buttonInfo = (Button)findViewById(R.id.BS_button_list_info);
		buttonInfo.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				Intent myIntent = new Intent(v.getContext(), UserGuide.class);
				myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent);
				overridePendingTransition(0, 0);
				
				return false;
			}
		});
	
		final Button buttonLogin = (Button)findViewById(R.id.BS_button_list_login_bottom);
		buttonLogin.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if (Global.g_bLogin == false)
				{
					if(event.getAction() == MotionEvent.ACTION_UP){
						// button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
						
						//login 창을 띄운
						PopupLoginDialog();
					}
				}
				else
				{
					if(event.getAction() == MotionEvent.ACTION_UP){
						PopupLogoutDialog();
					}
					//Logout(event);
				}
				return false;
			}
		});
	}

	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		m_maxBookCount = 50;
		
		setContentView(R.layout.bookshelf_list);
		
		init();
		
		TelephonyManager telManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		Global.g_DeviceToken = telManager.getDeviceId();
		
		prefs = getSharedPreferences("PrefName", MODE_PRIVATE);		
		Global.g_bLogin = prefs.getBoolean("g_bLogin", false);
		Global.g_LoginID = prefs.getString("g_LoginID", "");
		Global.g_LoginPwd = prefs.getString("g_LoginPwd", "");
		
		initMenuButtons();
		
		final Button button = (Button)findViewById(R.id.BS_button_list_login);
		button.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (Global.g_bLogin == false)
				{
					if(event.getAction() == MotionEvent.ACTION_DOWN){
						button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_b));
					}
					else if(event.getAction() == MotionEvent.ACTION_UP){
						button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
						
						//login 창을 띄운
						PopupLoginDialog();
					}
				}
				else
				{
					Logout(event);
				}
				return false;
			}
		});
		
		final Button button_toggle = (Button)findViewById(R.id.BS_button_list_toggle_view);
		button_toggle.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.image_view_on));
				}
				else if(event.getAction() == MotionEvent.ACTION_UP){
					button_toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.image_view_none));
					Intent myIntent = new Intent(v.getContext(), Library.class);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(myIntent);
		            overridePendingTransition(0, 0);
				}
				return false;
			}
		});
		
		
		Util.absolutePath = getFilesDir().getAbsolutePath();
		
		//libraryView = new LibraryView(this);
		//setContentView(libraryView);

		// 화면 회전 대응을 위해 추가
		Resources r = Resources.getSystem();
		Configuration config = r.getConfiguration();
		onConfigurationChanged(config);

		if(Global.g_bShippingVersion == true){
			Thread load = new Thread(new Runnable()	{
				public void run(){
					handler.postDelayed(new Runnable() {
						public void run(){
							SortPurchased();
					
							LoadID();
							
							if(Global.g_bLogin == true){
								LoginOK(Global.g_LoginID, Global.g_LoginPwd, Global.g_bLogin);			
							}
						}
					}, 0);
				}
			});
	
			load.start();
		}
		else{
			LoadID();									
			
			if(Global.g_bLogin == true){
				LoginOK(Global.g_LoginID, Global.g_LoginPwd, Global.g_bLogin);			
			}
		}
	}

	void SortPurchased()
	{
		int size = Global.books.books.size();
		int i, j=0;
		for(i=0;i<=size-1;i++){
			
			if(i >= m_maxBookCount)
				break;
			
			final BookForLibrary book = Global.books.books.get(i);
			String bookFilename = book.bookUrl.substring(book.bookUrl.lastIndexOf('/'));
			final String bookPath = Util.absolutePath + bookFilename;
			File bookFile = new File(bookPath);
			
			//if (IsPurchasedBook(book.bookId) == true) {
			if(bookFile.exists()){
				Global.books.books.remove(i);
				Global.books.books.add(j++, book);
			}
		}
		
		for(i=0;i<=size-1;i++){
			
			if(i >= m_maxBookCount)
				break;
			
			final BookForLibrary book = Global.books.books.get(i);
			String bookFilename = book.bookUrl.substring(book.bookUrl.lastIndexOf('/'));
			final String bookPath = Util.absolutePath + bookFilename;
			File bookFile = new File(bookPath);
			
			TextView bookNameView = (TextView)findViewById(m_bookname_id[i]);
			bookNameView.setText(book.bookName);
			
			ImageView buyButton = (ImageView)findViewById(m_buy_id[i]);
			if ( bookFile.exists() && Global.g_bLogin ) 
			{
				buyButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_view_none) );
			} else 
			{
				buyButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_buy_none) );
			}
			
			buyButton.setOnClickListener(new View.OnClickListener() {							
				public void onClick(View v) {
					
    				if (Global.g_bShippingVersion) {
	    				if (!Global.g_bLogin) {
	    					new AlertDialog.Builder(LibraryListView.this)
	    						.setTitle("알림")
	    						.setMessage("로그인 하신 후, 이용하실 수 있습니다.")
	    						.setPositiveButton("확인", null)
	    						.show();
	    					return;
	    				}
    				}
					
					m_curbook = book;
					
					String dirPath = getFilesDir().getAbsolutePath();
					String filename = dirPath + "/recbk";
									
					try {
						File file = new File(filename);
						FileOutputStream fout = new FileOutputStream(file);
						String bookId = m_curbook.bookId;
						
						byte[] buf = bookId.getBytes();
						fout.write(buf);
						fout.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}catch (IOException e){
						e.printStackTrace();
					}
					
					OpenBook(book);
				}
			});
		}
	}
	
	public static String HttpGetByString(String getURL) {
		try {
	        HttpClient client = new DefaultHttpClient(); 
	        HttpGet get = new HttpGet(getURL);
	        HttpResponse responseGet = client.execute(get);  
	        HttpEntity resEntityGet = responseGet.getEntity();  
	        
	        if (resEntityGet != null) {
	        	return EntityUtils.toString(resEntityGet);
            } else {
            	return null;
            }
		} catch (Exception e) {
		    Log.i("HTTP ERROR", e.toString());
		    return null;
		}
	}
	
	public static String HttpPostByString(String url, List<NameValuePair> data, String encoding) {
		try {
	        HttpClient client = new DefaultHttpClient();  
	        String postURL = url;
	        HttpPost post = new HttpPost(postURL);
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(data, encoding);
            post.setEntity(ent);
            HttpResponse responsePOST = client.execute(post);  
            HttpEntity resEntity = responsePOST.getEntity();  
            
            if (resEntity != null) {
                return EntityUtils.toString(resEntity, encoding);
            } else {
            	return null;
            }
	    } catch (Exception e) {
	    	Log.i("HTTP ERROR", e.toString());
	        return null;
	    }
	}
	
	boolean IsDownloadAllowed(BookForLibrary book) {
		String url = "http://www.ybmmobile.com/appstore/epub/download_list.asp?user_id=" + Global.g_LoginID + 
			"&device_id=" + Global.g_DeviceToken + "&contents_id=" + book.bookId + "&flag=";
		String str = HttpGetByString(url);
		
		String msg = "다른 기기에서 이미 인증 받은 도서입니다. 고객님의 아이디가 도용되지 않았는지 확인해주시길 바랍니다.";
		if (str != null && str.contains("TRUE")) {
			return true;
		} else {
			new AlertDialog.Builder(this)
				.setTitle("알림")
				.setMessage(msg)
				.setPositiveButton("확인", null)
				.show();
			return false;
		}
	}
	
	public void OpenBook(BookForLibrary book) // 원본은 LibraryView에 그대로 놓아둠.
	{
    	//책 파일이 있는지 검사
    	byte []bookUrl = book.bookUrl.getBytes();
    	for(int i = book.bookUrl.length() - 1; i >=0; --i)
    	{
    		if (bookUrl[i] =='/')
    		{
    			String filename = new String(bookUrl, i + 1, book.bookUrl.length() - i - 1);
    			
    			final String path = Util.absolutePath + "/" + filename;
    			File file = new File(path);
    			if (!file.exists())	
    			{
    				//Log.i("DEBUG", "책이 로컬에 없어서 다운로드");
    				
    				// 결제 체크를 하지 않으려면 g_ShippingVersion를 false로 하면 됨
    				if (Global.g_bShippingVersion) {
	    				if (!Global.g_bLogin) {
	    					new AlertDialog.Builder(this)
	    						.setTitle("알림")
	    						.setMessage("로그인 하신 후, 이용하실 수 있습니다.")
	    						.setPositiveButton("확인", null)
	    						.show();
	    					return;
	    				}
	    				
	    				if (this.IsDownloadAllowed(book) == false) { // [테스트!!!] 이거 지금 껐음
	    					return;
	    				}
	    				
	    				//구매가 안되어있나? - 이미 구매하기 버튼이 나와 있음 
	    				//if (!this.IsPurchasedBook(book.bookId) || true) { // [테스트!!!] 무조건 결제 할 수 있도록 함
	    				if (!this.IsPurchasedBook(book.bookId)) { // [테스트!!!] 무조건 결제 할 수 있도록 함
	    					final String bookId = book.bookId;
	    					final String bookPrice = book.bookPrice;
	    					final String bookName = book.bookName;
	    					
	    					new AlertDialog.Builder(this)
								.setTitle("알림")
								.setMessage("해당 도서는 한 아이디당 기기 1대로 인증됩니다. 한 번 다운로드 받으신 기기 외에 다른 기기에서는 다운로드가 불가능합니다.")
								.setPositiveButton("확인", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										// 결제 화면 띄우기
										Intent intent = new Intent(LibraryListView.this, Payment.class);
										intent.putExtra("bookId", bookId);
										intent.putExtra("bookPrice", bookPrice);
										intent.putExtra("bookName", bookName);
				    					startActivity(intent);
									}
								})
								.setNegativeButton("취소", null)
								.show();
	    					
	    					//return;
	    				}
	    				
	    				//이 디바이스 키체인으로 다운 받는게 허용되어있나?
	    				if (false && this.IsDownloadAllowed(book) == false) { // [테스트!!!] 이거 지금 껐음
	    					return;
	    				}
    				}
    				
    				proDialog1 = ProgressDialog.show(this, "", "Loading", true);
    				    				
    				final BookForLibrary final_book = book;
    				
    				new Thread(new Runnable() {
    	            	public void run() {    	                	    	                	    	      
    	            		
    	    				Util.Download(final_book.bookUrl, path);
    	    				
    	    				handler.post(new Runnable() {
    	    					public void run() {
    	    						if(proDialog1.isShowing()){
    	    							proDialog1.hide();
    	    							proDialog1.dismiss();
    	    						}
    	    					}
    	    				});
    	    				
    	    				Intent intent = new Intent(LibraryListView.this, EPubViewer.class);
    	    				intent.putExtra("bookFile", path);
    	    				startActivity(intent);
    	    				
    	    				// 구매하자마자 보이게 함.
    	    				handler.post(new Runnable(){
    	    					public void run(){
    	    						SortPurchased();
    	    					}
    	    				});
    	    				
    	    				//ImageView overlay = (ImageView)findViewById(m_overlays[final_book.layout_Idx]);
    	    				//overlay.setVisibility(ImageView.INVISIBLE);
    	            	}
    	            }).start();
    				
    			}
    			else
    			{
    				//Log.i("DEBUG", "책을 로컬에서 읽음");
    				
    				// TEST 도중, 책을 지우는 경우. 캐시 파일도 함께 지움.
    				/*file.delete();
    				String decFilename = path + ".cache";
    				File cfile = new File(decFilename);
    				if(cfile.exists())
    					cfile.delete();*/
    				
    				Intent intent = new Intent(LibraryListView.this, EPubViewer.class);
    				intent.putExtra("bookFile", path);
    				intent.putExtra("bookId", book.bookId);
    				startActivity(intent);
    				
    			}
    			break;
    		}
    	}
    }
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) { // 세로 전환시 발생
			Global.g_bShowTwoPages = false;
		} else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) { // 가로 전환시 발생
			Global.g_bShowTwoPages = true;
		}
		
		super.onConfigurationChanged(newConfig);

		/*
		 * View LoadPage = findViewById(R.id.ID_LAYOUT);
		 * 
		 * 
		 * switch(newConfig.orientation){
		 * 
		 * case Configuration.ORIENTATION_LANDSCAPE:
		 * LoadPage.setBackgroundResource(R.drawable.back_imgw);
		 * Logger.d("Androes", "Loading Start - back_imgw"); break;
		 * 
		 * case Configuration.ORIENTATION_PORTRAIT:
		 * LoadPage.setBackgroundResource(R.drawable.back_img);
		 * Logger.d("Androes", "Loading Start - back_img"); break;
		 * 
		 * }
		 */
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		final RelativeLayout optionmenu = (RelativeLayout)findViewById(R.id.BS_rel_list_bottmbar);
		if (keyCode == KeyEvent.KEYCODE_MENU) { // 메뉴 버튼을 누르면 옵션 메뉴를 생성			
			if(m_optionMenuOpened == false){
				m_optionMenuOpened = true;
				optionmenu.setVisibility(RelativeLayout.VISIBLE);
				
				/*final Button button_curbook = (Button)findViewById(R.id.BS_button_list_curbook);
				button_curbook.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						optionmenu.setVisibility(RelativeLayout.INVISIBLE);
						m_optionMenuOpened = false;
						
						String dirPath = getFilesDir().getAbsolutePath();
						String filename = dirPath + "/recbk";
										
						try {
							//File file = new File(filename);
							//FileInputStream fin = openFileInput("recbk");	
							FileInputStream fin = new FileInputStream(filename);
							byte[] buf = new byte[101];
							
							fin.read(buf);
							String tmp = new String(buf);
							String bookId = null;
							if('0' <= tmp.charAt(1) && tmp.charAt(1) <= '9')
								bookId = tmp.substring(0, 2);
							else
								bookId = tmp.substring(0, 1);
							
							m_curbook = books.findBookById(bookId);
						}
						catch(FileNotFoundException e){
							//e.printStackTrace();
							File file = new File(filename);
							return;
						}
						catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							File file = new File(filename);
							return;
						}
						
						if(m_curbook == null)
							return;
						
						OpenBook(m_curbook);
					}
				});
				
				final Button button_login = (Button)findViewById(R.id.BS_button_list_bottomlogin);
				if(Global.g_bLogin){
					button_login.setBackgroundDrawable(getResources().getDrawable(R.drawable.option_icon_logout));
					button_login.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							optionmenu.setVisibility(RelativeLayout.INVISIBLE);
							m_optionMenuOpened = false;
							
							PopupLogoutDialog();
						}
					});
				}
				else{
					button_login.setBackgroundDrawable(getResources().getDrawable(R.drawable.option_icon_login));
					button_login.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							optionmenu.setVisibility(RelativeLayout.INVISIBLE);
							m_optionMenuOpened = false;
							
							PopupLoginDialog();
						}
					});
				}*/
				
				
				final LinearLayout linear_back = (LinearLayout)findViewById(R.id.BS_lil_list_back);
				linear_back.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						optionmenu.setVisibility(RelativeLayout.INVISIBLE);
						m_optionMenuOpened = false;
					}
				});
			}
			else{
				optionmenu.setVisibility(RelativeLayout.INVISIBLE);
				m_optionMenuOpened = false;
			}
    	} 
		else if (keyCode == KeyEvent.KEYCODE_BACK) { // 옵션 메뉴가 보이는 중에 뒤로 가기를 누르면 옵션 메뉴만 숨김			
			if (m_optionMenuOpened == true) {
				optionmenu.setVisibility(RelativeLayout.INVISIBLE);
				m_optionMenuOpened = false;
        		
        		return true;
    		}
    	}
		
		return super.onKeyDown(keyCode, event);
	}
	
	
	public void init()
	{
		m_buy_id = new int [m_maxBookCount+1];
		
		m_buy_id[0] = R.id.BS_list_buy_book1;
		m_buy_id[1] = R.id.BS_list_buy_book2;
		m_buy_id[2] = R.id.BS_list_buy_book3;
		m_buy_id[3] = R.id.BS_list_buy_book4;
		m_buy_id[4] = R.id.BS_list_buy_book5;
		m_buy_id[5] = R.id.BS_list_buy_book6;
		m_buy_id[6] = R.id.BS_list_buy_book7;
		m_buy_id[7] = R.id.BS_list_buy_book8;
		m_buy_id[8] = R.id.BS_list_buy_book9;
		m_buy_id[9] = R.id.BS_list_buy_book10;
		m_buy_id[10] = R.id.BS_list_buy_book11;
		m_buy_id[11] = R.id.BS_list_buy_book12;
		m_buy_id[12] = R.id.BS_list_buy_book13;
		m_buy_id[13] = R.id.BS_list_buy_book14;
		m_buy_id[14] = R.id.BS_list_buy_book15;
		m_buy_id[15] = R.id.BS_list_buy_book16;
		m_buy_id[16] = R.id.BS_list_buy_book17;
		m_buy_id[17] = R.id.BS_list_buy_book18;
		m_buy_id[18] = R.id.BS_list_buy_book19;
		m_buy_id[19] = R.id.BS_list_buy_book20;
		m_buy_id[20] = R.id.BS_list_buy_book21;
		m_buy_id[21] = R.id.BS_list_buy_book22;
		m_buy_id[22] = R.id.BS_list_buy_book23;
		m_buy_id[23] = R.id.BS_list_buy_book24;
		m_buy_id[24] = R.id.BS_list_buy_book25;
		m_buy_id[25] = R.id.BS_list_buy_book26;
		m_buy_id[26] = R.id.BS_list_buy_book27;
		m_buy_id[27] = R.id.BS_list_buy_book28;
		m_buy_id[28] = R.id.BS_list_buy_book29;
		m_buy_id[29] = R.id.BS_list_buy_book30;
		m_buy_id[30] = R.id.BS_list_buy_book31;
		m_buy_id[31] = R.id.BS_list_buy_book32;
		m_buy_id[32] = R.id.BS_list_buy_book33;
		m_buy_id[33] = R.id.BS_list_buy_book34;
		m_buy_id[34] = R.id.BS_list_buy_book35;
		m_buy_id[35] = R.id.BS_list_buy_book36;
		m_buy_id[36] = R.id.BS_list_buy_book37;
		m_buy_id[37] = R.id.BS_list_buy_book38;
		m_buy_id[38] = R.id.BS_list_buy_book39;
		m_buy_id[39] = R.id.BS_list_buy_book40;
		m_buy_id[40] = R.id.BS_list_buy_book41;
		m_buy_id[41] = R.id.BS_list_buy_book42;
		m_buy_id[42] = R.id.BS_list_buy_book43;
		m_buy_id[43] = R.id.BS_list_buy_book44;
		m_buy_id[44] = R.id.BS_list_buy_book45;
		m_buy_id[45] = R.id.BS_list_buy_book46;
		m_buy_id[46] = R.id.BS_list_buy_book47;
		m_buy_id[47] = R.id.BS_list_buy_book48;
		m_buy_id[48] = R.id.BS_list_buy_book49;
		m_buy_id[49] = R.id.BS_list_buy_book50;
		
		
		m_bookname_id = new int [m_maxBookCount+1];
		
		m_bookname_id[0] = R.id.BS_list_book_name1;
		m_bookname_id[1] = R.id.BS_list_book_name2;
		m_bookname_id[2] = R.id.BS_list_book_name3;
		m_bookname_id[3] = R.id.BS_list_book_name4;
		m_bookname_id[4] = R.id.BS_list_book_name5;
		m_bookname_id[5] = R.id.BS_list_book_name6;
		m_bookname_id[6] = R.id.BS_list_book_name7;
		m_bookname_id[7] = R.id.BS_list_book_name8;
		m_bookname_id[8] = R.id.BS_list_book_name9;
		m_bookname_id[9] = R.id.BS_list_book_name10;
		m_bookname_id[10] = R.id.BS_list_book_name11;
		m_bookname_id[11] = R.id.BS_list_book_name12;
		m_bookname_id[12] = R.id.BS_list_book_name13;
		m_bookname_id[13] = R.id.BS_list_book_name14;
		m_bookname_id[14] = R.id.BS_list_book_name15;
		m_bookname_id[15] = R.id.BS_list_book_name16;
		m_bookname_id[16] = R.id.BS_list_book_name17;
		m_bookname_id[17] = R.id.BS_list_book_name18;
		m_bookname_id[18] = R.id.BS_list_book_name19;
		m_bookname_id[19] = R.id.BS_list_book_name20;
		m_bookname_id[20] = R.id.BS_list_book_name21;
		m_bookname_id[21] = R.id.BS_list_book_name22;
		m_bookname_id[22] = R.id.BS_list_book_name23;
		m_bookname_id[23] = R.id.BS_list_book_name24;
		m_bookname_id[24] = R.id.BS_list_book_name25;
		m_bookname_id[25] = R.id.BS_list_book_name26;
		m_bookname_id[26] = R.id.BS_list_book_name27;
		m_bookname_id[27] = R.id.BS_list_book_name28;
		m_bookname_id[28] = R.id.BS_list_book_name29;
		m_bookname_id[29] = R.id.BS_list_book_name30;
		m_bookname_id[30] = R.id.BS_list_book_name31;
		m_bookname_id[31] = R.id.BS_list_book_name32;
		m_bookname_id[32] = R.id.BS_list_book_name33;
		m_bookname_id[33] = R.id.BS_list_book_name34;
		m_bookname_id[34] = R.id.BS_list_book_name35;
		m_bookname_id[35] = R.id.BS_list_book_name36;
		m_bookname_id[36] = R.id.BS_list_book_name37;
		m_bookname_id[37] = R.id.BS_list_book_name38;
		m_bookname_id[38] = R.id.BS_list_book_name39;
		m_bookname_id[39] = R.id.BS_list_book_name40;
		m_bookname_id[40] = R.id.BS_list_book_name41;
		m_bookname_id[41] = R.id.BS_list_book_name42;
		m_bookname_id[42] = R.id.BS_list_book_name43;
		m_bookname_id[43] = R.id.BS_list_book_name44;
		m_bookname_id[44] = R.id.BS_list_book_name45;
		m_bookname_id[45] = R.id.BS_list_book_name46;
		m_bookname_id[46] = R.id.BS_list_book_name47;
		m_bookname_id[47] = R.id.BS_list_book_name48;
		m_bookname_id[48] = R.id.BS_list_book_name49;
		m_bookname_id[49] = R.id.BS_list_book_name50;
		
	}
}