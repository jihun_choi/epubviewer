package ybm.EpubViewer;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Xml.Encoding;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings.TextSize;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

public class Payment extends Activity {
	Payment self = this;
	
	class YSBAToeicReceiptResultData {
		public String regtempid;	//접수임시번호
		public String sisamid;		//결제MID
		public String price;		//결제금액
		public String next_url;		//P_NEXT_URL
		public String noti_url;		//P_NOTI_URL
		public String return_url;		//p_RETURN_URL
		public String product_name;		//사품명
	}
	
	YSBAToeicReceiptResultData infoData;
	String nameString;					//결제자명
	String mobileString;					//전화
	String emailString;					//메일
	
	// JavaScript 통신
	private final Handler handler = new Handler();
	private class AndroidBridge {
		public void setMessage(final String arg){
			handler.post(new Runnable()
			{
				public void run()
				{
					//Log.e(getLocalClassName(), "[callYBM]>>>>> " + "setMessage(" + arg + ")");
					
					// 웹에서 실행 방법
					// 취소 : window.callOPIcApp_A.setMessage("PayCancel");
					// 완료 : window.callOPIcApp_A.setMessage("PayEnd");
					
					// 결제 취소일 경우, 웹뷰 종료
					if (arg.equals("PayCancel"))
					{
						finish();
					}
					// 결제 종료일 경우
					else if (arg.equals("PayEnd"))
					{
						// Mypage로 이동
						//startActivity(new Intent(mMainContext, Mypage.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
						finish();
					}
					else
					{}
				}
			});
		}
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    setContentView(R.layout.payment);
	   
	    final Button button_ret = (Button)findViewById(R.id.PM_button_back);
	    button_ret.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_ret.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_kreturn_b));
				}
				else{
					button_ret.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_kreturn_a));
					finish();
				}
				return false;
			}
		});
	    
	    Intent i = new Intent(getIntent());
	    final String bookId = i.getStringExtra("bookId");
	    final String bookPrice = i.getStringExtra("bookPrice"); // 결제금액 테스트를 위해서 여기를 "1"로 하면 됨
	    final String bookName = i.getStringExtra("bookName");
	    
	    String XmlUrl = Global.g_PaymentXMLURL + "?contents_id=" + bookId + "&user_id=" + Global.g_LoginID;
	    String res = Library.HttpGetByString("http://" + Global.g_PaymentXMLHost + XmlUrl);
	    
	    //if (res != null) Log.i("PAYMENT", res);
	    
	    if (res == null) {
	    	new AlertDialog.Builder(this)
				.setTitle("알림")
				.setMessage("결제 정보를 얻어올 수 없습니다.")
				.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// 돌아가기
						finish();
					}
				})
				.show();
	    	return;
	    }
	    
	    String oid = res.substring(res.indexOf("<OrderID>") + 9, res.indexOf("</OrderID>"));
	    
	    // Log.i("PAYMENT", oid);
	    
	    infoData = new YSBAToeicReceiptResultData();
	    infoData.regtempid = oid;	//접수임시번호"
		infoData.sisamid = "ybmsisa440";		//결제MID
		infoData.price = bookPrice;		//결제금액
		infoData.next_url = 
			"http://www.ybmmobile.com/appstore/epub/inipay/mobilestore_visa3d_next.asp?oid=" + oid + "%%26id=" + Global.g_LoginID; //P_NEXT_URL
		infoData.noti_url = 
			"http://www.ybmmobile.com/appstore/epub/inipay/mobilestore_isp_noti.asp?id=" + Global.g_LoginID; //P_NOTI_URL
		infoData.return_url = 
			"http://www.ybmmobile.com/appstore/epub/inipay/mobilestore_isp_return.asp?oid=" + oid + "%%26id=" + Global.g_LoginID; //p_RETURN_URL
		infoData.product_name = "[wBook]" + bookName; //사품명
		
		nameString = Global.g_LoginName;
		mobileString = "";
		emailString = Global.g_LoginEmail;
		
		dispDailyContents();
	}
	
	void dispDailyContents() {
		final String url = "https://mobile.inicis.com/smart/wcard/";
		
		final String postString =
			"P_MID=" + infoData.sisamid +
			"&P_OID=" + infoData.regtempid +
			"&P_RETURN_URL=" + infoData.return_url +
			"&P_AMT=" + infoData.price +
			"&P_UNAME=" + nameString +
			"&P_NOTI=" + infoData.regtempid +
			"&P_NEXT_URL=" + infoData.next_url +
			"&P_NOTI_URL=" + infoData.noti_url +
			"&P_GOODS=" + infoData.product_name +
			"&P_MOBILE=" + mobileString + 
			"&P_EMAIL=" + emailString +
			"&P_CANCEL_URL=http://www.ybmmobile.com/appstore/epub/inipay/mobilestore_cancel.asp" +
			"&P_RESERVED=device=tablet" +
			"&P_HPP_METHOD=1" +
			"&P_MNAME=YBM시사닷컴";
		
		// Log.i("PAYMENT", postString);
		
		try {
			final WebView webView = (WebView)findViewById(R.id.PM_webview);
			webView.getSettings().setJavaScriptEnabled(true);
			webView.addJavascriptInterface(new AndroidBridge(), "callOPIcApp_A");
			webView.getSettings().setTextSize(TextSize.NORMAL);
			webView.getSettings().setDefaultZoom(ZoomDensity.FAR);
			webView.setWebViewClient(new MyWebViewClient());
		    webView.postUrl(url, postString.getBytes("euc-kr"));
		} catch (Exception e) {
			Log.i("PAYMENT", e.toString());
			
			// 결제 오류를 띄움
			new AlertDialog.Builder(this)
				.setTitle("결제 오류")
				.setMessage(e.toString())
				.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// 돌아가기
						finish();
					}
				})
				.show();
		}
	}
	
	class MyWebViewClient extends WebViewClient {
		@Override
		public void onPageFinished(WebView view, String url) {
			if(url.indexOf("pay_cancel.php") > 0) {
				finish();
			}
			
			if(url.indexOf("payend.asp") >= 0){
				new AlertDialog.Builder(Payment.this)
				.setTitle("알림")
				.setMessage("결제 성공")
				.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				})
				.show();
			} else if(url.indexOf("paycancel.asp") >= 0) {
				new AlertDialog.Builder(Payment.this)
				.setTitle("알림")
				.setMessage("결제 취소")
				.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				})
				.show();
			}			
			super.onPageFinished(view, url);
		}
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.i("PAYMENT", "url change: " + url);
			
			Uri uri = Uri.parse(url);
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			
			if(url.indexOf("pay_cancel.php") > 0) {
				finish();
				return true;
			}

			try	{
				startActivity(intent);

				//삼성카드 안심클릭을 위해 추가
				if(url.startsWith("ispmobile://"))	{
					finish();
				}
			} catch(ActivityNotFoundException e) {
				//url prefix가 ispmobile 일 경우만 alert를 띄움
				if(url.startsWith("ispmobile://")) {
					final WebView myView = view;
					view.loadData("<html><body></body></html>", "text/html", "euc-kr");
					new AlertDialog.Builder(self)
					.setTitle("알림")
					.setMessage("모바일 ISP 어플리케이션이 설치되어 있지 않습니다.\n설치를 눌러 진행 해 주십시요.\n취소를 누르면 결제가 취소 됩니다.")
					.setPositiveButton("설치", new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which)
						{
							// ISP 설치 페이지 URL
							myView.loadUrl("http://mobile.vpay.co.kr/jsp/MISP/andown.jsp");
							finish();
						}
					})
					.setNegativeButton("취소", new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which)
						{
							Toast.makeText(self, "(-1)결제를 취소 하셨습니다.", Toast.LENGTH_SHORT).show();
							finish();
						}
					})
					.show();
					return false;
				}
			}
			
			return true;
		}
	}
}

/*List<NameValuePair> postData = new ArrayList<NameValuePair>();
postData.add(new BasicNameValuePair("P_MID", infoData.sisamid));
postData.add(new BasicNameValuePair("P_OID", infoData.regtempid));
postData.add(new BasicNameValuePair("P_RETURN_URL", infoData.return_url));
postData.add(new BasicNameValuePair("P_AMT", infoData.price));
postData.add(new BasicNameValuePair("P_UNAME", nameString));
postData.add(new BasicNameValuePair("P_NOTI", infoData.regtempid));
postData.add(new BasicNameValuePair("P_NEXT_URL", infoData.next_url));
postData.add(new BasicNameValuePair("P_NOTI_URL", infoData.noti_url));
postData.add(new BasicNameValuePair("P_GOODS", infoData.product_name));
postData.add(new BasicNameValuePair("P_MOBILE", mobileString));
postData.add(new BasicNameValuePair("P_EMAIL", emailString));
postData.add(new BasicNameValuePair("P_CANCEL_URL", "http://www.ybmmobile.com/appstore/epub/inipay/mobilestore_cancel.asp"));
postData.add(new BasicNameValuePair("P_RESERVED", "device=tablet"));
postData.add(new BasicNameValuePair("P_HPP_METHOD", "1"));
postData.add(new BasicNameValuePair("P_MNAME", "YBM시사닷컴"));

String res = Library.HttpPostByString(url, postData, "euc-kr");

if (res == null) {
	new AlertDialog.Builder(this)
		.setTitle("알림")
		.setMessage("결제 요청 결과를 얻어올 수 없습니다.")
		.setPositiveButton("확인", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// 돌아가기
				finish();
			}
		})
		.show();
	return;
}

Log.i("PAYMENT", res);*/
