package ybm.EpubViewer;

import java.io.File;
import java.io.IOException;

import ybm.EpubViewer.EPubBook.MemoInfo;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.media.MediaRecorder;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class WordMenuView
{
	static Context m_context;
	static Activity m_activity;
	//static 
	
	public static void OpenWordMenu(Context context, final OnePageViewer parent, final int top, final int left)
	{
		m_context = context;
		m_activity = (OnePageViewer)context;
		
		//View m_topbar_menu;
		//final View m_wordmenu;
		
		final LayoutInflater inflater = (LayoutInflater)m_context.getSystemService(m_activity.LAYOUT_INFLATER_SERVICE);
		final RelativeLayout back = (RelativeLayout)m_activity.findViewById(R.id.MM_rel_back);
		
		//RemoveTopBarMenu();
		
		//m_topbar_menu_type = 1;
		
		//m_topbar_menu = inflater.inflate(R.layout.fontmenu, null);
		
		Global.g_curWordMenu = inflater.inflate(R.layout.wordmenu, null);
		back.addView(Global.g_curWordMenu);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)Global.g_curWordMenu.getLayoutParams();
		params.topMargin = top;
		params.leftMargin = left;
		
		// ------------- 녹음 음성 메모 부분 ---------------------
        Button rec = (Button)m_activity.findViewById(R.id.WM_button_rec);
        rec.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				back.removeView(Global.g_curWordMenu);
				Global.g_curWordMenu = null;
					
				View recmenu = inflater.inflate(R.layout.recmenu, null);
				back.addView(recmenu);
				Global.g_curWordMenu = recmenu;
		        
		        RelativeLayout menu = (RelativeLayout)m_activity.findViewById(R.id.RC_recmenu);
		        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)menu.getLayoutParams();
		        
		        params.topMargin = top;
		        params.leftMargin = left;
		        
		        // 녹음 파일을 저장할 위치 -> /data/ybm 에 하고 있음
				// File file = Environment.getExternalStorageDirectory();
		        
		        
				//final String path = file.getAbsolutePath() + "/data/ybm/" + System.nanoTime() + ".3gp";
		        final String path = m_activity.getFilesDir().getAbsolutePath() + "/" + System.nanoTime() + ".3gp";
				File rec_file = null;
		        
				// final String path = file.getAbsolutePath() + "/data/ybm/" + System.nanoTime() + ".3gp";
		        //final String path = Util.absolutePath + "/" + System.nanoTime() + ".3gp"; // <- 여기서 정한 경로는 퍼미션 때매 못 읽음
		        
				
		        final Button button_rec = (Button)m_activity.findViewById(R.id.RC_button_rec);		        
		        button_rec.setOnTouchListener(new View.OnTouchListener() {
		        	// ------- 녹음 시작 버튼을 클릭했을 경우 (빨간 동그라미 버튼)
					public boolean onTouch(View v, MotionEvent event) {
						if(event.getAction() == MotionEvent.ACTION_DOWN){
							Log.i("debug", "clicked Rec Button");
							button_rec.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_rcd_b));
							
							try {
								Global.g_curRecorder = new MediaRecorder();
								
								// 녹음 시작
								Global.g_curRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
								Global.g_curRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
								Global.g_curRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
								Global.g_curRecorder.setOutputFile(path);
								Global.g_curRecorder.prepare();
								Global.g_curRecorder.start();
								
								Global.g_curRecordingPath = path;
							} catch (IOException e) {
								new AlertDialog.Builder(m_context)
								.setTitle("녹음 시작 실패")
								.setMessage(e.toString())
								.setPositiveButton("확인", null)
								.show();

								button_rec.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_st_a));
							
							}
							
						}
						else{
							button_rec.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_st_a));
							button_rec.setOnTouchListener(new View.OnTouchListener() {
								
								public boolean onTouch(View v, MotionEvent event) {
									if(event.getAction() == MotionEvent.ACTION_DOWN){
										button_rec.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_st_b));
									}
									else{
										button_rec.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_rcd));
										back.removeView(Global.g_curWordMenu);
										
										try {
											// 녹음 중지
											Global.g_curRecorder.stop();
											
											// 메모를 지정
											MemoInfo memo = new MemoInfo();
											memo.set(1, Global.g_SelMin, Global.g_SelMax, path);
											Global.g_CurEpubBook.AddMemo(memo);
										} catch (Exception e) {
											new AlertDialog.Builder(m_context)
											.setTitle("저장 실패")
											.setMessage(e.toString())
											.setPositiveButton("확인", null)
											.show();
										} finally {
											Global.g_curRecorder.release();
											
											Global.g_curRecordingPath = null;
											
											if(Global.g_bShowTwoPages == false)
												Global.g_curOnePageViewer.ReleaseSelection();
											else
												Global.g_curTwoPageViewer.ReleaseSelection();
										}
									}
									
									return false;
								}
							});
						}							
						
						return false;
					}
				});
				
				// ------------- 녹음 부분 작성 ---------------
				
				// 기존 메모는 지워버림.
				//if(Global.g_curMemo != null){
				//	Global.g_CurEpubBook.DeleteMemo(Global.g_curMemo);
				//	Global.g_curMemo = null;
				//}
				
				//ReleaseSelection();
				
				
			}
		});
		
		String word = Global.g_CurEpubBook.GetTextInSelectionFrom(
				Global.g_SelMin, Global.g_SelMax);
		
		int flag=0;
		for(int i=0;i<=word.length()-1;i++){
			if(word.charAt(i) > 128){
				flag = 1;
				break;
			}
		}
        
		Button dic = (Button)m_activity.findViewById(R.id.WM_button_dic);
		if(flag == 1){
			LinearLayout m = (LinearLayout)m_activity.findViewById(R.id.WM_wordmenu);
			m.removeView(dic);
		}
		else{
	        dic.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					back.removeView(Global.g_curWordMenu);
					
					// 한글이 있는 경우 사전 검색을 하지 않는다.
					String word = Global.g_CurEpubBook.GetTextInSelectionFrom(
							Global.g_SelMin, Global.g_SelMax);
					
					for(int i=0;i<=word.length()-1;i++){
						if(word.charAt(i) > 128)
							return;						
					}
					
					View dicmenu = inflater.inflate(R.layout.worddic, null);
					back.addView(dicmenu);
					Global.g_curWordMenu = dicmenu;
					
					LinearLayout dictionary_bar = (LinearLayout)m_activity.findViewById(R.id.MM_rel_dictionary);
					dictionary_bar.setVisibility(View.VISIBLE);
					
					Button goSearch = (Button)m_activity.findViewById(R.id.DIC_go_search);
					goSearch.setOnClickListener(new View.OnClickListener() {
						
						public void onClick(View v) {
							WebView webview = (WebView)m_activity.findViewById(R.id.DIC_webview);		
							EditText editText = (EditText)m_activity.findViewById(R.id.DIC_edittext);
							String word = editText.getText().toString();
							
					        webview.loadUrl("http://www.ybmallinall.com/calldic/mobile.asp?kwd=" + word);
					        
					        InputMethodManager mgr = (InputMethodManager)m_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
							mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
						}
					});
					
					/*final EditText editText = (EditText)m_activity.findViewById(R.id.DIC_edittext);		        
					editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
						
						public void onFocusChange(View v, boolean hasFocus) {
							if(hasFocus == false){
								// 포커스를 잃었을 경우 키보드도 없앤다.
								InputMethodManager mgr = (InputMethodManager)m_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
								mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
							}
						}
					});*/
					
					parent.tabbar_upward_setAnimationListener();
					parent.m_topbar_menu_type = 4;
					parent.RemoveTopBarMenu(); // Top Bar와 연관된 메뉴 지움.
					parent.m_topbar_menu_type = 5;
					
					
					/* tabbar.startAnimation(tabbar_upward);
					RemoveTopBarMenu(); // Top Bar와 연관된 메뉴 지움. */
					
					LinearLayout menu = (LinearLayout)m_activity.findViewById(R.id.DIC_menu);
			        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)menu.getLayoutParams();		      		        
			        
			        if(Global.g_bShowTwoPages == false)
			        {
			        	params.topMargin = 160;
			        	params.leftMargin = 0;
			        	params.height = (int)( m_activity.getWindowManager().getDefaultDisplay().getHeight() - params.topMargin );
			        	params.width = m_activity.getWindowManager().getDefaultDisplay().getWidth();
			        }
			        else{
			        	params.topMargin = 65;
			        	params.leftMargin = m_activity.getWindowManager().getDefaultDisplay().getWidth()/2;
			        	params.height = m_activity.getWindowManager().getDefaultDisplay().getHeight()-100;
			        	params.width = params.leftMargin;
			        }
			        
			        menu.setLayoutParams(params);
			        
			        WebView webview = (WebView)m_activity.findViewById(R.id.DIC_webview);		        
			        webview.loadUrl("http://www.ybmallinall.com/calldic/mobile.asp?kwd=" + word);
			        
				}
			});
		}
        
        Button highlt = (Button)m_activity.findViewById(R.id.WM_button_highlight);
        highlt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				back.removeView(Global.g_curWordMenu);
				
				if(Global.g_curMemo != null){
					Global.g_CurEpubBook.DeleteMemo(Global.g_curMemo);
					Global.g_curMemo = null;
				}
				
				MemoInfo memo = new MemoInfo();
				memo.set(2, Global.g_SelMin, Global.g_SelMax, null); // 2 - 형광펜
				Global.g_CurEpubBook.AddMemo(memo);
				
				if(Global.g_bShowTwoPages == false)
					Global.g_curOnePageViewer.ReleaseSelection();
				else
					Global.g_curTwoPageViewer.ReleaseSelection();
			}
		});
        
        Button memo = (Button)m_activity.findViewById(R.id.WM_button_memo); // 메모하기 버튼을 눌렀을 때
        memo.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				back.removeView(Global.g_curWordMenu);

				View memomenu = inflater.inflate(R.layout.wordmemo, null);
				back.addView(memomenu);
				Global.g_curWordMenu = memomenu;
				
				LinearLayout menu = (LinearLayout)m_activity.findViewById(R.id.MEMO_menu);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)menu.getLayoutParams();
		        
				/*
		        if(top > 250)
		        	params.topMargin = 250;
		        else
		            params.topMargin = top;
		        
		        params.leftMargin = left;*/		        		        
		        
		        if(Global.g_bShowTwoPages == false){		        	
		        	params.topMargin = 0;
		        	params.leftMargin = 0;
		        	// params.height = m_activity.getWindowManager().getDefaultDisplay().getHeight()/2;
		        	// params.width = m_activity.getWindowManager().getDefaultDisplay().getWidth();
		        }
		        else{
		        	params.topMargin = 75;
		        	params.leftMargin = m_activity.getWindowManager().getDefaultDisplay().getWidth()/2;
		        	params.height = m_activity.getWindowManager().getDefaultDisplay().getHeight()-140;
		        	params.width = params.leftMargin;
		        }
		        
		        menu.setLayoutParams(params);
		        
		    	parent.tabbar_upward_setAnimationListener();				
				//parent.RemoveTopBarMenu(); 
				// parent.m_topbar_menu_type = 5;*/
				
		        final EditText edittext = (EditText)m_activity.findViewById(R.id.MEMO_edittext);		            
				edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					
					public void onFocusChange(View v, boolean hasFocus) {
						if(hasFocus == false){
							// 포커스를 잃었을 경우 키보드도 없앤다.
							InputMethodManager mgr = (InputMethodManager)m_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
							mgr.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
							
						}
					}
				});
		        
		        
		        if(Global.g_curMemo != null && Global.g_curMemo.type == 0){		        	
		        	//EditText edittext = (EditText)findViewById(R.id.MEMO_edittext);
		        	edittext.setText(Global.g_curMemo.content);
		        }
		        
    
		        Button button_ok = (Button)m_activity.findViewById(R.id.MEMO_button_ok);
		        button_ok.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						
						if(Global.g_curMemo != null){
							Global.g_CurEpubBook.DeleteMemo(Global.g_curMemo);
							Global.g_curMemo = null;
						}
						
						EditText edittext = (EditText)m_activity.findViewById(R.id.MEMO_edittext);
						String text = edittext.getText().toString();
						
						MemoInfo memo = new MemoInfo();
						memo.set(0, Global.g_SelMin, Global.g_SelMax, text);
						Global.g_CurEpubBook.AddMemo(memo);				
						
						if(Global.g_bShowTwoPages == false)
							Global.g_curOnePageViewer.ReleaseSelection();
						else
							Global.g_curTwoPageViewer.ReleaseSelection();
					}
				});
		        
		        Button button_back = (Button)m_activity.findViewById(R.id.MEMO_button_back);
		        button_back.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {	
						
						if(Global.g_curMemo != null){
							Global.g_CurEpubBook.DeleteMemo(Global.g_curMemo);
							Global.g_curMemo = null;
						}
						
						if(Global.g_bShowTwoPages == false)
							Global.g_curOnePageViewer.ReleaseSelection();
						else
							Global.g_curTwoPageViewer.ReleaseSelection();					
					}
				});
				
		        Button button_cancel = (Button)m_activity.findViewById(R.id.MEMO_button_cancel);
		        button_cancel.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {	
						if(Global.g_curMemo != null){
							Global.g_CurEpubBook.DeleteMemo(Global.g_curMemo);
							Global.g_curMemo = null;
						}
						
						if(Global.g_bShowTwoPages == false){
							Global.g_curOnePageViewer.ReleaseSelection();
							Global.g_curOnePageViewer.RemoveMenu();
						}
						else{
							Global.g_curTwoPageViewer.ReleaseSelection();
							Global.g_curTwoPageViewer.RemoveMenu();						
						}
					}
				});
			}
        });
	}
}