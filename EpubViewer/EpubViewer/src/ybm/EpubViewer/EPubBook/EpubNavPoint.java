package ybm.EpubViewer.EPubBook;

public class EpubNavPoint {
	String navId; //navPoint Tag안의 id;
	int	playOrder = 0; //차례의 순서 
	String navClass; //있는것도 있고 없는 것도 있고..어떻게 처리 해야할지..
	
	public int showLevel = 0;//몇 단계 밑에 있는 차례인가.. 예) 큰 차례 밑에 있는 중 차례 
	int labelType = 0 ; //0 : text
	public String label;//차례에서 보여줘야 할 텍스트 
	
	public String srcFile;//이 nav의 링크 파일
	public String srcAnchor;//파일에서도 anchor가 걸려있는 부분 처음부터라면 nil;

	void SetContent(String src)
	{
		int idx = src.indexOf('#');
		if	(src.indexOf('#') != -1)
		{
			srcFile = src.substring(0, src.indexOf('#'));
			srcAnchor = src.substring(idx);
		}
		else 
		{
			srcFile = src;
			srcAnchor = null;
		}
	}
}
