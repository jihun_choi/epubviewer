package ybm.EpubViewer;

public class NFEncode {
	public static String Base64encode(StringBuffer content)
	{
		String sBASE_64_CHARACTERS = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
		StringBuffer asContents = new StringBuffer(content);
		
		if (asContents.length() % 3 != 0)
		{
			int iMax = 3 - (asContents.length() % 3);
			for(int i = 0; i < iMax; ++i)
				asContents.append(" ");
		}
		
		StringBuffer lsResult = new StringBuffer();
		for(int lnPosition = 0; lnPosition < asContents.length(); lnPosition +=3)
		{
			char Byte0 = asContents.charAt(lnPosition);
			char Byte1 = asContents.charAt(lnPosition + 1);
			char Byte2 = asContents.charAt(lnPosition + 2);
			
			int SaveBit1 = Byte0 & 3;
			int SaveBit2 = Byte1 & 15;
			
			char Char1 = sBASE_64_CHARACTERS.charAt(((Byte0 & 252) / 4));
			char Char2 = sBASE_64_CHARACTERS.charAt(((Byte1 & 240) / 16) | (SaveBit1 * 16) & 0xFF);
			char Char3 = sBASE_64_CHARACTERS.charAt(((Byte2 & 192) / 64) | (SaveBit2 * 4) & 0xFF);
			char Char4 = sBASE_64_CHARACTERS.charAt(Byte2 & 63);
			
			lsResult.append(Char1);
			lsResult.append(Char2);
			lsResult.append(Char3);
			lsResult.append(Char4);
		}
		return lsResult.toString();
	}

	public static String nfencode(final String sText)
	{
		String NFKEY= "yBMN39iN1E9siSAFrut7201CoMA99pP";
		int cntData = sText.length() - 1;
		int cntCode = NFKEY.length() - 1;
		
		StringBuffer arrData = new StringBuffer();
		StringBuffer arrCode = new StringBuffer();
		
		for (int i = 0; i <= cntData; ++i)
			arrData.append(sText.charAt(i));
		
		for (int i = 0; i <= cntCode; ++i)
			arrCode.append(NFKEY.charAt(i));
		
		int flag = 0;
		StringBuffer strResult = new StringBuffer();
		for (int i = 0; i <= cntData; ++i)
		{
			Integer val = (arrData.charAt(i) ^ arrCode.charAt(flag));
			strResult.append(val.toString());
			strResult.append("!");
			if (flag == cntCode)
				flag = 0;
			else
				++flag;		
		}
		
		String ret = Base64encode(strResult);
		
		return ret;
	}

}
