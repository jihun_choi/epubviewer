package ybm.EpubViewer;

import org.apache.http.util.ByteArrayBuffer;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.widget.ImageView;

public class BookForLibrary 
{
	String bookId;
	String bookName;
	String bookCover;
	String bookUrl;
	Bitmap bookCoverImage;
	String bookPrice;
	Rect	bookRect;
	Boolean isPurchased;
	
	public int layout_Idx; // Library.java���� ���.
	
	public BookForLibrary()
	{
		
	}
	
	public ByteArrayBuffer GetRawData()
	{
		char NullByte = '\0';

		ByteArrayBuffer[] baf = new ByteArrayBuffer[5];
		for(int i = 0; i < 5; ++i)
			baf[i] = new ByteArrayBuffer(20);
		baf[0].append(bookId.getBytes(), 0, bookId.length());
		baf[1].append(bookCover.getBytes(), 0, bookCover.length());
		baf[2].append(bookName.getBytes(), 0, bookName.length());
		baf[3].append(bookUrl.getBytes(), 0, bookUrl.length());
		baf[4].append(bookPrice.getBytes(), 0, bookPrice.length());

		ByteArrayBuffer ret = new ByteArrayBuffer(100);
		for(int i = 0; i < 5; ++i)
		{
			ret.append(baf[i].buffer(), 0, baf[i].length());
			ret.append(NullByte);
		}		
		return ret;
	}
	
	public void CreateWithData(ByteArrayBuffer data)
	{
		byte[] bytes = data.buffer();
		int lastnull = 0 ;
		int order = 0;
		for(int i = 0; i < data.length(); ++i)
		{
			if (bytes[i] == '\0')
			{
				ByteArrayBuffer aData = new ByteArrayBuffer(i - lastnull);
				aData.append(bytes, lastnull, i - lastnull);
				if (order == 0)
					bookId = new String(aData.buffer());
				else if (order == 1)
					bookCover = new String(aData.buffer());
				else if (order == 2)
					bookName = new String(aData.buffer());
				else if (order == 3)
					bookUrl = new String(aData.buffer());
				else if (order == 4)
					bookPrice = new String(aData.buffer());		
				lastnull = i + 1;
				order ++;
			}
		}
	}
}


