package ybm.EpubViewer;

import java.util.ArrayList;

import ybm.EpubViewer.EPubBook.HtmlContent;
import ybm.EpubViewer.EPubBook.SearchResult;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;



public class SearchView
{
	public static ArrayList<SearchResult> m_Result;
	public static ArrayList<String> m_arrayData;
	public static String strSearchWord = null;
	
	static TableLayout m_table;
	
	static Context m_context;
	static Activity m_activity;
	
	public static View OpenSearchMenu(Context context)
	{
		//RemoveTopBarMenu();
		
		if(Global.g_autoState == true){
			AlertDialog.Builder aDialog = new AlertDialog.Builder(context);
			aDialog.setTitle("메뉴 열기 실패");
			aDialog.setMessage("자동 넘김 중에는 이 기능을 사용할 수 없습니다.");
			aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

			AlertDialog ad = aDialog.create();
			ad.show();	
			
			return null;
		}
		
		Activity activity = (Activity)context;
		m_context = context;
		m_activity = activity;
		
		View m_topbar_menu;
		
		final LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		final RelativeLayout back = (RelativeLayout)activity.findViewById(R.id.MM_rel_back);

		m_topbar_menu = inflater.inflate(R.layout.search, null);
		back.addView(m_topbar_menu);
		
		final EditText edittext = (EditText)activity.findViewById(R.id.SC_edittext);
		// edittext.setBackgroundColor(Color.TRANSPARENT);
		edittext.setTextSize(14);
		edittext.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		edittext.setHint("단어 입력 후 검색버튼을 누르세요.");
		edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				
				if(actionId == EditorInfo.IME_ACTION_SEARCH){
					String str = edittext.getText().toString();
					if(str.length() <= 1)
						return false;
					
					
					Global.g_CurEpubBook.resetSearchedTexts();
					
					m_Result = new ArrayList<SearchResult> ();
					
					strSearchWord = str;
					int i;
					for(i=0;i<=Global.g_CurEpubBook.htmlcontents.size()-1;i++){
						HtmlContent content = Global.g_CurEpubBook.htmlcontents.get(i);					
						content.search(str);
					}
					
					addSearchResult();
				}
				return false;
			}
		});
		
		/*
		edittext.setOnKeyListener(new OnKeyListener() {
	    	
	        public boolean onKey(View v, int keyCode, KeyEvent event) {
	            if (keyCode == KeyEvent.KEYCODE_ENTER) {
	            	String str = edittext.getText().toString();
					if(str.length() <= 1)
						return false;
					
					
					Global.g_CurEpubBook.resetSearchedTexts();
					
					m_Result = new ArrayList<SearchResult> ();
					
					strSearchWord = str;
					int i;
					for(i=0;i<=Global.g_CurEpubBook.htmlcontents.size()-1;i++){
						HtmlContent content = Global.g_CurEpubBook.htmlcontents.get(i);					
						content.search(str);
					}
					
					addSearchResult();
					return true;
	            }
	            return false;
	        }
	    });
		*/
		
		edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus == false){
					// 포커스를 잃었을 경우 키보드도 없앤다.
					InputMethodManager mgr = (InputMethodManager)m_context.getSystemService(Context.INPUT_METHOD_SERVICE);
					mgr.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
					
				}
			}
		});
		
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)m_topbar_menu.getLayoutParams();
		params.topMargin = 75;
		if(Global.g_bShowTwoPages == false)
			params.leftMargin = 0;
		else
			params.leftMargin = 700;
		
		//m_topbar_menu_type = 2;
		
		m_table = (TableLayout)activity.findViewById(R.id.SC_table);
		m_table.setStretchAllColumns(true);
		
		final Button button_rem = (Button)activity.findViewById(R.id.SC_button_rem);
		button_rem.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				edittext.setText("");
			}
		});
		
		
		final Button button_search = (Button)activity.findViewById(R.id.SC_button_search);
		button_search.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String str = edittext.getText().toString();
				if(str.length() <= 1)
					return;
				
				
				Global.g_CurEpubBook.resetSearchedTexts();
				
				m_Result = new ArrayList<SearchResult> ();
				
				strSearchWord = str;
				int i;
				for(i=0;i<=Global.g_CurEpubBook.htmlcontents.size()-1;i++){
					HtmlContent content = Global.g_CurEpubBook.htmlcontents.get(i);					
					content.search(str);
				}
				
				addSearchResult();
			}
		});
		
		return m_topbar_menu;
	}
	
	public static void addSearchResult()
	{	
		Log.i("debug", "m_Result size " + m_Result.size());
		
		if(m_Result.isEmpty() == true){
			// alert..
			Log.i("debug", "m_Result size return");
			return;
		}
		
		RelativeLayout rel = (RelativeLayout)m_activity.findViewById(R.id.SC_rel_result);		
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)rel.getLayoutParams();
		if(Global.g_bShowTwoPages == false)
			params.height = 700;
		else
			params.height = 500;
		rel.setLayoutParams(params);
		
		m_table.removeAllViews();
		
		int i;
		for(i=0;i<=m_Result.size()-1;i++){
			SearchResult ret = m_Result.get(i);
			
			int startPage = Global.g_CurEpubBook.GetStartPageOfContent(ret.content);
			String strPageNo = (startPage + ret.pageNoOfContent + 1) + "";
			
			addTableRow(ret.resStr, strPageNo, startPage + ret.pageNoOfContent);
		}
		
		m_Result = new ArrayList<SearchResult> ();
	}
	
	static void addTableRow(String text, String tag, final int page)
	{
	    RelativeLayout rel = new RelativeLayout(m_context);
	    RelativeLayout.LayoutParams params;
	     
	    TextView word = new TextView(m_context);
	    word.setText(text);
	    word.setTextSize(12);
	    word.setTextColor(Color.BLACK);	    
	    
	    params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
	    
	    rel.addView(word, params);
	    
	    TextView pos = new TextView(m_context);
	    pos.setText(tag);
	    pos.setTextSize(12);
	    pos.setTextColor(Color.BLUE);
	    
	    params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

	    rel.addView(pos, params);

	    TableRow tr = new TableRow(m_context);
	    
	    tr.addView(rel, new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
	    
	    m_table.addView(tr, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
	    
	    // 클릭시 이동?
	    rel.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Global.g_CurEpubBook.curPage = page;
				
		    	//InputMethodManager mgr = (InputMethodManager)m_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		    	//mgr.hideSoftInputFromWindow(null, InputMethodManager.RESULT_HIDDEN);
				
				if(Global.g_bShowTwoPages == false){
					Global.g_curOnePageViewer.refreshPage();
				}
				else
				{
					Global.g_curTwoPageViewer.refreshPage();
				}
			}
		});
	}
}