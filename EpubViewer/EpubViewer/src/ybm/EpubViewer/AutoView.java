package ybm.EpubViewer;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;



public class AutoView
{
	static Context m_context;
	static Activity m_activity;
	
	static int tmp_state = 20;
	
	public static View OpenAutoMenu(Context context, final Handler m_handler)
	{
		final Activity activity = (Activity)context;
		m_context = context;
		m_activity = activity;
		
		View m_topbar_menu;
		
		final LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		final RelativeLayout back = (RelativeLayout)activity.findViewById(R.id.MM_rel_back);
		
		m_topbar_menu = inflater.inflate(R.layout.automenu, null);
		back.addView(m_topbar_menu);
		
		final SeekBar seekbar = (SeekBar)activity.findViewById(R.id.AM_seekbar);
		seekbar.setMax(320); // 최대 5분
		seekbar.setProgress(Global.g_autoTime);
		
		TextView textview = (TextView)m_activity.findViewById(R.id.AM_textview_time);
		int min = Global.g_autoTime/60;
		int sec = Global.g_autoTime - (min*60);
		textview.setText(min + "분 " + sec + "초");
		
		tmp_state = Global.g_autoTime;
		
		seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				
				if(tmp_state < progress)
					progress = tmp_state + 20;
				else if(tmp_state > progress)
					progress = tmp_state - 20;
				
				if(progress == 0)
					progress = 20;
				
				seekbar.setProgress(progress);
				
				tmp_state = progress;
				
				TextView textview = (TextView)m_activity.findViewById(R.id.AM_textview_time);
				int min = progress/60;
				int sec = progress - (min*60);
				textview.setText(min + "분 " + sec + "초");
			}
		});
		
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)m_topbar_menu.getLayoutParams();
		params.topMargin = 75;
		if(Global.g_bShowTwoPages == false)
			params.leftMargin = 0;
		else
			params.leftMargin = 730;
		
		final Button button_auto = (Button)activity.findViewById(R.id.AM_button);
		button_auto.setOnTouchListener(null);
		
		if(Global.g_autoState == false){
			// button_auto.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.start));
			button_auto.setText("시작");
			button_auto.setOnTouchListener(new View.OnTouchListener() {
				
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction() == MotionEvent.ACTION_DOWN){
						// button_auto.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.start_b));
						button_auto.setText("시작");
					}
					else{
						// button_auto.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.stop));
						button_auto.setText("중지");
						Global.g_autoTime = seekbar.getProgress();						
						Global.g_autoState = true;
						
						Thread autoThread = new Thread(new Runnable() {
							public void run()
							{
								long saveTime = System.currentTimeMillis();
								long curTime;
								
								while(Global.g_autoState == true){
									try{
										// 정확한 시간 카운트가 안됨.
										//Thread.sleep(Global.g_autoTime * 1000);
										curTime = System.currentTimeMillis();
										while(curTime - saveTime < Global.g_autoTime*1000){
											Thread.sleep(2000);
											curTime = System.currentTimeMillis();
										}
										
										if(Global.g_autoState == true){
											m_handler.post(new Runnable(){
												public void run()
												{
													if(Global.g_bShowTwoPages == false)
														Global.g_curOnePageViewer.goNextPage();
													else
														Global.g_curTwoPageViewer.goNextPage();
												}
											});	
										}
										
										saveTime = curTime;
									}
									catch(InterruptedException e){
										break;
									}
								}
							}
						});
						autoThread.start();
						
						if(Global.g_bShowTwoPages == false)
							Global.g_curOnePageViewer.RemoveTopBarMenu();
						else
							Global.g_curTwoPageViewer.RemoveTopBarMenu();

					}
					
					return false;
				}
			});
		}
		else{
			//button_auto.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.stop));
			button_auto.setText("중지");
			button_auto.setOnTouchListener(new View.OnTouchListener() {
				
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction() == MotionEvent.ACTION_DOWN){
						// button_auto.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.stop_b));
						button_auto.setText("중지");
					}
					else{
						// button_auto.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.start));
						button_auto.setText("시작");
						Global.g_autoState = false;
						if(Global.g_bShowTwoPages == false)
							Global.g_curOnePageViewer.RemoveTopBarMenu();
						else
							Global.g_curTwoPageViewer.RemoveTopBarMenu();
					}
					
					return false;
				}
			});
		}
		
		return m_topbar_menu;
	}
}