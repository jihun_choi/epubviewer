package ybm.EpubViewer.EPubBook;

import android.graphics.Rect;

public class ParagraphPositionInfo {
	public Rect position;
	public HtmlContent content;
	public int paragraphIdx;
}
