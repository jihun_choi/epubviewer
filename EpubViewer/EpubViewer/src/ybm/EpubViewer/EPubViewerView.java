package ybm.EpubViewer;

import java.util.ArrayList;

import ybm.EpubViewer.EPubBook.EpubBook;
import ybm.EpubViewer.EPubBook.HtmlContent;
import ybm.EpubViewer.EPubBook.MemoInfo;
import ybm.EpubViewer.EPubBook.NSRange;
import ybm.EpubViewer.EPubBook.Paragraph;
import ybm.EpubViewer.EPubBook.ParagraphPositionInfo;
import ybm.EpubViewer.EPubBook.SelectedParagraphInfo;
import ybm.EpubViewer.Multimedia.MultimediaItem;

import android.R.bool;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.content.res.Resources;
import android.graphics.Rect;
/*
 * 고치다가 시간이 없어서 죄다 주석처리하고 커밋함^^; 
 * 
 * 아이폰 소스를 카피해서 옮기고 있는데, 안드로이드 api를 몰라서 삽질중 ㅎ
 * 
 * 이미지들은 red/drawable안에 넣어놨음.. 물론 아이패드 이미지라 사이즈 조정이 필요
 */
public class EPubViewerView extends View {

	//ePubViewerView.m, h에 있는 변수들 포팅 
	EPubViewer parent;
	Point firstTouch;
	Point lastTouch;
	
	ArrayList<MultimediaItem> MultimediaItems;//현재 출력 페이지에 들어갈 오디오/비디오 아이템을 들고 있는다. 
	ArrayList<ParagraphPositionInfo> ParagraphPositionInfos;
	
	/*
	녹음/재생기능을 담당할 클래스, 
	AudioManager audioManager;
    */
    
	/* 팝오버 메뉴들 - 안드로이드에 맞게 바꿔줘야함 
	SearchControllerForPopover* searchControllerForPopover;
	FontControllerForPopover* fontcontrollerForPopover;
	PenToolController *_penTool;
	ColorPickerController *_colorPicker;
	TimerPageFlipControllForPopover *timerPageFlipControllForPopover;
	DictionaryControllerForPopover *dictionaryControllerForPopover;
	MemoControllerForPopover *memoControllerForPopover;
	AudioRecordingControllerForPopover* audioRecordingControllerForPopover;
	
	UIPopoverController *popoverController; //<-위의 팝오버컨트롤러의 프레임 역할을 할 컨트롤
     */
    
	/*
	//화면 위쪽에 있는 메뉴 버튼들
	UIButton* bgChangeButton;//<- 이거 안쓰임  
	UIButton* agendaButton;//차례보기 
	UIToolbar* toolBar;
	*/
    
	Boolean bHideBar;
	/* 페이지 넘기는 기능들에 대한 변수들 
	PagingDragger pageDragger;
	NSTimer* pageFlippingTimer;
	float pageFlippingTime;
	Boolean  bPageFlipping;
	 */
	
	//드래그로 선택된 문장의 처음, 끝 
	SelectedParagraphInfo selStart = null;
	SelectedParagraphInfo selFinish = null;
	 
	Boolean bFirstMove;//touch하고 난 후 처음 움직였을때
	Boolean bSelectionDragMode;
	long  tTouchStart, tTouchFinish;
	
	Bitmap onePageBackgroundImage;
	Bitmap twoPageBackgroundImage;
	MemoInfo memoToDelete;
	//AudioPlayerView* audio;


	int deleteNo;//메모, 형광펜, 단어장, 녹음 중에 뭘 지울지 타입 번호를 가지고 있음.
	bool bDontReleseSelection;//선택된 문장의 회색 배경을 지우지 말것 
	
	
	public EPubViewerView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		Init();
	}

	protected void Init()
	{
		/*popoverController = nil;
		fontcontrollerForPopover = nil;
		searchControllerForPopover = nil;
		
		[self createCmdToolBar];
		
		pageDragger = [[PagingDragger alloc] init:CGPointMake(0, g_ScreenFrame.size.height - 50)];
		pageDragger.delegate = self;
		[self SetSliderValue];
		[self addSubview:pageDragger];
		*/
		ParagraphPositionInfos = new ArrayList<ParagraphPositionInfo>();
		if (selStart == null)selStart = new SelectedParagraphInfo();
		if (selFinish == null)selFinish = new SelectedParagraphInfo();
	
		bHideBar = false;
		/*
		pageFlippingTime = 0.5;
		pageFlippingTimer = nil;
		bPageFlipping = false;
		 */
		MultimediaItems = new ArrayList<MultimediaItem>();
		onePageBackgroundImage = CreateOnePageBackground();
		twoPageBackgroundImage = CreateTwoPageBackground();
	
		/*if (g_AudioView)
		{
			g_AudioView.parent = self;
			[self addSubview:g_AudioView];
		}
		if (g_DelAudioView)
		{
            UIImage* img1 = [UIImage imageNamed:@"btn_x.png"];
            UIImage* img2 = [UIImage imageNamed:@"btn_x_b.png"];
            g_DelAudioView = [[[UIButton alloc] initWithFrame:CGRectMake(350, 50, 20, 20) ] autorelease];
            [g_DelAudioView setImage:img1 forState:UIControlStateNormal];
            [g_DelAudioView setImage:img2 forState:UIControlStateSelected];
            [g_DelAudioView addTarget:self action:@selector(DeleteAudio) forControlEvents:UIControlEventTouchUpInside];
            
			//[g_DelAudioView removeTarget:self action:@selector(DeleteAudio) forControlEvents:UIControlEventTouchUpInside];
			//[g_DelAudioView addTarget:self action:@selector(DeleteAudio) forControlEvents:UIControlEventTouchUpInside];
			[self addSubview:g_DelAudioView];				
		}
        
        audioManager = [[AudioManager alloc] init];
        audioManager.parent = self;
*/
	}
	

	Bitmap CreateTwoPageBackground()
	{
		int width = 1024;
		int height = 600;

		// 페이지 바탕을 구성할 캔버스 타겟
		Bitmap target = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(target);
		
		Resources res = getContext().getResources();
		
		// 책 왼편
		Bitmap left = BitmapFactory.decodeResource(res, R.drawable.page_l_768);
		c.drawBitmap(left, 0, 0, new Paint());
		
		// 책 가운데 접히는 부분
		Bitmap mid = BitmapFactory.decodeResource(res, R.drawable.page_mid_768);
		c.drawBitmap(mid, (width - mid.getWidth()) / 2, 0, new Paint());
		
		// 책 오른편
		Bitmap right = BitmapFactory.decodeResource(res, R.drawable.page_r_768);
		c.drawBitmap(right, width - right.getWidth(), 0, new Paint());
		
		// 나머지 영역을 채우는 하얀색 그림
		Bitmap space = BitmapFactory.decodeResource(res, R.drawable.page_bg_768);
		c.drawBitmap(space, null, 
				new Rect(left.getWidth(), 0, (width - mid.getWidth()) / 2, space.getHeight()), new Paint());
		c.drawBitmap(space, null, 
				new Rect((width + mid.getWidth()) / 2, 0, width - right.getWidth(), space.getHeight()), new Paint());
		
		return target;
	}

	Bitmap CreateOnePageBackground()
	{
		int width = 600;
		int height = 1004;
		
		// 페이지 바탕을 구성할 캔버스 타겟
		Bitmap target = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(target);
		
		Resources res = getContext().getResources();
		
		// 책 왼쪽편 이미지
		Bitmap left = BitmapFactory.decodeResource(res, R.drawable.page_left_999);
		c.drawBitmap(left, 0, 0, new Paint());
		
		// 책 오른편 이미지 (-11은 아름다운 아티스트의 마음입니다^^ 왜 17픽셀 아래에 그렸지?)
		Bitmap right = BitmapFactory.decodeResource(res, R.drawable.page1_999);
		c.drawBitmap(right, width - right.getWidth(), -17, new Paint());
		
		// 책 하얀색 바탕 영역 이미지
		Bitmap mid = BitmapFactory.decodeResource(res, R.drawable.page_mid_999);
		
		// 바탕을 page_mid_999 양측 사이드를 뺀 위치에 채운다.
		c.drawBitmap(mid, null, 
				new Rect(left.getWidth(), 0, width - right.getWidth(), mid.getHeight()), new Paint());
		
		return target;
	}

	@Override 
	protected void onDraw(Canvas canvas)
	{
		//새 화면이니 글자가 화면 어느 위치에 있는지 정보를 지우고 다시 수집한다
		ParagraphPositionInfos.clear();
		
		//자바에서 글로벌 변수는 어찌 써야하냐? -_-;
		EpubBook curEpubBook = Global.g_CurEpubBook;
		Rect screenFrame = Global.g_ScreenFrame;
		Resources res = getContext().getResources();
		
		float pageHeight = screenFrame.height();
		
		Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint.setTextSize(16);
		mPaint.setTypeface(null);
		mPaint.setColor(Color.WHITE);
		
		if (Global.g_bShowTwoPages) {
			//canvas.drawBitmap(twoPageBackgroundImage, 0.0f, 0.0f, mPaint);
			
			// ...
		} else {
			canvas.drawBitmap(onePageBackgroundImage, 0.0f, 0.0f, mPaint);
			//4월 15일 -kros
			if (curEpubBook != null)
			{
				//남은 책 페이지수에 따라 오른쪽의 이미지가 달라짐 
				int []filename = {R.drawable.page1_999, R.drawable.page2_999, R.drawable.page3_999};
				int idx = 2;
				if (curEpubBook.curPage > (int)(curEpubBook.GetTotalPage() * 2.0 / 3))
					idx = 0;
				else if (curEpubBook.curPage > (int)(curEpubBook.GetTotalPage() / 3.0))
					idx = 1;
				
				Bitmap right = BitmapFactory.decodeResource(res, filename[idx]);
				canvas.drawBitmap(right, screenFrame.width() - right.getWidth(), 0, mPaint);
				DrawOnePage(new Rect(0, 0, screenFrame.width(), (int)pageHeight), curEpubBook.GetCurPage());
				
			}
				
			// ...
		}
		
	/*	
		if (g_bShowTwoPages)//가로보기
		{
			[twoPageBackgroundImage drawInRect:g_ScreenFrame];
			if(g_CurEpubBook)
			{
				[self DrawOnePage:CGRectMake(0, 0, g_ScreenFrame.size.width/2, pageHeight) page:[g_CurEpubBook GetCurPage]];
				[self DrawOnePage:CGRectMake(g_ScreenFrame.size.width/2, 0, g_ScreenFrame.size.width/2, pageHeight) page:[g_CurEpubBook GetCurPage] + 1];
			}
		} 
		else //세로보기 
		{
	
			}
		}	
		////////////////////////////////////////북마크 표시
		bool bDrawBookMark = false;
		for(int i = 0; i < [[g_CurEpubBook BookMarks] count]; ++i)
		{
			BookMarkPos* bmp = [[g_CurEpubBook BookMarks] objectAtIndex:i];
			if (bmp.pageNo == [g_CurEpubBook curPage] || (g_bShowTwoPages && bmp.pageNo == [g_CurEpubBook curPage] + 1))
			{
				bDrawBookMark = true;
				break;
			}
		}
		
		if (bDrawBookMark)
		{
			UIImage* image = [UIImage imageNamed:@"bookmark1.png"];
			CGSize size = [image size];
			CGRect rcMid;
			if (!bHideBar)
				rcMid = CGRectMake( g_ScreenFrame.size.width - size.width - 10, 40, size.width, size.height);
			else {
				rcMid = CGRectMake( g_ScreenFrame.size.width - size.width - 10, 3, size.width, size.height);
			}
			[image drawInRect:rcMid];
			[image release];	
		}
		
		[self DrawPagingBar];
*/
		
		//canvas.drawColor(Color.BLACK);
		//Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		//mPaint.setTextSize(16);
		//mPaint.setTypeface(null);
		//mPaint.setColor(Color.RED);
		
		//canvas.drawText("asdfaf",100, 100, mPaint);
	}
	

	void DrawOnePage(Rect rect, float pageNo)
	{	
	/*	CGContextRef context = UIGraphicsGetCurrentContext();
		
		HtmlContent* htmlcontent = [g_CurEpubBook GetContentOfPage:pageNo]; 
		int currentPage = [g_CurEpubBook GetContentPageOfPage:pageNo];
		
		Page* page = [[htmlcontent Pages] objectAtIndex:currentPage];
	 	CGFloat curY = htmlcontent.PageRect.origin.y + g_TopMargin + rect.origin.y;
		
		CGFloat additionalMargin = (rect.size.height - (g_TopMargin + g_BottomMargin) - [page Height])/2;
		
		if ([page bYmargin])
			curY += additionalMargin;
		else 
		{
			curY += (rect.size.height - g_PageFrame.size.height)/2;
		}
		
		int page_X = rect.origin.x + (rect.size.width - g_PageFrame.size.width)/2;
		
		//배경색칠하기 
		if ([htmlcontent szBgColor])
		{
			UIColor* defaultColor = [UIColor colorWithRed:(250/255.0) green:(246/255.0) blue:(237/255.0) alpha:1];
			UIColor* bgColor = GetColorFromString([htmlcontent szBgColor], defaultColor);
			CGContextSetFillColorWithColor(context, bgColor.CGColor);
			CGContextAddRect(context, CGRectMake(page_X, curY, g_PageFrame.size.width, g_PageFrame.size.height));
			[bgColor release];
		}
		if (g_BgColor)
		{
			CGContextSetFillColorWithColor(context, g_BgColor.CGColor);
			CGContextAddRect(context, CGRectMake(page_X, curY, g_PageFrame.size.width, g_PageFrame.size.height));
		}
		CGContextDrawPath(context, kCGPathFill);
		
		
		for(NSInteger i = [page StartParaIdx]; i <= [page EndParaIdx]; ++i)
		{
			Paragraph* para = [[htmlcontent paragraphs] objectAtIndex:i];
			
			para.playerDelegate = self;
			int startline = 0;
			int endline = -1;
			
			if (i == [page StartParaIdx])
				startline = [page StartLine];
			if (i == [page EndParaIdx])
				endline = [page EndLine];
			
			if (endline == -2)//next page
				break;
			
			CGRect paraRect = CGRectMake(page_X, curY, htmlcontent.PageRect.size.width, rect.size.height - curY);
			float paraHeight = [para drawRect:paraRect to:startline from:endline justify:TRUE ];
			curY += paraHeight;
			[self AddParagraphPositionInfo:CGRectMake(paraRect.origin.x, paraRect.origin.y, paraRect.size.width, paraHeight) content:htmlcontent paraIdx:para.m_nIndex]; 
			
		}*/
		
	}
}
