package ybm.EpubViewer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ybm.EpubViewer.EPubBook.BookMarkPos;
import ybm.EpubViewer.EPubBook.EpubBook;
import ybm.EpubViewer.EPubBook.HtmlContent;
import ybm.EpubViewer.EPubBook.LineData;
import ybm.EpubViewer.EPubBook.MemoInfo;
import ybm.EpubViewer.EPubBook.NSRange;
import ybm.EpubViewer.EPubBook.Page;
import ybm.EpubViewer.EPubBook.Paragraph;
import ybm.EpubViewer.EPubBook.ParagraphPositionInfo;
import ybm.EpubViewer.EPubBook.SelectedParagraphInfo;
import ybm.EpubViewer.EPubBook.TextInfo;
import ybm.EpubViewer.OnePageViewer.MyTextView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TableRow.LayoutParams;

public class TwoPageViewer extends Activity {

	int m_tabbar; // 0 - invisibile,  1 - visible	
	boolean bDrawBookMark = false;
	
	long m_starttime;
	
	public MyTextView2 m_textView;
	View m_mainmemu;
	View m_wordmenu;
	
	boolean m_menuopened;
	//boolean m_drawAudioButton = false;
	
	float m_start_x;
	float m_start_y;
	float m_end_x;
	
	SelectedParagraphInfo m_selStart;
	SelectedParagraphInfo m_selFinish;
	
	Page m_page;
	HtmlContent m_htmlContent;
	
	int m_topbar_menu_type;
	View m_topbar_menu;
	
	CountDownTimer m_timer = null;
	boolean m_notDraw = false;
	boolean m_doTurn = false;
	float m_rotDegree = 0.0f; // -90 -> 0 -> 90
	Bitmap[] m_prevPage = new Bitmap[2];
	Bitmap[] m_thisPage = new Bitmap[2];
	
	Handler m_handler;
	//MediaRecorder m_recoder = null; // 녹음기
	//String m_recodingPath = null; // 녹음 중인 경로
	MediaPlayer m_player = new MediaPlayer(); // 메모 재생만 이것으로 함
	Bitmap m_background = null;
	
	public class MyTextView2 extends View {
		ArrayList<ParagraphPositionInfo> ParagraphPositionInfos;		
		
		Bitmap CreateTwoPageBackground()
		{
			int width = 1024;
			int height = 600;

			// 페이지 바탕을 구성할 캔버스 타겟
			Bitmap target = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
			Canvas c = new Canvas(target);
			
			Resources res = getContext().getResources();
			
			// 책 왼편
			Bitmap left = BitmapFactory.decodeResource(res, R.drawable.page_l_768);
			c.drawBitmap(left, 0, 0, new Paint());
			
			// 책 가운데 접히는 부분
			Bitmap mid = BitmapFactory.decodeResource(res, R.drawable.page_mid_768);
			c.drawBitmap(mid, (width - mid.getWidth()) / 2, 0, new Paint());
			
			// 책 오른편
			Bitmap right = BitmapFactory.decodeResource(res, R.drawable.page_r_768);
			c.drawBitmap(right, width - right.getWidth(), 0, new Paint());
			
			// 나머지 영역을 채우는 하얀색 그림
			Bitmap space = BitmapFactory.decodeResource(res, R.drawable.page_bg_768);
			c.drawBitmap(space, null, 
					new Rect(left.getWidth(), 0, (width - mid.getWidth()) / 2, space.getHeight()), new Paint());
			c.drawBitmap(space, null, 
					new Rect((width + mid.getWidth()) / 2, 0, width - right.getWidth(), space.getHeight()), new Paint());
			
			return target;
		}
		
		public MyTextView2(Context context) {
			super(context);
			
			m_background = CreateTwoPageBackground();
			
			ParagraphPositionInfos = new ArrayList<ParagraphPositionInfo>();
		}
		
		@Override
		public void onDraw(Canvas canvas) {
			if (Global.g_curActivity != TwoPageViewer.this)
    	 		Global.g_curActivity = TwoPageViewer.this;
			
			if (!Global.g_bShowTwoPages) {
				int height = getWindowManager().getDefaultDisplay().getHeight();
	    	 	int width = getWindowManager().getDefaultDisplay().getWidth();
	    	 	
	    	 	Global.g_ScreenFrame = new Rect(0, 0, width, height);
				Global.g_PageFrame = new Rect(0, 0, width / 2 - 100, height - 120); // <- 적당히 맞추었음
				
				Global.g_bShowTwoPages = true;
				
				Global.g_CurEpubBook.ProcessPaging();
				
				// 책 보던 곳 계산
				if (Global.g_OldCurPage != -1) {
					float ratio = (float)Global.g_OldCurPage / (float)Global.g_OldTotalPage;
					Global.g_CurEpubBook.curPage = (int)(ratio * (float)Global.g_CurEpubBook.GetTotalPage());
					
					if (Global.g_CurEpubBook.curPage < 0)
						Global.g_CurEpubBook.curPage = 0;
					else if (Global.g_CurEpubBook.curPage > Global.g_CurEpubBook.GetTotalPage()) {
						Global.g_CurEpubBook.curPage = Global.g_CurEpubBook.GetTotalPage() - 1;
					}
					
					// 짝수 페이지를 왼쪽으로 함
					if (Global.g_CurEpubBook.curPage % 2 > 0)
						Global.g_CurEpubBook.curPage--;
					
					Global.g_OldTotalPage = Global.g_CurEpubBook.GetTotalPage();
				}
			}
			
			//새 화면이니 글자가 화면 어느 위치에 있는지 정보를 지우고 다시 수집한다
			ParagraphPositionInfos.clear();
			
			EpubBook curEpubBook = Global.g_CurEpubBook;
			Rect screenFrame = Global.g_ScreenFrame;
			
			if (!m_notDraw && !m_doTurn) {
				Rect halfRect = new Rect(0, 0, screenFrame.width() / 2, screenFrame.height());
				
				DrawOnePage(new Canvas(m_thisPage[0]), halfRect, curEpubBook.GetCurPage());
				DrawOnePage(new Canvas(m_thisPage[1]), halfRect, curEpubBook.GetCurPage() + 1);
				
				drawAudioButton();
			}
			
			Global.g_OldCurPage = curEpubBook.GetCurPage();
			
			if (!m_doTurn) {
				canvas.drawBitmap(m_thisPage[0], 0, 0, null);
				canvas.drawBitmap(m_thisPage[1], screenFrame.width() / 2, 0, null);
			} else {
				canvas.drawBitmap(m_prevPage[0], 0, 0, null); // 왼쪽은 이전 페이지(좌)
				canvas.drawBitmap(m_thisPage[1], screenFrame.width() / 2, 0, null); // 오른쪽은 다음 페이지(우)
				
				// 카메라 조작
				Camera camera = new Camera();
				camera.save();
				
				camera.translate(0, Global.g_ScreenFrame.height() / 2, 500);
				camera.rotateY(m_rotDegree);
				
				Matrix m = new Matrix();
				camera.getMatrix(m);
				
				camera.restore();
				
				m.postScale(1.87f, 1.87f);
				m.postTranslate(Global.g_ScreenFrame.width() / 2, Global.g_ScreenFrame.height() / 2);
				
				Paint paint = new Paint(); 
				paint.setAntiAlias(true); 
				paint.setFilterBitmap(true);
				
				if (m_rotDegree >= -90 && m_rotDegree <= 0) { // 오른쪽에서 페이지가 넘어가는 중
					canvas.drawBitmap(m_prevPage[1], m, paint);
				} else { // 왼쪽에서 페이지가 넘어가는 중
					canvas.drawBitmap(m_thisPage[0], m, paint);
				}
			}
		}
		
		public void DrawOnePage(Canvas canvas, Rect rect, float pageNo) {
	        
			Rect srcRect = new Rect(rect);
			srcRect.offset(pageNo % 2 == 0 ? 0 : Global.g_ScreenFrame.width() / 2, 0); // 짝수 페이지가 왼쪽
			canvas.drawBitmap(m_background, srcRect, rect, null); // 배경 그림
			
			if (pageNo >= Global.g_CurEpubBook.GetTotalPage())
				return; // 우측 페이지가 범위를 벗어났다면 배경만 그리도록 여기서 return
				
			m_htmlContent = Global.g_CurEpubBook.GetContentOfPage((int)pageNo);
			int currentPage = Global.g_CurEpubBook.GetContentPageOfPage(pageNo);
			
			m_page = m_htmlContent.Pages.get(currentPage);
			float curY = m_htmlContent.PageRect.top + Global.g_TopMargin + rect.top;
			
			float additionalMargin = (rect.height() - (Global.g_TopMargin + Global.g_BottomMargin) - m_page.Height) / 2;
			
			if (m_page.bYmargin)
				curY += additionalMargin;
			else
				curY += (rect.height() - Global.g_PageFrame.height()) / 2;
			
			int pageX = rect.left + (rect.width() - Global.g_PageFrame.width()) / 2;
			
			if (m_htmlContent.szBgColor != null) {
				int bgColor = Global.GetColorFromString(m_htmlContent.szBgColor, Color.WHITE);
				Paint paint = new Paint();
				paint.setColor(bgColor);
				paint.setStyle(Paint.Style.FILL);
				canvas.drawRect(new Rect(pageX, (int)curY, Global.g_PageFrame.width(), Global.g_PageFrame.height()), paint);
			}
			
			if (Global.g_BgColor != null) {
				Paint paint = new Paint();
				paint.setColor(Global.g_BgColor);
				paint.setStyle(Paint.Style.FILL);
		
				canvas.drawRect(new Rect(pageX, (int)curY, Global.g_PageFrame.width(), Global.g_PageFrame.height()), paint);
			}
			
//			CGContextDrawPath(context, kCGPathFill);
			for(int i = m_page.StartParaIdx; i <= m_page.EndParaIdx; ++i) {
				Paragraph para = m_htmlContent.paragraphs.get(i);
				
				//Log.i("debug", "paragraphs get " + i);
				
				para.playerDelegate = this.getContext();
//				para.playerDelegate = self;
				
				int startline = 0;
				int endline = -1;
				
				if (i == m_page.StartParaIdx)
					startline = m_page.StartLine;
				if (i == m_page.EndParaIdx)
					endline = m_page.EndLine;
				
				if (endline == -2)//next page
					break;
			
				//if(para.objType == 2){ // Audio 파일이 있다면 
					//m_audioFile = Global.g_CurEpubBook.GetRecFromFile(para.objFilename);
				//	m_drawAudioButton = true;
					
				//	m_audioButton_y = curY;		 
				//	m_audioButton_x = pageX;
				//}
				
				Rect paraRect = new Rect(pageX, (int)curY, pageX + m_htmlContent.PageRect.width(), rect.height() - (int)curY);
				boolean bJustify = true;
				curY += para.drawRect(canvas, paraRect, startline, endline, bJustify);
				
				ParagraphPositionInfo posInfo = new ParagraphPositionInfo();
				posInfo.position = new Rect(paraRect.left, paraRect.top, paraRect.width(), paraRect.height());
				posInfo.content = m_htmlContent;
				posInfo.paragraphIdx = para.m_nIndex;
				ParagraphPositionInfos.add(posInfo);
			}
		}
	}
	
	@Override
	public void onPause()
	{
		pageViewed = Global.g_CurEpubBook.GetCurPage();
		
		super.onPause();
	}
	
	int pageViewed = 0;
	
	@Override
	public void onResume()
	{
		m_handler = new Handler();
		Global.g_autoState = false;
		
		boolean pageMoved = (pageViewed != Global.g_CurEpubBook.GetCurPage());
		
		if (pageMoved) {
			// 보이고 있던 mp3 play 버튼이 있었다면 사라지게 함
			if (Global.g_curMp3Play != null) {
				final RelativeLayout container = 
					(RelativeLayout)this.findViewById(R.id.MM_rel_back);
				
				container.removeView(Global.g_curMp3Play);
				Global.g_curMp3Play = null;
			}
			
			// update the seek-bar
			final SeekBar bar = (SeekBar)findViewById(R.id.MM_seekbar);
	        bar.setProgress((int)((float)Global.g_CurEpubBook.GetCurPage() / (float)Global.g_CurEpubBook.GetTotalPage() * 100.0f));
        }
		
		super.onResume();
	}
	
	@Override
	public void onStop()
	{
		Global.g_autoState = false;
		if(Global.g_curMp3Player != null && Global.g_curMp3Player.isPlaying())
			Global.g_curMp3Player.pause();
		
		if(Global.g_curMp3Bar != null)
			Global.g_curMp3Bar.clearFocus();
		
		RemoveMenu();
		super.onStop();
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		if(Global.g_curMp3Player != null){
			Global.g_curMp3Player.stop();
			Global.g_curMp3Player.release();
			Global.g_curMp3Player = null;
		}
		
		final RelativeLayout back = (RelativeLayout)findViewById(R.id.MM_rel_back);
		if(back != null){
			back.clearFocus();
			back.removeAllViews();
		}
		
		Global.g_curMp3Bar = null;
		
		if(m_thisPage[0] != null && m_thisPage[0].isRecycled() == false)
			m_thisPage[0].recycle();
		
		if(m_thisPage[1] != null && m_thisPage[1].isRecycled() == false)
			m_thisPage[1].recycle();
		
		if(m_prevPage[0] != null && m_prevPage[0].isRecycled() == false)
			m_prevPage[0].recycle();
		
		if(m_prevPage[1] != null && m_prevPage[1].isRecycled() == false)
			m_prevPage[1].recycle();
		
		m_thisPage = null;
		m_prevPage = null;
		
		Global.g_curMp3Play = null;
		Global.g_curMp3PlayPosition = null;
		
		m_textView.destroyDrawingCache();
		m_player = null;
		
		if(m_background != null && m_background.isRecycled() == false)
			m_background.recycle();
	}
	
	public void goNextPage()
	{
		RemoveTopBarMenu();
		
		if(Global.g_CurEpubBook.curPage < Global.g_CurEpubBook.GetTotalPage() - 2) {
			if (m_timer != null) {
				m_timer.cancel();
				m_timer.onFinish();
			}
			
			if(Global.g_CurEpubBook.curPage % 2 > 0)
				Global.g_CurEpubBook.curPage++;
			else
				Global.g_CurEpubBook.curPage += 2;			
			
			if(Global.g_curWordMenu != null)
				ReleaseSelection();
			
			int height = getWindowManager().getDefaultDisplay().getHeight();
		 	int width = getWindowManager().getDefaultDisplay().getWidth();
		 	
			// 보이고 있던 mp3 play 버튼이 있었다면 사라지게 함
			if (Global.g_curMp3Play != null) {
				final RelativeLayout container = 
					(RelativeLayout)this.findViewById(R.id.MM_rel_back);
				
				container.removeView(Global.g_curMp3Play);
				Global.g_curMp3Play = null;
			}
		 	
		 	// 캔버스 스왑
		 	m_prevPage[0].recycle();
		 	m_prevPage[1].recycle();
			m_prevPage[0] = m_thisPage[0];
			m_prevPage[1] = m_thisPage[1];
			m_thisPage[0] = Bitmap.createBitmap(width / 2, height, Bitmap.Config.ARGB_8888);
		    m_thisPage[1] = Bitmap.createBitmap(width / 2, height, Bitmap.Config.ARGB_8888);
		    
		    // 새 페이지 그리기
		    Rect halfRect = new Rect(0, 0, Global.g_ScreenFrame.width() / 2, Global.g_ScreenFrame.height());
		    m_textView.DrawOnePage(new Canvas(m_thisPage[0]), halfRect, Global.g_CurEpubBook.GetCurPage());
		    m_textView.DrawOnePage(new Canvas(m_thisPage[1]), halfRect, Global.g_CurEpubBook.GetCurPage() + 1);
		    
		    // 페이지 넘기기 효과를 위해 좌우 반전
		    m_thisPage[0] = Bitmap.createScaledBitmap(m_thisPage[0], 
		    		-m_thisPage[0].getWidth(), m_thisPage[0].getHeight(), false);
		   	
		    // 애니메이션
		    m_timer = new CountDownTimer(700, 33) {
				@Override
				public void onTick(long millisUntilFinished) {
					// [2000, 0] -> [0, 2000] -> [0, -90, -180?]
					m_rotDegree = -180.0f * ((700 - millisUntilFinished) / 700.0f);
					m_textView.invalidate();
				}
				
				@Override
				public void onFinish() {
					// 페이지 넘기기 효과를 위해 반전한 것을 원래대로 함
					m_thisPage[0] = Bitmap.createScaledBitmap(m_thisPage[0], 
							-m_thisPage[0].getWidth(), m_thisPage[0].getHeight(), false);
					
					m_doTurn = false;
					m_textView.invalidate();
					m_timer = null;
					
					drawAudioButton();
				}
			};
			
			m_doTurn = true;
			m_timer.start();
		}
	}
	
	public void goPrevPage()
	{
		RemoveTopBarMenu();
		
		if(Global.g_CurEpubBook.curPage > 1){
			if (m_timer != null) {
				m_timer.cancel();
				m_timer.onFinish();
			}
			
			if(Global.g_curWordMenu != null)
				ReleaseSelection();
			
			if(Global.g_CurEpubBook.curPage % 2 > 0)
				Global.g_CurEpubBook.curPage--;
			else
				Global.g_CurEpubBook.curPage -= 2;
			
			// 페이지 넘기기 효과를 위해 좌우 반전
		    m_thisPage[0] = Bitmap.createScaledBitmap(m_thisPage[0], 
		    		-m_thisPage[0].getWidth(), m_thisPage[0].getHeight(), false);
		    
			// 보이고 있던 mp3 play 버튼이 있었다면 사라지게 함
			if (Global.g_curMp3Play != null) {
				final RelativeLayout container = 
					(RelativeLayout)this.findViewById(R.id.MM_rel_back);
				
				container.removeView(Global.g_curMp3Play);
				Global.g_curMp3Play = null;
			}
		    
		    // 애니메이션
		    m_timer = new CountDownTimer(700, 33) {
				@Override
				public void onTick(long millisUntilFinished) {
					// [2000, 0] -> [-180, 0]
					m_rotDegree = -180.0f * (millisUntilFinished / 700.0f);
					m_textView.invalidate();
				}
				
				@Override
				public void onFinish() {
					// 페이지 넘기기 효과를 위해 반전한 것을 원래대로 함
					m_thisPage[0] = Bitmap.createScaledBitmap(m_thisPage[0], 
							-m_thisPage[0].getWidth(), m_thisPage[0].getHeight(), false);
					
					int height = getWindowManager().getDefaultDisplay().getHeight();
				 	int width = getWindowManager().getDefaultDisplay().getWidth();
				 					 	
				 	
					// 새 페이지 그리기
					if (Global.g_CurEpubBook.GetCurPage() > 0) {
						// 캔버스 스왑
					 	m_thisPage[0].recycle();
					 	m_thisPage[1].recycle();
					 	m_thisPage[0] = m_prevPage[0];
					 	m_thisPage[1] = m_prevPage[1];
						m_prevPage[0] = Bitmap.createBitmap(width / 2, height, Bitmap.Config.ARGB_8888);
						m_prevPage[1] = Bitmap.createBitmap(width / 2, height, Bitmap.Config.ARGB_8888);
						
					    Rect halfRect = new Rect(0, 0, Global.g_ScreenFrame.width() / 2, Global.g_ScreenFrame.height());
					    m_textView.DrawOnePage(new Canvas(m_prevPage[0]), halfRect, Global.g_CurEpubBook.GetCurPage() - 2);
					    m_textView.DrawOnePage(new Canvas(m_prevPage[1]), halfRect, Global.g_CurEpubBook.GetCurPage() - 1);
					}
					
					m_notDraw = false;
					m_doTurn = false;
					m_textView.invalidate();
					m_timer = null;
					
					drawAudioButton();
				}
				
			};
			
			m_notDraw = true;
			m_doTurn = true;
			m_timer.start();
		}
	}
	
	void drawAudioButton()
	{
		Log.i("debug", "drawAudioButton");
		
		if (Global.g_curMp3Play != null) {
			final RelativeLayout container = 
				(RelativeLayout)this.findViewById(R.id.MM_rel_back);
			
			container.removeView(Global.g_curMp3Play);
			Global.g_curMp3Play = null;
		}		
		
		Canvas canvas = new Canvas();
		Rect halfRect = new Rect(0, 0, Global.g_ScreenFrame.width() / 2, Global.g_ScreenFrame.height());
	    m_textView.DrawOnePage(canvas, halfRect, Global.g_CurEpubBook.GetCurPage());
	    m_textView.DrawOnePage(canvas, halfRect, Global.g_CurEpubBook.GetCurPage() + 1);
		
		if (Global.g_curMp3Play == null || Global.g_curMp3PlayPosition == null)
			return;
		
		Log.i("debug", "mp3play");
		
		try {
			final RelativeLayout container = 
				(RelativeLayout)this.findViewById(R.id.MM_rel_back);
			
			container.addView(Global.g_curMp3Play);
			
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)Global.g_curMp3Play.getLayoutParams();
			params.alignWithParent = false;
			params.addRule(RelativeLayout.ALIGN_TOP | RelativeLayout.ALIGN_LEFT);
			//params.topMargin = Global.g_curMp3PlayPosition.top - 45; // 그림과 마찬가지로 오묘하게 위로 좀 올림
			params.topMargin = Global.g_curMp3PlayPosition.top;
			params.leftMargin = Global.g_curMp3PlayPosition.left;
			
			final Button button = (Button)Global.g_curActivity.findViewById(R.id.mp3play);
			button.setText("");
			button.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_mp3_a));
			button.setOnTouchListener(new View.OnTouchListener() {
				
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction() == MotionEvent.ACTION_DOWN){
						button.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_mp3_b));
					}
					else{
						button.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_mp3_a));
						
						Log.i("AUDIO", "PLAY MP3PLAY " + Global.g_curMp3PlayURL);
						
						Pattern p = Pattern.compile("/[0-9]+/.*\\.mp3");
						Matcher m = p.matcher(Global.g_curMp3PlayURL);
						
						String streamingInfoURL = null;
						
						if (m.find()) {
							streamingInfoURL = m.group();
						}
						
						if (streamingInfoURL != null) {
							String ret =
								Library.HttpGetByString("http://www.ybmmobile.com/appstore/play_m4a.asp?filename=" + streamingInfoURL);
							
							Pattern pp = Pattern.compile("<MediaURL>(.+)</MediaURL>");
							Matcher mm = pp.matcher(ret);
							
							String streamingURL = null;
							
							if (mm.find()) {
								streamingURL = mm.group(1);
							}
							
							if (streamingURL != null) {
								// 재생
								// StreamingURL -> 여기에서 파일명 추출함.
								
								String url = "http://upsisa.ybmsisa.com/new_business/epub/classichouse/";
								if(Integer.parseInt(Global.g_CurEpubBook.bookId) < 10)
									url += "0" + Global.g_CurEpubBook.bookId + "/";
								else
									url += Global.g_CurEpubBook.bookId + "/";
								int end = streamingURL.indexOf(".mp3");
								end += 3;
								
								int start;
								for(start = end; start >= 0; start--){
									if(streamingURL.charAt(start) == '/') 
										break;
								}
								
								String filename = streamingURL.substring(start + 1, end + 1);
								url += filename;	
								
								final RelativeLayout back = (RelativeLayout)findViewById(R.id.MM_rel_back);
								if(Global.g_curMp3Bar != null){
									back.removeView(Global.g_curMp3Bar);
								}
								
								String completeFileName = Global.g_CurEpubBook.bookId + "ch" + filename;

								Global.g_curMp3Bar = StreamingView.OpenStreamingMenu(TwoPageViewer.this, m_handler, url, 
										completeFileName);
							}

						}
					}
					
					return false;
				}
			});
			
		} catch (Exception e) {
			// 여기서 이상하게 가끔 죽는 문제가 있음
			e.printStackTrace();
		}
	}
	
	void viewBookmark()
	{
        final RelativeLayout bookmark = (RelativeLayout)findViewById(R.id.MM_rel_mark);
        bDrawBookMark = false;
        
		for(int j=0;j<=Global.g_CurEpubBook.BookMarks.size()-1;j++){
			BookMarkPos bmp = Global.g_CurEpubBook.BookMarks.get(j);
			if(bmp.pageNo == Global.g_CurEpubBook.curPage || 
					bmp.pageNo == Global.g_CurEpubBook.curPage+1){
				bDrawBookMark = true;
				Global.g_CurEpubBook.BookMarks.remove(j);
				Global.g_CurEpubBook.SaveMemos();
			}
		}
        
        if(bDrawBookMark == false)
        	bookmark.setVisibility(RelativeLayout.INVISIBLE);
        else
        	bookmark.setVisibility(RelativeLayout.VISIBLE);

	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    // 화면 사이즈
	    int height = getWindowManager().getDefaultDisplay().getHeight();
	 	int width = getWindowManager().getDefaultDisplay().getWidth();
	 	
	 	// 캔버스로 사용 할 비트맵
	 	m_prevPage[0] = Bitmap.createBitmap(width / 2, height, Bitmap.Config.ARGB_8888); // 왼쪽 페이지
	 	m_prevPage[1] = Bitmap.createBitmap(width / 2, height, Bitmap.Config.ARGB_8888); // 오른쪽 페이지
	    m_thisPage[0] = Bitmap.createBitmap(width / 2, height, Bitmap.Config.ARGB_8888);
	    m_thisPage[1] = Bitmap.createBitmap(width / 2, height, Bitmap.Config.ARGB_8888);
	    
        setContentView(R.layout.twopage);
        
        m_textView = new MyTextView2(this);
        addContentView(m_textView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        
        final LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        m_mainmemu = inflater.inflate(R.layout.mainmenu, null);
        addContentView(m_mainmemu, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));        
        
        m_textView.invalidate();
        
	    Log.i("debug", "TwoPageViewer onCreate");
	    
	    Global.g_curWordMenu = null;
	    
        m_selStart = null;
        m_selFinish = null;
        m_menuopened = false;
        
	    Global.g_curTwoPageViewer = this;
	    drawAudioButton();
	    
	    if(Global.g_bChangeView == true){

	    }
	    
	    //-------------------------------- intent -------------------------------
        Intent i = new Intent(getIntent());
        m_tabbar = i.getIntExtra("tabbar", 1);
        //m_bookmark = i.getIntExtra("bookmark", 0);
        //m_auto = i.getIntExtra("auto", 0);
        
        
		//------------------------------------------------------------------------
        
        final Animation tabbar_upward = AnimationUtils.loadAnimation(this, R.anim.tabbar_up);
        final Animation tabbar_downward = AnimationUtils.loadAnimation(this, R.anim.tabbar_down);
        
        final SeekBar bar = (SeekBar)findViewById(R.id.MM_seekbar);
        final RelativeLayout tabbar = (RelativeLayout)findViewById(R.id.MM_rel_tabbar);
        if(m_tabbar == 0){
        	tabbar.setVisibility(4); // Invisible
        	bar.setVisibility(SeekBar.INVISIBLE);
        }
        else{
        	tabbar.setVisibility(RelativeLayout.VISIBLE);
        	bar.setVisibility(SeekBar.VISIBLE);
        }
        
        final RelativeLayout bookmark = (RelativeLayout)findViewById(R.id.MM_rel_mark);
        viewBookmark();
        
        bar.setThumb(getResources().getDrawable(R.drawable.btn_bottom));
        bar.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar));        
        bar.setMax(100);
        bar.setProgress((int)((float)Global.g_CurEpubBook.GetCurPage() / 
        		(float)Global.g_CurEpubBook.GetTotalPage() * 100.0f));
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				int page;
				
				if(seekBar.getProgress() > seekBar.getMax() - 5)
					page = Global.g_CurEpubBook.GetTotalPage() - 1;
				else if(seekBar.getProgress() < 5)
					page = 0;
				else
					page = seekBar.getProgress() * Global.g_CurEpubBook.GetTotalPage() / 100;				
				
				if(page > Global.g_CurEpubBook.curPage){
					Global.g_CurEpubBook.curPage = page-1;					
					goNextPage();
				}
				else if(page < Global.g_CurEpubBook.curPage){
					Global.g_CurEpubBook.curPage = page+1;
					goPrevPage();
				}
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				
			}
		});
        
        RelativeLayout righttouch = (RelativeLayout)findViewById(R.id.TP_rel_righttouch);
        righttouch.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				goNextPage();
			}
		});
        
        RelativeLayout lefttouch = (RelativeLayout)findViewById(R.id.TP_rel_lefttouch);
        lefttouch.setOnClickListener(new View.OnClickListener() {	
			public void onClick(View v) {
				goPrevPage();
			}
		});
        
        final Button button_mark = (Button)findViewById(R.id.MM_button_mark);
        button_mark.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_mark.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_kmark_b));
				}
				else{
					if(bDrawBookMark == false){
						bookmark.setVisibility(0);
						
						TextInfo ret = new TextInfo();
						Global.g_CurEpubBook.GetTextInfoOfPage(Global.g_CurEpubBook.curPage, ret);
						if(ret.contentIdx != -1)
							Global.g_CurEpubBook.AddBookMark(ret);
					}
					else{
						bookmark.setVisibility(4);
						
						for(int i=0;i<=Global.g_CurEpubBook.BookMarks.size()-1;i++){
							BookMarkPos bmp = Global.g_CurEpubBook.BookMarks.get(i);
							if(bmp.pageNo == Global.g_CurEpubBook.curPage || 
									bmp.pageNo == Global.g_CurEpubBook.curPage+1){
								Global.g_CurEpubBook.BookMarks.remove(i);
								Global.g_CurEpubBook.SaveMemos();
							}
						}
					}
					
					bDrawBookMark = !(bDrawBookMark);
					
					button_mark.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_kmark_a));
				}
				
				return true;
			}
		});
        
        
        final RelativeLayout rel3 = (RelativeLayout)findViewById(R.id.TP_rel_back);
        rel3.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					if(Global.g_curWordMenu != null){
						m_menuopened = true;
					}
					else
						m_menuopened = false;
					
					m_start_x = event.getRawX();
					m_start_y = event.getRawY()-50;
					m_starttime = System.currentTimeMillis();
					touchesBegan((int)event.getRawX(), (int)event.getRawY()-50); // -50 ?? 
				}
				else if(event.getAction() == MotionEvent.ACTION_UP){
					
					m_end_x = event.getRawX();
					// 드래그시 처리.
					if(m_start_x - event.getRawX() > 50.0 && 
							System.currentTimeMillis() - m_starttime < 500){ // 왼쪽으로 페이지 넘어감. 다음 페이지.
						if(Global.g_CurEpubBook.curPage < Global.g_CurEpubBook.GetTotalPage() -1){
							goNextPage();
						}
						
						return true;
					}
					
					if(event.getRawX() - m_start_x > 50.0 &&
							System.currentTimeMillis() - m_starttime < 500){ // 이전 페이지.
						if(Global.g_CurEpubBook.curPage < Global.g_CurEpubBook.GetTotalPage() -1){
							goPrevPage();
						}
						
						return true;
					}
					
					if(System.currentTimeMillis() - m_starttime < 500){
						if(m_tabbar == 0){
							tabbar.setVisibility(0); // Visible
							tabbar.startAnimation(tabbar_downward);					
						}
						else{
							tabbar.startAnimation(tabbar_upward);
							RemoveTopBarMenu(); // Top Bar와 연관된 메뉴 지움.
						}
						
						return true;
					}
					
					// 오래 누르고 있었을 경우 처리
					// 드래그를 했을 경우 선택 영역만 처리. 그렇지 않은 경우 자동으로 단어 선택.
					if(touchesMoved((int)event.getRawX(), (int)event.getRawY()-50)){
						
						// TODO : Memo에 해당하는 글자인지 피악하고 맞으면 표시해줌.
						
						// TODO : 한 두 글자를 오래 눌렀을 경우 양옆으로 띄어 쓰기까지 모두 찾아서 선택 영역에 포함시켜줌.
						
						OpenWordMenu((int)event.getRawX(), (int)event.getRawY()-50);
						return true;
					}
					
					if(m_menuopened == true){ // 단어 메모 메뉴가 열려있는 경우 닫힘으로 바꿈. 
						m_menuopened = false;
						
						return true;
					}
				}
				
				return true;
			}
		});
        
        //rel3.setOnClickListener(new View.OnClickListener() {
		//	public void onClick(View v) {
		//		// TODO Auto-generated method stub
        //
		//	}
		//});
        
        
        tabbar_upward.setAnimationListener(new Animation.AnimationListener() {
			
			public void onAnimationStart(Animation animation) { }

			public void onAnimationRepeat(Animation animation) { }

			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				tabbar.setVisibility(4); // Invisible
				bar.setVisibility(SeekBar.INVISIBLE);
				m_tabbar = 0;
			}
		});

        tabbar_downward.setAnimationListener(new Animation.AnimationListener() {

			public void onAnimationStart(Animation animation) {	}

			public void onAnimationRepeat(Animation animation) { }

			public void onAnimationEnd(Animation animation) {
				m_tabbar = 1;
				bar.setVisibility(SeekBar.VISIBLE);
				tabbar.setVisibility(RelativeLayout.VISIBLE);
			}
		});
        
        
        /*final Button button_agbookmark = (Button)findViewById(R.id.MM_button_lib);
        button_agbookmark.setOnTouchListener(new View.OnTouchListener() {			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_agbookmark.setBackgroundDrawable(
							getResources().getDrawable(R.drawable.btn_klib_b));
				}
				else{
					button_agbookmark.setBackgroundDrawable(
							getResources().getDrawable(R.drawable.btn_klib_a));
					
					//Intent i = new Intent(TwoPageViewer.this, AgendaBookmark.class);
					//startActivity(i);
					finish();
				}
				return false;
			}
		});*/
        
        final Button button_con = (Button)findViewById(R.id.MM_button_con);
        button_con.setOnTouchListener(new View.OnTouchListener() {			
			public boolean onTouch(View v, MotionEvent event) {
				
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_con.setBackgroundDrawable(
							getResources().getDrawable(R.drawable.btn_kcon_b));
				}
				else{
					button_con.setBackgroundDrawable(
							getResources().getDrawable(R.drawable.btn_kcon_a));
					
					Intent i = new Intent(TwoPageViewer.this, AgendaCon.class);
					startActivity(i);
				}
				return false;
			}
		});
        
        final Button button_font = (Button)findViewById(R.id.MM_button_font);
        button_font.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_font.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_kfont_b));
				}
				else{
					
					button_font.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_kfont_a));					
										
					if(m_topbar_menu_type == 1){
						RemoveTopBarMenu();
						m_topbar_menu_type = 0;
						return true;
					}
					
					OpenFontMenu();
				}
				return true;
			}
		});
        
        final Button button_search = (Button)findViewById(R.id.MM_button_sch);
        button_search.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_search.setBackgroundDrawable(
							getResources().getDrawable(R.drawable.btn_ksch_b));
				}
				else{
					button_search.setBackgroundDrawable(
							getResources().getDrawable(R.drawable.btn_ksch_a));
					
					if(m_topbar_menu_type == 2){
						RemoveTopBarMenu();
						m_topbar_menu_type = 0;
						return true;
					}
					
					OpenSearchMenu();
				}
				return false;
			}
		});
        
        final Button button_auto = (Button)findViewById(R.id.MM_button_auto);
        button_auto.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_auto.setBackgroundDrawable(
							getResources().getDrawable(R.drawable.btn_kauto_b));
				}
				else{
					button_auto.setBackgroundDrawable(
							getResources().getDrawable(R.drawable.btn_kauto_a));
					
					if(m_topbar_menu_type == 3){
						RemoveTopBarMenu();
						m_topbar_menu_type = 0;
						return true;
					}
					
					OpenAutoMenu();
				}
				
				return false;
			}
		});
	}
	
	
	void OpenAutoMenu()
	{
		RemoveTopBarMenu();
		
		m_topbar_menu_type = 3;
		
		m_topbar_menu = AutoView.OpenAutoMenu(this, m_handler);
	}
	
	void OpenSearchMenu()
	{
		RemoveTopBarMenu();
		
		m_topbar_menu = SearchView.OpenSearchMenu(this);
		
		m_topbar_menu_type = 2;
	}
	
	public void refreshPage() // FontManager에서 사용.
	{
		/*
		if (Global.g_CurEpubBook.GetCurPage() > 0) {
			m_prevPage = Bitmap.createBitmap(m_prevPage.getWidth(), m_prevPage.getHeight(), Bitmap.Config.ARGB_8888);
			
			Canvas c = new Canvas(m_prevPage);
			m_textView.DrawOnePage(c, 
				new Rect(0, 0, Global.g_ScreenFrame.width(), Global.g_ScreenFrame.height()), 
				Global.g_CurEpubBook.GetCurPage() - 1);
		}*/
		
		if(Global.g_CurEpubBook.curPage % 2 > 0)
			Global.g_CurEpubBook.curPage--;
		
		// 화면 다시 그림
		m_textView.invalidate();
		RemoveTopBarMenu();
		
		final SeekBar bar = (SeekBar)findViewById(R.id.MM_seekbar);
		bar.setProgress((int)((float)Global.g_CurEpubBook.GetCurPage() / (float)Global.g_CurEpubBook.GetTotalPage() * 100.0f));
	}
	
	void OpenFontMenu() // 글꼴 설정 버튼 클릭시 메뉴 띄움.
	{
		RemoveTopBarMenu();
		
		m_topbar_menu_type = 1;
		
		m_topbar_menu = FontManagerView.OpenFontMenu(this);
	}
	
	public void RemoveTopBarMenu()
	{
		final RelativeLayout back = (RelativeLayout)findViewById(R.id.MM_rel_back);
		
		if(m_topbar_menu != null){
			m_topbar_menu.clearFocus();
			back.removeView(m_topbar_menu);
		}
		
		m_topbar_menu = null;
		m_topbar_menu_type = 0;
	}
	
	void RemoveMenu()
	{		
		if (Global.g_curRecordingPath != null) {
			try {
				// 혹시 녹음중이었다면 종료
				Global.g_curRecorder.stop();
				
				// 메모를 지정
				//MemoInfo memo = new MemoInfo();
				//memo.set(1, Global.g_SelMin, Global.g_SelMax, m_recodingPath);
				//Global.g_CurEpubBook.AddMemo(memo);
			} catch (Exception e) {
			} finally {
				Global.g_curRecorder.release();
				
				Global.g_curRecordingPath = null;
			}
		}
		
		if(Global.g_curWordMenu == null)
			return;
		
		RelativeLayout back = (RelativeLayout)findViewById(R.id.MM_rel_back);
		Global.g_curWordMenu .clearFocus();
		back.removeView(Global.g_curWordMenu );
		Global.g_curWordMenu  = null;
	}
	
	void ReleaseSelection()
	{
		Global.g_SelMax = null;
		Global.g_SelMin = null;
		
		RemoveMenu();
		
		m_textView.invalidate();
	}
	
	void SetSelection()
	{
		ReleaseSelection();
		
		Log.i("debug", "SetSelection");
		
		if(m_selStart.compare(m_selFinish) == Global.NSOrderedDescending){
			Global.g_SelMax = m_selStart;
			Global.g_SelMin = m_selFinish;
		}
		else{
			Global.g_SelMin = m_selStart;
			Global.g_SelMax = m_selFinish;
		}
		
		m_selStart = null;
		m_selFinish = null;
		
		m_textView.invalidate();
		
		Global.g_curMemo = null;
		
		MemoInfo memo = Global.g_CurEpubBook.IsInMemo(Global.g_SelMin.content.contentIdx, 
				Global.g_SelMin.paragraphIdx, Global.g_SelMin.textIdx, Global.g_SelMin.charIdx);
		
		if(memo != null){
			Global.g_curMemo = memo;
			
			if(memo.type == 0)
			{
				// 메모 - 메모 메뉴를 열었을 경우 기존 메모가 뜸.
			}
			else if(memo.type == 1){
				// 음성메모
				
			}
			else if(memo.type == 2){
				// 형광펜
				
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.setTitle("알림");
				alert.setMessage("지우시겠습니까?");
				alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Global.g_CurEpubBook.DeleteMemo(Global.g_curMemo);
						ReleaseSelection();
						dialog.dismiss();
					}
				});
				
				alert.setNegativeButton("취소", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss(); // 다이얼로그 없앰.
					}
				});
				
				alert.show();
				
			}
		}
	}
	
	public void FindSpaceFrom(SelectedParagraphInfo start, SelectedParagraphInfo finish)
	{		
		SelectedParagraphInfo newStart = new SelectedParagraphInfo(start);
		SelectedParagraphInfo newFinish = new SelectedParagraphInfo(finish);
		
		char[] pre = new char[] {' ', '.', '(', ')', '\"', '\'', ','};
		int i;
		while(newStart.charIdx > 0)
		{
			newStart.charIdx--;
			String word = Global.g_CurEpubBook.GetTextInSelectionFrom(newStart, start);
			boolean flag = false;
			if(word.length() > 0){
				for(i=0;i<=pre.length-1;i++){
					if(pre[i] == word.charAt(0)){
						flag = true;
						break;
					}
				}
			}
			
			if(flag){
				newStart.charIdx++;
				break;
			}
		}
		
		int cnt=0;
		while(cnt++ < 200){
			newFinish.charIdx++;
			String word = Global.g_CurEpubBook.GetTextInSelectionFrom(finish, newFinish);			
			boolean flag = false;
			if(word.length() > 0 && word.length() >= cnt){
				for(i=0;i<=pre.length-1;i++){
					if(pre[i] == word.charAt(word.length()-1)){
						newFinish.charIdx--;
						flag = true;
						break;
					}
				}
			}
			else
				flag = true;
			
			if(flag){
				//newFinish.charIdx--;
				break;
			}
		}
		
		start.Copy(newStart);
		finish.Copy(newFinish);
	}
	
	void touchesBegan(int x, int y)
	{
		ReleaseSelection();
		
		Point pos = new Point();
		pos.set(x, y);
		
		m_selStart = new SelectedParagraphInfo();
		
		int current = Global.g_CurEpubBook.curPage;

		HtmlContent htmlcontent = Global.g_CurEpubBook.GetContentOfPage(current);		
		int currentPage = Global.g_CurEpubBook.GetContentPageOfPage(current);		
		Page page = htmlcontent.Pages.get(currentPage);
		
		//Page page = m_htmlContent.Pages.get(current); // TODO: 버그 잡을 것
		// ERROR/AndroidRuntime(18528): java.lang.IndexOutOfBoundsException: Invalid index 16, size is 2
		// 양쪽보기에서 페이지를 뒤로 빠르게 넘기다 보면 여기서 죽음
		
		m_selStart.content = htmlcontent;
		m_selStart.page = page;
		m_selStart.margin = 0;
		
		int flag = 0;
		for(int i = page.StartParaIdx; i <= page.EndParaIdx; ++i) {
			
			Paragraph para = htmlcontent.paragraphs.get(i);
			m_selStart.paragraphIdx = i;
			
			if(para.GetTextAtPosition(pos, m_selStart)){
				m_selFinish = m_selStart;
				flag = 1;
				break;
			}
		}
		
		if(flag == 1)
			return;
		
		HtmlContent htmlcontent2 = Global.g_CurEpubBook.GetContentOfPage(current+1);		
		int currentPage2 = Global.g_CurEpubBook.GetContentPageOfPage(current+1);		
		Page page2 = htmlcontent2.Pages.get(currentPage2);
		
		m_selStart.content = htmlcontent2;
		m_selStart.page = page2;
		m_selStart.margin = Global.g_ScreenFrame.width() / 2;
		
		pos.set(x - m_selStart.margin, y);
		
		for(int i = page2.StartParaIdx; i <= page2.EndParaIdx; ++i) {
			
			Paragraph para = htmlcontent2.paragraphs.get(i);
			m_selStart.paragraphIdx = i;
			
			if(para.GetTextAtPosition(pos, m_selStart)){
				m_selFinish = m_selStart;
				flag = 1;
				break;
			}
		}
	}
	
	boolean touchesMoved(int x, int y)
	{		
		boolean flag = false;
		// 시간 처리는 앞에서 하고 들어옴. 시간이 넘는 경우에만 함수 호출.
		//if(System.currentTimeMillis() - m_starttime > 300){
			if(m_selStart != null)
				flag = true;
		//}
			
			
		if(flag == true){			
			flag = false;
			Point pos = new Point();
			//pos.set(x, y);
			m_selFinish = new SelectedParagraphInfo();
			m_selFinish.content = m_selStart.content;
			m_selFinish.page = m_selStart.page;
			m_selFinish.margin = m_selStart.margin;
			
			pos.set(x - m_selFinish.margin, y);
			
			for(int i = m_selFinish.page.StartParaIdx; i <= m_selFinish.page.EndParaIdx; ++i) {
				Paragraph para = m_selFinish.content.paragraphs.get(i);
				m_selFinish.paragraphIdx = i;				
				
				if(para.GetTextAtPosition(pos, m_selFinish)){
					flag = true;
					break;
				}
			}
			
			if(flag){
				// 한 자리에서 오래 터치하였을 경우 자동으로 한 단어를 찾아서 선택.
				if((m_start_x - x <= 50 || x - m_start_x <= 50)
						&& (m_start_y - y <= 50 || y - m_start_y <= 50)){ 					
					FindSpaceFrom(m_selStart, m_selFinish);
				}
				
				SetSelection();
				return true;
			}
		}
		
		return false;
	}
	
	void OpenWordMenu(int x, int y) // 글자가 선택되어야만 열림.
	{		
		final LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
		final RelativeLayout back = (RelativeLayout)findViewById(R.id.MM_rel_back);
		
		final int left;
		final int top = y + 25;
		
		// 메뉴가 잘리지 않게 하기 위해서 메뉴 열리는 위치를 당김.
		if(x > 600)
			left = 600;
		else
			left = x;
		
		if(Global.g_curMemo != null && Global.g_curMemo.type == 1){
			// 음성메모
			
			View recmenu = inflater.inflate(R.layout.recmenu2, null);
			back.addView(recmenu);
			Global.g_curWordMenu = recmenu;
			
			LinearLayout menu = (LinearLayout)findViewById(R.id.RC_recmenu2);
	        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)menu.getLayoutParams();
	        
	        params.topMargin = top;
	        params.leftMargin = left;
	        
	        menu.setLayoutParams(params);
	        
	        final Button button_play = (Button)findViewById(R.id.RC_button_play);
	        button_play.setOnTouchListener(new View.OnTouchListener() {
				
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction() == MotionEvent.ACTION_DOWN){
						button_play.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_play_b));
					}
					else{
						
						try {
							//Log.i("RECORD", "재생 시작" + Global.g_curMemo.content);
							
							m_player.reset();
							m_player.setDataSource(Global.g_curMemo.content);
							m_player.setLooping(false);
							m_player.prepare();
							
							m_player.setOnCompletionListener(new OnCompletionListener() {
								public void onCompletion(MediaPlayer arg0) {
									// TODO : 재생이 끝나면 자동으로 stop 버튼으로 돌아오도록 설정.
									
									// 여기가 재생이 끝나면 들어오는 코드 영역
									Log.i("RECORD", "녹음 재생이 완료됨");
								}
							});
							
							m_player.start();
														
							button_play.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_stop));
							button_play.setOnTouchListener(new View.OnTouchListener() {
								
								public boolean onTouch(View v, MotionEvent event) {
									if(event.getAction() == MotionEvent.ACTION_DOWN){
										button_play.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_stop_b));
									}
									else{
										button_play.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_play));
										
										// 스탑 버튼을 눌렀을 때 정지
										if (m_player.isPlaying())
											m_player.stop();
										
										// TODO : 정지 버튼을 눌러서 |> 버튼으로 만들고 다시 재생을 누르면 재생 안됨
										// 꼭 다른 단어를 클릭해서 재생 팝업을 없앴다가 다시 띄워서 눌러야 재생 됨
									}
									return false;
								}
							});
						} catch (IOException e) {
							new AlertDialog.Builder(TwoPageViewer.this)
							.setTitle("녹음 재생 실패")
							.setMessage(e.toString())
							.setPositiveButton("확인", null)
							.show();
						}						
					}
					
					return false;
				}
			});
	        
	        final Button button_rem = (Button)findViewById(R.id.RC_button_remove);
	        button_rem.setOnTouchListener(new View.OnTouchListener() {
				
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction() == MotionEvent.ACTION_DOWN){
						button_rem.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_x_b));
					}
					else{
						Global.g_CurEpubBook.DeleteMemo(Global.g_curMemo);
						ReleaseSelection();
						RemoveMenu();
					}
					return false;
				}
			});
	        
	        return;
		}
		
		
		// WordMenuView.OpenWordMenu(TwoPageViewer.this, top, left);
		
		//m_wordmenu = inflater.inflate(R.layout.wordmenu, null);
		//back.addView(m_wordmenu);
		//RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)m_wordmenu.getLayoutParams();
		//params.topMargin = top;
		//params.leftMargin = left;
	}
	
    @Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
		super.onConfigurationChanged(newConfig);
		
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) { // 세로 전환시 발생
			
			//final RelativeLayout back = (RelativeLayout)findViewById(R.id.MM_rel_back);
			//if(Global.g_curMp3Bar != null)
			//	back.removeView(Global.g_curMp3Bar);
			
			//back.clearFocus();
			//ReleaseSelection();
			
	    	//Intent i = new Intent(TwoPageViewer.this, OnePageViewer.class);
	    	//i.putExtra("tabbar", m_tabbar);
	    	//startActivity(i);
			
			Intent i = getIntent();
			i.putExtra("rotate", true);
			i.putExtra("tabbar", m_tabbar);
			setResult(RESULT_OK, i);
	    	finish();
		}
		else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) 
		{ // 가로 전환시 발생

		}
	}
}
