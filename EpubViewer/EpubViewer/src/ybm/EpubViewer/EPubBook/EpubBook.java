package ybm.EpubViewer.EPubBook;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import ybm.EpubViewer.EPubViewer;
import ybm.EpubViewer.Global;
import ybm.EpubViewer.Logger;
import ybm.EpubViewer.Util;
import ybm.EpubViewer.ZipDecryptInputStream;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

public class EpubBook {

	// ZipArchive* EpubZip;
    HashMap<String, byte[]> EpubZip;

	String rootFolder;	
	String bookpath;
	ArrayList<EpubItem> Items;
	EpubSpine Spine;
	public EpubToc Toc;
	/*
	 * //Spine과 연결될 Item (EpubItem)
	 * 
	 * StyleCSSs는 key : 스타일 파일 이름 value: (클래스 - Context)로
	 * 이루어진 NSMutableDictionary // Context는 Key와 Value로 이루어진
	 * NSMutableDictionary이다 // 
	 */
	 HashMap<String, HashMap<String, HashMap<String, String>>> StyleCSSs = null;
	 public ArrayList<HtmlContent> htmlcontents;//실제 html을 읽어서 보관한다
	 public int curPage;
	 TextInfo loadedTextInfo;//책이 열렸을때 이전 책의 페이징 정보를 담아둠
	 public ArrayList<BookMarkPos> BookMarks;
	 public ArrayList<MemoInfo> Memos;

	 public String bookId = null;
	 
	 String curItemId;
	 Context activityContext;//파일 입출력을 위해 받아옴 
	 /* 
	 * 
	 *; //현재 보이는 아이템 
	 *  ArrayList szColors;
	 * 
	 * ;//메모를 남겼을때 위치와 내용을 담는 MemoInfo들의 array ArrayList BookMarks;
	 */

	public EpubBook(Context ct) {
		activityContext = ct;
		// 변수 초기화.
		Items = new ArrayList<EpubItem>();
		Spine = new EpubSpine();
		Toc = new EpubToc();
		EpubZip = new HashMap<String, byte[]>();
		htmlcontents = new ArrayList<HtmlContent>();
		//Global.g_FontMgr = new  FontManager();
		BookMarks = new ArrayList<BookMarkPos>();
		Memos = new ArrayList<MemoInfo>();
		//Global.g_ScreenFrame = new Rect(0, 0, 600, 1004);
		//Global.g_PageFrame = new Rect(0, 0, 400, 900);

		curPage = 0;
	}

	public void ProcessPaging()
	{
		
		for (int i = 0; i < htmlcontents.size(); ++i)
		{
			EPubViewer.setOpenProgressMessage("컨텐츠 위치 계산중 " + (i + 1) + " / " + htmlcontents.size());
			
			HtmlContent content = htmlcontents.get(i);
			//Log.i("debug", "ProcessPaging html " + content);
			content.CalculatePage(Global.g_PageFrame);
		}
		
		//book mark page를 다시 계산한다 
		for(int i = 0; i < BookMarks.size(); ++i)
		{
			BookMarkPos bmp = BookMarks.get(i);
			bmp.pageNo =  GetPageOfTextInfo(bmp);
		}
		
		
		//차례보기를 만들자
		//Tag - Page 
	}
	
	public int GetPageOfTextInfo(TextInfo info)
	{
		if (info != null)
		{
			int calculPage = 0;
			int cIdx = (info.contentIdx < htmlcontents.size() - 1)?info.contentIdx: htmlcontents.size() - 1;
			for (int i = 0; i <= cIdx; ++i)
			{
				HtmlContent content = htmlcontents.get(i);
				
				if (i == info.contentIdx)
				{
					if (info.paragraphIdx >= content.paragraphs.size())
						return 0;
					
					Paragraph para = content.paragraphs.get(info.paragraphIdx);
					int line = para.GetLineFromTextIdx(info.textIdx, info.charIdx);
		
					for (int j = 0; j < content.Pages.size(); ++j)
					{
						Page page = content.Pages.get(j);
						if (page.StartParaIdx == info.paragraphIdx && page.StartLine <= line || page.StartParaIdx <= info.paragraphIdx)
						{
							if (page.EndParaIdx == info.paragraphIdx && (page.EndLine > line || page.EndLine == -1) || page.EndParaIdx > info.paragraphIdx)
							{
								return calculPage + j;
							}
						}
					}
				}
				calculPage += content.Pages.size();
			}
		}
		
		//Log.i("debug" , "GetPageOfTextInfo return 0");
		return 0;
	}

	public Boolean Open(String filename)
	{
		bookpath = filename;
		Boolean ret = true;
		
		if (bookpath != null) 
		{
			// config 파일이 있는지 찾아봄. (캐시파일이 정상적으로 생성되면 config 파일이 생성됨.)
			// 나중에 config 파일에 필요한 것을 넣음.
			String confFilename = bookpath + ".conf";
			
			// 암호를 푼 책 파일 이름
			String decFilename = bookpath + ".cache";
			
			//Log.i("NEW ZIP", "READ CAHCE " + decFilename);
			
	   		FileInputStream fis = null;
	   		
	   		// config 파일을 먼저 찾음
	   		try {	   			
	   			fis = new FileInputStream(confFilename);
	   			
	   			try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   			
	   			fis = new FileInputStream(decFilename);
	   			
	   			Log.i("NEW ZIP", "CACHE FOUND!");
	   		} catch (FileNotFoundException e_) {
	   			Log.i("NEW ZIP", "CACHE NOT FOUND");
			
	   			try {
					fis = new FileInputStream(bookpath);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				
				EPubViewer.setOpenProgressMessage("캐싱 중..");
				
				// 원본 압축 파일에서 몽땅 읽어서
				ZipDecryptInputStream zdis = new ZipDecryptInputStream(fis, "eoqkrsktp.");
		        ZipInputStream zis = new ZipInputStream(zdis);
		        ZipEntry ze;

		        // 새 무압축 파일로 이동할 것임
		        FileOutputStream fo = null;
				
				//Log.i("NEW ZIP", "FILENAME=" + decFilename);
				
				try {
					fo = new FileOutputStream(decFilename);
					
					ZipOutputStream zos = new ZipOutputStream(fo);
					zos.setLevel(0);
					
					while ((ze = zis.getNextEntry()) != null) 
					{
						zos.putNextEntry(new ZipEntry(ze.getName()));
						
						final int BUFFER_SIZE = 1024;
						byte buf[] = new byte[BUFFER_SIZE];
						int readBytes;
						int totalBytes = 0;
						
						while ((readBytes = zis.read(buf, 0, BUFFER_SIZE)) > 0) {
							// xor 연산하여 저장
							for (int i = 0; i < readBytes; i++)
								buf[i] ^= 1;
							
							zos.write(buf, 0, readBytes);
							totalBytes += readBytes;
						}
						
						zos.closeEntry();
						zis.closeEntry();
						
						//Log.i("NEW ZIP", "WRITE " + ze.getName() + " " + totalBytes + " BYTES");
					}
					
					zos.close();
					fo.close();
					
			        zis.close();
			        fis.close();
			        
			        try{
			        	fo = new FileOutputStream(confFilename);
			        	fo.close();
			        }
			        catch(Exception e)
			        {
			        	e.printStackTrace();			  
			        }
			       			       
			        
			        // 재시도
			        return Open(filename);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
	   		}
	   		
			
			EPubViewer.setOpenProgressMessage("압축 해제 중..");
			
			//순차적으로 밖에 못읽어서 어쩔수 없이 메모리에 다 담는다..
			//용량의 압박-_-; 그때그때 읽자니 너무 느림 
			ZipInputStream zis = new ZipInputStream(fis);
	        ZipEntry ze;
	        try {
	        	while ((ze = zis.getNextEntry()) != null) 
				{
	        		ArrayList<byte[]> bytes = new ArrayList<byte[]>();
	        		ArrayList<Integer> sizes = new ArrayList<Integer>();
	        		
	        		final int BUFFER_SIZE = 1024;
	        		byte buf[] = new byte[BUFFER_SIZE];
	        		int totalBytes = 0, readBytes;
	        		
	        		while ((readBytes = zis.read(buf, 0, BUFFER_SIZE)) > 0) {
	        			totalBytes += readBytes;
	        			
						// xor 연산하여 구함
						for (int i = 0; i < readBytes; i++)
							buf[i] ^= 1;
	        			
	        			bytes.add(buf);
	        			sizes.add(readBytes);
	        			
	        			buf = new byte[BUFFER_SIZE]; // new buffer
	        		}
	        		
	        		buf = new byte[totalBytes];
	        		int dstPos = 0;
	        		
	        		for (int i = 0; i < bytes.size(); i++) {
	        			System.arraycopy(bytes.get(i), 0, buf, dstPos, sizes.get(i));
	        			dstPos += sizes.get(i);
	        		}
	        		
	        		EpubZip.put(ze.getName(), buf);
	        		
	        		//Log.i("NEW ZIP", "ENTRY=" + ze.getName() + ", SIZE=" + totalBytes);
	        		
	        		zis.closeEntry();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	        //1) zip을 풀고 container file을 불러서 rootfile의 위치를 알아온다 
			String rootfile = null;

			byte[] container = null;
			if ((container = EpubZip.get("META-INF/container.xml")) != null )
			{
				rootfile = new EpubContainerParser(container).rootfile;
				container = null;
				EpubZip.remove("META-INF/container.xml");//차지하는 메모리 용량을 줄이기 위해 다 읽은 파일은 삭제  
			}
			
			if (rootfile != null)
			{
				//rootfile의 root folder를 저장
				rootFolder = rootfile.substring(0, rootfile.lastIndexOf('/') + 1);
				
	//2) root file에서 실제 파일 위치를 불러온다
				if ((container = EpubZip.get(rootfile)) != null )
				{
					new EpubRootFileParser(container, Spine, Items);
					container = null;
					EpubZip.remove(rootfile);
				}

	//3) Spine의 TOC파일을 파싱한다.
				String tocfile = rootFolder + GetItemHRef(Spine.toc);
				if ((container = EpubZip.get(tocfile)) != null )
				{
					EPubViewer.setOpenProgressMessage("파싱 중..");
					
					new EpubTocFileParser(container, Toc);
					container = null;
					EpubZip.remove(tocfile);
				}
			
	//4) Spine의 다른 파일들을 파싱한다.
				int itemcnt = Spine.GetItemCount();
				EpubHtmlFileParser htmlparser = new EpubHtmlFileParser();
				htmlparser.rootfolder = rootFolder;
	
				for (int i = 0;i < itemcnt; ++i)
				{
					EPubViewer.setOpenProgressMessage("파싱 중.. " + (i + 1) + " / " + itemcnt);
					
					String itemId = Spine.GetItemAtIndex(i);
					EpubItem item = GetItem(itemId);
					if (item != null && item.isHtml())
					{
						String fileToRead = new StringBuilder(50).append(rootFolder).append(item.href).toString();
						
						if ((container = EpubZip.get(fileToRead)) != null )
						{
							Logger.d("[ITEM]",item.href);
							htmlparser.fullpath = fileToRead;
							HtmlContent cont = htmlparser.Parser(container, item.itemId);
							cont.contentIdx = htmlcontents.size();
							htmlcontents.add(cont);
							container = null;
							EpubZip.remove(fileToRead);
							//Logger.d("[ITEM]- end",item.href);
						}
					}
				}

				
				if (loadedTextInfo == null)
					loadedTextInfo = new TextInfo();
				LoadBookReadingInfo();
				ProcessPaging();
				curPage = 0;//[self GetPageOfTextInfo:loadedTextInfo];
				loadedTextInfo = null;
				//5) 첫 페이지를 spine의 첫번째 부분으로 세팅 - 아마도 id : cover
				curItemId = Spine.GetItemAtIndex(0);
			
			}
			else
			{
				ret = false;
			}
			try {
				zis.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		}

		Global.g_FontMgr.UpdateFontList();

		return ret;
	}

	Bitmap GetImageFromFile(String fileToRead)
	{
		byte[] container = null;
		if ((container = EpubZip.get(fileToRead)) != null )
		{
			return BitmapFactory.decodeByteArray(container, 0, container.length);
		}
		return null;
	}
	
	public byte[] GetRecFromFile(String fileToRead)
	{
		byte[] container = null;
		if ((container = EpubZip.get(fileToRead)) != null )
		{
			return container;
		}
		return null;
	}
	
	private EpubItem GetItem(String itemid)
	{
		int size = Items.size();
		for (int i = 0; i < size; i++)
		{
			EpubItem item = Items.get(i);
			if (item.itemId.compareToIgnoreCase(itemid) == 0)
			{
				return item;
			}
		}
		return null;
	}

	private String GetItemHRef(String itemid)
	{
		EpubItem item = GetItem(itemid);

		if (item != null)
			return item.href;
		
		return null;
	}

	HashMap<String, HashMap<String, String>> GetStyleCSS(String fileToRead)
	{	
		if (StyleCSSs == null)
		{
			StyleCSSs = new HashMap<String, HashMap<String, HashMap<String, String>>>(2);
		}
	//1)해당 스타일 파일이 이전에 파싱이 되어있나?
		HashMap<String, HashMap<String, String>> contents = StyleCSSs.get(fileToRead);
		
		if(contents != null)
			return contents;
	//2)파싱하자 
		contents = new HashMap<String, HashMap<String, String>>(50);//<-2번쨰 dic (class- value)값을 넣어줘야함
	
		StyleCSSParser cssparser = new StyleCSSParser();
		cssparser.Parse(fileToRead, contents);
		
		StyleCSSs.put(fileToRead, contents);
		return StyleCSSs.get(fileToRead);
	}
	
	HashMap<String, HashMap<String, String>> GetStyleCSSFromString(StringBuilder styleInHtml)
	{
		HashMap<String, HashMap<String, String>> contents = new HashMap<String, HashMap<String, String>>();
		StyleCSSParser cssparser = new  StyleCSSParser();
		cssparser.ParseData(styleInHtml.toString().getBytes(), contents);
		return contents;
	}
	

	public MemoInfo IsInMemo(int contentIdx, int paraIdx, int textIdx, int charIdx)
	{
		int size = Memos.size();
		for (int i = 0; i < size; ++i)
		{
			MemoInfo memo = Memos.get(i);
			
			if (memo == null || 
					memo.start == null || memo.start.content == null || memo.start.content.contentIdx > contentIdx || 
					memo.end == null || memo.end.content == null || memo.end.content.contentIdx < contentIdx)
			{
				continue;
			}
			
			if (memo.start.content.contentIdx == contentIdx)
			{
				if (memo.start.paragraphIdx > paraIdx)
					continue;
				if (memo.start.paragraphIdx == paraIdx)
				{
					if (memo.start.textIdx > textIdx)
						continue;
					if (memo.start.textIdx == textIdx && memo.start.charIdx > charIdx)
						continue;
				}
			}

			if (memo.end.content.contentIdx == contentIdx)
			{
				if (memo.end.paragraphIdx < paraIdx)
					continue;
				if (memo.end.paragraphIdx == paraIdx)
				{
					if (memo.end.textIdx < textIdx)
						continue;
					if (memo.end.textIdx == textIdx && memo.end.charIdx < charIdx)
						continue;
				}
			}
			return memo;
		}
		return null;
	}

	public int GetTotalPage()
	{
		int totalpage = 0;
		int size = htmlcontents.size();
		for (int i = 0; i < size; ++i)
		{
			HtmlContent content = htmlcontents.get(i);
			totalpage += content.Pages.size();
		}
		return totalpage;
	}

	public int GetCurPage()
	{
		return curPage;
	}

	int GetCurContentPage()
	{
		return GetContentPageOfPage(curPage);
	}

	public int GetStartPageOfContent(HtmlContent content)
	{
		int calculPage = 0;
		int size = htmlcontents.size();
		for (int i = 0; i < size; ++i)
		{
			HtmlContent cur = htmlcontents.get(i);
			if (cur == content)
			{
				return calculPage;
			}
			calculPage += cur.Pages.size();
		}
		return -1;
	}

	public HtmlContent GetCurContent()
	{
		return GetContentOfPage(curPage);
	}

	public HtmlContent GetContentOfPage(int page)
	{
		if (page < GetTotalPage())
		{
			int calculPage = 0;
			int size = htmlcontents.size();
			for (int i = 0; i < size; ++i)
			{
				HtmlContent content = htmlcontents.get(i);
				int count = content.Pages.size();
				if (calculPage + count > page)
					return content;
				calculPage += count;
			}
		}
		return null;
	}

	int IndexOfConent(HtmlContent content)//content의 index를 return해준다 
	{
		int size =  htmlcontents.size();
		for (int i = 0; i < size; ++i)
		{
			HtmlContent _content = htmlcontents.get(i);
			if (_content == content)
				return i;
		}
		return -1;
	}

	public int GetContentPageOfPage(float page)
	{
		if (page < GetTotalPage())
		{
			int calculPage = 0;
			for (int i = 0; i < htmlcontents.size(); ++i)
			{
				HtmlContent content = htmlcontents.get(i);
				
				if (calculPage + content.Pages.size() > page)
				{
					return (int) (page - calculPage);
				}
				calculPage += content.Pages.size();
			}
		}
		return -1;
	}

	
	byte[] GetDataFromFile(String fileToRead)
	{
		byte[] container = null;
		if ((container = EpubZip.get(fileToRead)) != null )
		{
			return container;
		}
		return null;
	}

	void Close()
	{	
		SaveBookReadingInfo();
/*
		if (EpubZip)
		{
			EpubZip = null;
		}
		if (Items)
			[Items release];
		if (Spine)
			[Spine release];
		if (Toc)
			[Toc release];
		if (StyleCSSs)		
			[StyleCSSs release];//이 안쪽도 알아서 release가 되나?	
		
		if (g_FontMgr) 
			[g_FontMgr release];
		
		if (htmlcontents)
			[htmlcontents release];
		
		if (BookMarks)
			[BookMarks release];
		
		if (Memos)
			[Memos release];
		
		[super Close];*/
	}
	
	public MemoInfo AddMemo(MemoInfo memo)
	{
		int smallIdx = -1;//지금 북마크가 들어갈 위치보다 작은 인덱스 
		for(int i = 0; i < Memos.size(); ++i)
		{
			MemoInfo _memo = Memos.get(i);
			if (_memo.start.compare(memo.start) == Global.NSOrderedDescending)
				break;
			smallIdx++;
		}
		
		Memos.add(smallIdx + 1, memo);
		SaveMemos();
		return Memos.get(smallIdx + 1);
	}

	public void DeleteMemo(MemoInfo memo)
	{
		if (memo.type == 1) //음성 메모라면 음성 메모 파일을 지운다 
		{
			//String dirPath = activityContext.getFilesDir().getAbsolutePath();
			//String filename = dirPath + "/"+memo.content;
			
			final String filename = memo.content; // 안드로이드 버전은 컨텐츠에 경로까지 다 저장
			
			File file = new File(filename);
			file.delete();
			}
		Memos.remove(memo);
		SaveMemos();
	}

	public HtmlContent GetHtmlContentOfHref(String href)
	{
		for (int i = 0; i <Items.size(); ++i)
		{
			EpubItem item = Items.get(i);
			if (item.href.compareTo(href) == 0)
			{
				for (int j = 0; j <htmlcontents.size(); j++)
				{
					HtmlContent content = htmlcontents.get(j);
					if (content.itemId.compareTo(item.itemId) == 0)
					{
						return content;
					}
				}
			}
		}
		return null;
	}
	
	Paragraph GetNextParagraphOfParagraph(Paragraph para)
	{
		Boolean bNext = false;
		for (int i = 0; i < htmlcontents.size(); ++i)
		{
			HtmlContent content = htmlcontents.get(i);
			for (int j = 0; j < content.paragraphs.size(); ++j)
			{
				Paragraph _para = content.paragraphs.get(j);
				if (bNext)
					return _para;
				if (_para == para)
					bNext = true;
			}
		}
		return null;
	}

	Paragraph GetFirstParagraphOfPage(int pageNo)
	{
		HtmlContent htmlcontent = GetContentOfPage(pageNo); 
		int pageInContent = GetContentPageOfPage(pageNo);
		Page page = htmlcontent.Pages.get(pageInContent);
		Paragraph para = htmlcontent.paragraphs.get(page.StartParaIdx);
		return para;
	}

	int GetFirstLineOfPage(int pageNo)
	{
		HtmlContent htmlcontent = GetContentOfPage(pageNo); 
		int pageInContent = GetContentPageOfPage(pageNo);
		Page page = htmlcontent.Pages.get(pageInContent);
		return page.StartLine;
	}

	public void GetTextInfoOfPage(int pageNo, TextInfo ret)
	{
		if (ret != null)
		{
			if (pageNo < GetTotalPage())
			{
				int calculPage = 0;
				for (int i = 0; i < htmlcontents.size(); ++i)
				{
					HtmlContent content = htmlcontents.get(i);
					
					if (calculPage + content.Pages.size() > pageNo)
					{
						int pageInContent = pageNo - calculPage;
						Page page = content.Pages.get(pageInContent);
						
						int startparaIdx = page.StartParaIdx;
						int startlineIdx = page.StartLine;

						Paragraph para = content.paragraphs.get(startparaIdx);
						if (startlineIdx >= para.lineCount)
						{
							++startparaIdx;
							if (content.paragraphs.size() > startparaIdx)
							{
								para = content.paragraphs.get(startparaIdx);
								startlineIdx = 0;
							}
							else 
							{
								ret.contentIdx = -1;
								return;
							}
						}
						
						LineData linedata = para.GetLineData(startlineIdx);
						
						ret.contentIdx = i;
						ret.paragraphIdx = startparaIdx;
					
						ret.textIdx = (linedata != null)?linedata.startText:0; //본 라인은 몇번째 text에서 시작하는가?
						ret.charIdx = (linedata != null)?linedata.startChar:0; //본 라인은 위 text의 몇 번째 글자에서 시작하는가?
						return;
					}
					calculPage += content.Pages.size();
				}
			}	
		}
	}

	public HtmlContent GetHtmlContentAtIdx(int idx)
	{
		if (htmlcontents.size() > idx)
			return htmlcontents.get(idx);
		return null;
	}

	public void AddBookMark(TextInfo pos)
	{
		int smallIdx = -1;//지금 북마크가 들어갈 위치보다 작은 인덱스 
		for(int i = 0; i < BookMarks.size(); ++i)
		{
			BookMarkPos bmp = BookMarks.get(i);
			if (bmp.contentIdx == pos.contentIdx &&
				bmp.paragraphIdx == pos.paragraphIdx &&
				bmp.textIdx == pos.textIdx &&
				bmp.charIdx == pos.charIdx)
				return;

			if (bmp.contentIdx < pos.contentIdx )
				smallIdx = i;
			else if (bmp.contentIdx == pos.contentIdx)
			{
				if (bmp.paragraphIdx < pos.paragraphIdx)
					smallIdx = i;
				else if (bmp.paragraphIdx == pos.paragraphIdx)
				{
					if (bmp.textIdx < pos.textIdx)
						smallIdx = i;
					else if (bmp.textIdx == pos.textIdx)
					{
						if (bmp.charIdx < pos.charIdx)
							smallIdx = i;
					}
				}
			}
		}
		
		BookMarkPos bmp = new BookMarkPos();
		bmp.contentIdx = pos.contentIdx;
		bmp.paragraphIdx = pos.paragraphIdx;
		bmp.textIdx = pos.textIdx;
		bmp.charIdx = pos.charIdx;
		bmp.pageNo = GetPageOfTextInfo(pos);

		Calendar nowDate = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"));
		bmp.day = nowDate.DATE;
		bmp.year= nowDate.YEAR;
		bmp.month = nowDate.MONTH;
		bmp.hour = nowDate.HOUR;
		
		BookMarks.add(smallIdx + 1, bmp);
		SaveMemos();
	}

	int GetPageOfParagraph(Paragraph para)
	{
		int calculPage = 0;
		for (int i = 0; i < htmlcontents.size(); ++i)
		{
			HtmlContent content = htmlcontents.get(i);
			int pageInContent = content.PageOfParagraph(para);
			if (pageInContent >= 0)
			{
				return calculPage + pageInContent;
			}
			calculPage += content.Pages.size();
		}
		return 0;
	}

	public void resetSearchedTexts()
	{
		for(int i = 0;i <htmlcontents.size(); ++i)
		{
			HtmlContent content = htmlcontents.get(i);
			for (int j = 0; j < content.paragraphs.size(); ++j)
			{
				Paragraph para = content.paragraphs.get(j);
				para.resetSearchedTexts();
			}	
		}
	}
	
/*SearchControllerForPopover 클래스를 안드로이드용으로 바꿔줘야함 
	void search(String str, SearchControllerForPopover searchController)
	{
		for(int i = 0;i <htmlcontents.size(); ++i)
		{
			HtmlContent content = htmlcontents.get(i);
			content.search(str, searchController);
		}
	}
*/
	
	public String GetTextInSelectionFrom(SelectedParagraphInfo selStart, SelectedParagraphInfo selFinish)
	{
		StringBuffer ret = new StringBuffer();
		
		SelectedParagraphInfo start = selStart;
		SelectedParagraphInfo end = selFinish;
		if (selStart.compare(selFinish) == Global.NSOrderedDescending)
		{
			start = selFinish;
			end = selStart;
		}
			
		for(int i = start.content.contentIdx; i <= end.content.contentIdx; ++i)
		{
			HtmlContent content = htmlcontents.get(i);
			int startparaIdx = 0;
			int endparaIdx = content.paragraphs.size() - 1;
			if (i == start.content.contentIdx) 
				startparaIdx = start.paragraphIdx;
			if (i == end.content.contentIdx)
				endparaIdx = end.paragraphIdx;
			
			for (int j = startparaIdx; j <= endparaIdx; ++j)
			{
				Paragraph para = content.paragraphs.get(j);
				if (para.objType == 0) //text
				{
					int startTextIdx = 0;
					int endTextIdx = para.texts.size() - 1;
					if (i == start.content.contentIdx && j == startparaIdx)
						startTextIdx = start.textIdx;
					if (i == end.content.contentIdx && j == endparaIdx)
						endTextIdx = end.textIdx;
			
					for (int l = startTextIdx; l <= endTextIdx; ++l)
					{
						TextContent text = para.texts.get(l);
						int startcharIdx = 0;
						int endcharIdx = text.content.length() - 1;
						if (i == start.content.contentIdx && j == startparaIdx && l == startTextIdx)
							startcharIdx = start.charIdx;
						if (i == end.content.contentIdx && j == endparaIdx && l == endTextIdx)
							endcharIdx = end.charIdx;
					
						int charmin = (text.content.length() - 1 < endcharIdx)?text.content.length()-1:endcharIdx;
						if (startcharIdx == -1) startcharIdx = 0;
						
						ret.append(text.content.substring(startcharIdx, charmin+1));
						/*for (int k = ; k <= ; ++k)
						{
							ret.append(text.content.charAt(k));
						}*/	
					}
				}
			}
		}	
		return ret.toString();
	}

	public void SaveMemos()
	{	  
		//String filename = Environment.getExternalStorageDirectory() + "/test.inf2";
		String filename = bookpath + ".inf2";
		File file = new File(filename);
		OutputStream out = null;
	
		try {
			out = new BufferedOutputStream(new FileOutputStream(file));
			if (out != null)
			{
				int cnt = Memos.size();
				out.write(Util.intToByteArray(cnt));
				for(int i = 0; i < Memos.size(); i++)
				{
					MemoInfo memo = Memos.get(i);
					memo.Save(out);
				}
				cnt = BookMarks.size();
				out.write(Util.intToByteArray(cnt));
				for (int i = 0; i < cnt; ++i)	
				{
					BookMarkPos bmp = BookMarks.get(i);
					bmp.Save(out);
				}
			}
			out.close();
			
			//Log.i("MEMO", "저장 성공");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public void LoadMemos()
	{
		//String filename = Environment.getExternalStorageDirectory() + "/test.inf2";
		String filename = bookpath + ".inf2";
		File file = new File(filename);
		InputStream in = null;
		
		try {
			in = new BufferedInputStream(new FileInputStream(file));
			if (in != null)
			{
				byte[] buffer = new byte[4];
				
				Memos.clear();
				
				in.read(buffer, 0, 4);
				int cnt = Util.byteArrayToInt(buffer);
				
				for(int i = 0; i < cnt; ++i)
				{
					MemoInfo memo = new MemoInfo();
					memo.Load(in);
					Memos.add(memo);
				}
				
				BookMarks.clear();
				
				in.read(buffer, 0, 4);
				cnt = Util.byteArrayToInt(buffer);
				
				for(int i = 0; i < cnt; ++i)
				{
					BookMarkPos bmp = new BookMarkPos();
					bmp.Load(in);
					BookMarks.add(bmp);
				}			
				in.close();
				in = null;
			}
			
			//Log.i("MEMO", "불러오기 성공");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void SaveBookReadingInfo()
	{
/*		//글꼴, 행간격, 글씨크기 저장
		//현재 페이지 저장
		//페이징 정보 저장 
		NSString *temp = [NSString stringWithFormat:@"%@.inf1", bookpath];
		char szFilename[1024];	
		memset(szFilename, 0, 1024);
		memcpy(szFilename, (char*)[[temp dataUsingEncoding:NSUTF8StringEncoding] bytes], [temp length]);

		FILE* st = fopen(szFilename, "wb");
		if(st)
		{
			fwrite(&SpaceBetweenLines, sizeof(NSInteger), 1, st);
			fwrite(&g_bSameFontSize, sizeof(BOOL), 1, st);
			fwrite(&g_FontSize, sizeof(NSInteger), 1, st);
			fwrite(&g_FontRate, sizeof(NSInteger), 1, st);
			char face[100];
			memset(face, 0, 100);
			memcpy(face, (char*)[[g_FontFace dataUsingEncoding:NSUTF8StringEncoding] bytes], [g_FontFace length]);
			
			fwrite(face, sizeof(char), 100, st);
			fwrite(&curPage, sizeof(float), 1, st);
			TextInfo* info = [[TextInfo alloc] init];
			[self GetTextInfoOfPage:curPage ret:info];
			
			int contentIdx = info.contentIdx;
			int paragraphIdx = info.paragraphIdx;
			int textIdx = info.textIdx;
			int charIdx = info.charIdx;		
			[info release];
			
			fwrite(&contentIdx, sizeof(int), 1, st);
			fwrite(&paragraphIdx, sizeof(int), 1, st);
			fwrite(&textIdx, sizeof(int), 1, st);
			fwrite(&charIdx, sizeof(int), 1, st);

			fclose(st);
		}*/
	}

	void LoadBookReadingInfo()
	{
		/*NSString *temp = [NSString stringWithFormat:@"%@.inf1\0", bookpath];
		//remove(NSStringToChar(temp));
		//temp = [NSString stringWithFormat:@"%@.inf1", bookpath];
		BOOL success;
		NSFileManager *fileManager = [NSFileManager defaultManager];
		success = [fileManager fileExistsAtPath:temp];	
		if (success)
		{
			char szFilename[1024];					 
			memset(szFilename, 0, 1024);
			memcpy(szFilename, (char*)[[temp dataUsingEncoding:NSUTF8StringEncoding] bytes], [temp length]);
			
			FILE* st = fopen(szFilename, "rb");
			if(st)
			{
				fread(&SpaceBetweenLines, sizeof(NSInteger), 1, st);
				fread(&g_bSameFontSize, sizeof(BOOL), 1, st);
				fread(&g_FontSize, sizeof(NSInteger), 1, st);
				fread(&g_FontRate, sizeof(NSInteger), 1, st);
				char face[100];			
				fread(face, sizeof(char), 100, st);
				fread(&curPage, sizeof(float), 1, st);
			
				if (loadedTextInfo)
					[loadedTextInfo release];
				loadedTextInfo = [[TextInfo alloc] init];
				int contentIdx;
				int paragraphIdx;
				int textIdx;
				int charIdx;
				
				fread(&contentIdx, sizeof(int), 1, st);
				fread(&paragraphIdx, sizeof(int), 1, st);
				fread(&textIdx, sizeof(int), 1, st);
				fread(&charIdx, sizeof(int), 1, st);
				
				loadedTextInfo.contentIdx = contentIdx;
				loadedTextInfo.paragraphIdx = paragraphIdx;
				loadedTextInfo.textIdx = textIdx;
				loadedTextInfo.charIdx = charIdx;		
				
				
				fclose(st);
				if (strlen(face) > 0)
				{
					if (g_FontFace)
					{
						[g_FontFace release];
					}
					g_FontFace = [[NSString alloc] initWithFormat:@"%s",face];
				}
			}		
		}*/
	}
	

}
