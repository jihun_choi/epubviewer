package ybm.EpubViewer.EPubBook;

 
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.util.ByteArrayBuffer;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import ybm.EpubViewer.Logger;


public class EpubTocFileParser extends DefaultHandler{
	String currentText = null;
	Boolean isInTextTag;
	
	EpubToc toc;
	EpubNavPoint curNavPoint;
	int showLevel;//NavPoint�� ����.. ������ ������ ������....
	
	public EpubTocFileParser(byte[] buf, EpubToc _toc)
	{	
		currentText = null;
		isInTextTag = false;
		showLevel = 0;
		toc = new EpubToc();
		String szXml = "";
		try {
			szXml = new String(buf, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			XMLReader reader = parser.getXMLReader();

			reader.setContentHandler(this);
			InputStream istream = new ByteArrayInputStream(szXml.getBytes());
			reader.parse(new InputSource(istream));
			
		}
		catch(Exception e)
		{
			Logger.d("error"," EpubTocFileParser error");
		}
		
		_toc.docTitle = toc.docTitle;
		_toc.docAuthor = toc.docAuthor;
		_toc.navPoints = toc.navPoints;
	}
	
	@Override
    public void characters(char[] ch, int start, int length) throws SAXException 
    {
        super.characters(ch, start, length);
        String str = new String(ch, start, length);
        //Logger.d("character", str);
    	if (isInTextTag)
    	{
    		if (currentText == null)
    			currentText = "";
  
    		currentText += str;
    	} 
    }

    @Override
    public void endElement(String uri, String localName, String name) throws SAXException 
    {
        super.endElement(uri, localName, name);
        //Logger.d("end", localName);
        if (localName.equalsIgnoreCase("text"))
    	{
    		isInTextTag = false;
    	}

    	if (localName.equalsIgnoreCase("docTitle"))
    	{
    		if (currentText != null)
    		{
    			toc.docTitle = currentText;
    			currentText = "";
    		}
    	}
    	
    	if (localName.equalsIgnoreCase("docAuthor"))
    	{
    		if (currentText != null)
    		{
    			toc.docAuthor = currentText;
    			currentText = "";
    		}
    	}	
    	
    	if (localName.equalsIgnoreCase("navPoint"))
    	{
    		if (curNavPoint != null)
    		{
    			toc.navPoints.add(curNavPoint);		
    			curNavPoint = null;
    		}
    		showLevel--;
    	}
    	
    	if (localName.equalsIgnoreCase("navLabel"))
    	{
    		if (currentText != null)
    		{
    			curNavPoint.labelType = 0;
    			curNavPoint.label = currentText;
    			currentText = "";
    		}
    	}
       
    }


    @Override
    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        //Logger.d("start", localName);
        if (localName.equalsIgnoreCase("text"))
    	{
    		isInTextTag = true;
    	}
    	
    	if (localName.equalsIgnoreCase("navPoint"))
    	{
    		if (curNavPoint != null)
    		{
    			toc.navPoints.add(curNavPoint);		
    		}

    		curNavPoint = new EpubNavPoint();
    		curNavPoint.navId = attributes.getValue("id");
    		curNavPoint.navClass = attributes.getValue("class"); 
    		curNavPoint.playOrder = (attributes.getValue("playOrder") != null)? Integer.parseInt(attributes.getValue("playOrder")) : 0; 
    		curNavPoint.showLevel = showLevel;
    		showLevel ++;
    	}
    	
    	if (localName.equalsIgnoreCase("content"))
    	{
    		curNavPoint.SetContent(attributes.getValue("src"));
    	}
    }
	
}
