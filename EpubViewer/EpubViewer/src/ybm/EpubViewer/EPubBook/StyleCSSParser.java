package ybm.EpubViewer.EPubBook;

import java.util.HashMap;
import java.util.Map;
import ybm.EpubViewer.Global;

public class StyleCSSParser {
	int mode;
	String firstClass;
	String curClass;

	void ParseData(byte[] container,  HashMap<String, HashMap<String, String>> content)
	{
		mode = 2;
		NSRange sentenceRange = new NSRange(0, 0);
		byte[] bytes = container.toString().replace("\r", "").replace("\n", "").replace(" ", "").replace("\t", "").getBytes();
		for (int i = 0; i< bytes.length; ++i)
		{
			if (bytes[i] == '@')
			{
				mode = 1;
				sentenceRange.location = i + 1;
			}
			else if (bytes[i] == '{')
			{
				sentenceRange.length = i - sentenceRange.location ;
				String str = new String(bytes, sentenceRange.location, sentenceRange.length); 
		
				//ncoding:NSUTF8StringEncoding];
				
				if (mode == 1) //"@로 시작"
				{
					// font-에 대한 내용은 다 넘긴다
				}
				if (mode ==2)
				{
					if (str.indexOf(".") != -1)
					{
						if (str.startsWith(".") == true)
							curClass = new StringBuilder(30).append(firstClass).append(str).toString();
						else
							curClass = str;
					}
					else
						firstClass = str;
					
					sentenceRange.location = i + 1;
					
					mode = 3; // In Braket
				}
			}
			else if (bytes[i] == '}')
			{
				sentenceRange.length = i - sentenceRange.location ;
			
				String str = new String(bytes, sentenceRange.location, sentenceRange.length); 
				//NSASCIIStringEncoding
				if (mode == 3)
				{
					String key;
					if (curClass != null)
					{
						key = curClass;
					}
					else 
					{
						key = firstClass;
					}
					
					//NSLog(@"curclass: [%@]",key);
					HashMap<String, String> value = ContentParse(str);
					if (value != null)
					{
						content.put(key, value);
					}
					
					curClass = null;
					firstClass = null;
				}
				sentenceRange.location = i + 1;
				mode = 2;
			}			
		}	
		//NSLog(@"content : %d",[content count]);
	} 
	 
	void Parse(String filename, HashMap<String, HashMap<String, String>> content)
	{
		byte[] container = null;
		if ((container = Global.g_CurEpubBook.EpubZip.get(filename)) != null )
		{
			ParseData(container, content);
		}
	}

	HashMap<String, String> ContentParse(String content)
	{
		if (content.length() > 0)
		{
			HashMap<String, String> contents = new HashMap<String, String>(10);
			int p1 = 0, p2 = 0, p3 = 0, p4 = 0;

			int start = 1;
			while (start > 0 && p1 < content.length()) {
				start = content.indexOf(':', start) + 1;
				if (start == 0)
					break;
				p2 = start - 1;
				p3 = start;
				start = content.indexOf(';', start) + 1;
				p4 = start - 1;
				if (start == 0)
					break;
				String key = content.substring(p1, p2);
				String val = content.substring(p3, p4);

				contents.put(key, val);

				p1 = start;
			}
			return contents;
		}
		return null;
	}
}
