package ybm.EpubViewer;

import java.util.ArrayList;

import org.apache.http.util.ByteArrayBuffer;

public class BooksForLibrary {

	java.lang.String coversUrl;
	ArrayList<BookForLibrary> books;
	public BooksForLibrary()
	{
		books = new ArrayList<BookForLibrary>();
	}
	public void AddBook(BookForLibrary book)
	{
		books.add(book);
	}
	
	public BookForLibrary findBookById(String bookId)
	{
		int len = books.size();
		int i;
		for(i=0;i<=len-1;i++){
			if(Integer.parseInt(books.get(i).bookId) == Integer.parseInt(bookId))
				return books.get(i);
		}
		
		return null;
	}

	public ByteArrayBuffer GetRawData()
	{
		char NullByte = '\1';
		
		ByteArrayBuffer  baf = new ByteArrayBuffer(100);
		baf.append(coversUrl.getBytes(), 0, coversUrl.length());
		baf.append(NullByte);

		int size = books.size();
		for(int i = 0; i < size; ++i )
		{
			BookForLibrary book = books.get(i);
			ByteArrayBuffer bookData = book.GetRawData();
			baf.append(bookData.buffer(), 0, bookData.length());
			baf.append(NullByte);
		}
		return baf;
	}
	
	
	public void CreateWithData(ByteArrayBuffer data)
	{
		books.clear();
		byte[] bytes = data.buffer();
		int order = 0;
		int lastnull = 0;
		for(int i = 0; i < data.length(); ++i)
		{
			if (bytes[i] == '\1')
			{
				ByteArrayBuffer aData = new ByteArrayBuffer(i - lastnull + 1);
				aData.append(data.buffer(), lastnull, i - lastnull);
				
				if (order == 0)
				{
					coversUrl = new String(aData.buffer());
				}else
				{
					BookForLibrary book = new BookForLibrary();
					book.CreateWithData(aData);
					books.add(book);
				}
				lastnull = i + 1;
				order++;
			}
		}	
	}
		
}




/*
 * 
 * 인터넷에서 파일 저장 
InputStream is = mConn.getInputStream();
BufferedInputStream bis = new BufferedInputStream(is);
FileOutputStream fos = new FileOutputStream(file);
byte [] buffer = new byte[8*1024];
int length = 0;

while ((length = bis.read(buffer)) >= 0) {
    fos.write(buffer, 0, length);
}
fos.flush();
fos.close();*/