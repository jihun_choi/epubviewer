package ybm.EpubViewer.EPubBook;

import java.util.ArrayList;

import ybm.EpubViewer.Global;
import ybm.EpubViewer.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.RelativeLayout;

public class Paragraph {
	HtmlContent parent;//어떤 content에 속해 있나? 
	public int m_nIndex; //본 문단은 몇번째 문단인가?
	
	String paraId;// P Tag의 id element의 값
	
	
	public float height; //본 문단의 총 높이는 얼마인가?
	int lineCount;// 본 문단은 몇 라인인가?
	ArrayList<LineData> lineDatas;// 각 라인에 대한 정보  

	
	//문단은 밑의 셋중에 한 값을 갖는다
	public boolean	NextPage; //true면 다음 페이지로 바로 넘어간다
	public ArrayList<TextContent> texts; //출력할 텍스트들 오른쪽의 형식을 인자로 갖는다. TextContent text; 
	
	public int objType;//0: text, 1: image 2: audio 3: video
	public String objFilename;
	//String imageName; //출력할 이미지
	
	int alignment; //태그에서 설정한 얼라인먼트
	
	ArrayList<SearchedText> searchTexts;// 검색된 글자의 위치 
	
	int indentSpaceCount; //들여쓰기 칸수
	
	String linkHtml; //Link할 단란의 번호를 찾아둔다, NULL 이면 없는것
	
	String szBgColor;
	public float topMargin;
	public float bottomMargin;
	
	ArrayList<CharacterPositionInfo> CharacterPositionInfos;//현재 페이지에서 각 글자들의 화면상의 위치를 저장한다

//	id <PlayerDelegate> playerDelegate;
//	EPubViewer playerDelegate = null;
	public Context playerDelegate = null;
	
	Activity m_curActivity = null; // 마지막으로 생성했던 mp3 플레이 버턴의 액티비티
	View m_curButton = null; // 현재 생성된 mp3 플레이 버턴의 뷰
	
	public boolean isText() {
		return (objType == 0 && lineDatas != null && lineDatas.size() > 0);
	}

	public boolean isImage()
	{
		return (objType == 1 && objFilename != null);
	}
	
	public Paragraph()
	{
			
		m_nIndex = 0; //본 문단은 몇번째 문단인가?
		
		height = 0; //본 문단의 총 높이는 얼마인가?
		lineCount = 0;// 본 문단은 몇 라인인가?
		lineDatas = null;// 각 라인에 대한 정보  
		
		//문단은 밑의 셋중에 한 값을 갖는다
		NextPage = false; //true면 다음 페이지로 바로 넘어간다
		texts = new ArrayList<TextContent> ();
		
		objType = 0;
		objFilename = null; //출력할 이미지
		
		alignment = Global.UITextAlignmentLeft; //태그에서 설정한 얼라인먼트
		linkHtml = null;
		topMargin = 0;
		bottomMargin = 0;
		
		CharacterPositionInfos = new ArrayList<CharacterPositionInfo> ();
		
		paraId = null;

		searchTexts = new ArrayList<SearchedText>();
	}
	
	public void AddText(TextContent text, StringBuilder content)
	{
		TextContent text1 = new TextContent();
		text1.content = content.toString();
		text1.fontIdx = text.fontIdx;
		text1.szTextcolor = text.szTextcolor;

		texts.add(text1);
	}

	public LineData GetLineData(int idx)
	{
		return lineDatas.get(idx);
	}

	public int GetLineNoFrom(int Startline, float remainHeight, float []pHeight)
	{
		float curHeight = 0;
		
		pHeight[0] = height;
		
		if (objFilename != null)
		{
			if (height > remainHeight)
			{
				pHeight[0] = (float) 0;
				return 0;
			}
			return 1;
		}
		
		if (height <= remainHeight)
		{
			return lineDatas.size();
		}
		
		for (int i = Startline; i < lineDatas.size(); ++i)
		{
			LineData pLineData  = lineDatas.get(i);
			
			if (curHeight + pLineData.height > remainHeight) // 이번 줄이 안들어 간다면
			{
				pHeight[0] = curHeight;
				return i;
			}
			
			else if (curHeight + pLineData.height + Global.SpaceBetweenLines > remainHeight) // 이번 줄만 들어가면
			{
				pHeight[0] = curHeight + pLineData.height + Global.SpaceBetweenLines;
				return i + 1;
			}
			
			else curHeight +=  pLineData.height + Global.SpaceBetweenLines;
			
		}	
		pHeight[0] = curHeight;
		return lineDatas.size();		
	}

	public int GetLineFromTextIdx(int textIdx, int charIdx)
	{
		for (int i = 0; i < lineDatas.size() - 1; ++i)
		{
			LineData pLineData = lineDatas.get(i);
			if(pLineData == null)return 0;
			if (  (pLineData.startText == textIdx && pLineData.startChar <= charIdx) || (pLineData.startText < textIdx) )
			{
				LineData pLineData2  = lineDatas.get(i + 1);
				if (  (pLineData2.startText == textIdx && pLineData2.startChar > charIdx) || (pLineData2.startText > textIdx) )
					return i;
			}
		}
		return lineDatas.size() - 1;
	}
	
	public boolean IsEnglish(char buf)
	{
		return (buf >='a' && buf <= 'z') || (buf >= 'A'&& buf <='Z');
	}

	public float CalculateLines(float width)
	{
		lineCount = 0;
		height = 0; 
		float xPos = 0;
			
		if (objType == 1)
		{
			Bitmap image = Global.g_CurEpubBook.GetImageFromFile(objFilename);
			
			height = image.getHeight();
			
			if (image.getWidth() > width)
			{
				height = height * (width / image.getWidth());
			}
			
			if (height > Global.g_PageFrame.height() - (Global.g_TopMargin + Global.g_BottomMargin) ) 
				height = Global.g_PageFrame.height() - (Global.g_TopMargin + Global.g_BottomMargin);
			
			return height;
		}
		else if (objType == 2)
		{
			//NSLog(@"Audio 일때 Height 계산 처리");
			
			height = 20;
			return height;
		}
		else if (objType == 3)
		{
			//NSLog(@"Video 일때 Height 계산 처리");
			
		
			
			height = 35;
			return height;			
		}

		if (lineDatas != null)
			lineDatas.clear();
		else
			lineDatas = new ArrayList<LineData>();
		
		LineData pLineData = new LineData();
		pLineData.startText = 0;
		pLineData.startChar = 0;
		pLineData.finishText = 0;
		pLineData.finishChar = 0;
		pLineData.height = 0;
		
		xPos += indentSpaceCount * Global.g_ParagraphIndent; //단락 들여쓰기 처리
		
		int tempStartChar = 0;
		int tempStartText = 0;
		int tempFinishChar = 0;
		int tempFinishText = 0;
		float tempXPos = 0;
		
		ArrayList<TextContent> _texts = texts;
		int _size = _texts.size();
		
		for (int i = 0; i < _size; ++i)
		{
			TextContent text = _texts.get(i);
			String str = text.content;
			int str_len = str.length();
			//Paint paint = Global.g_FontMgr.GetPaint(text.fontIdx);

			if (str_len == 0 && texts.size() == 1) //아무것도 없는 단락일때 <p></p>
			{
				pLineData.height = Global.g_FontMgr.GetCharSize(' ', text.fontIdx).height;
				break;
			}
			
			Rect lastBounds = new Rect(0, 0, 0, 0);

			for (int j = 0; j < str_len; ++j)
			{
				char charJ = str.charAt(j);
			
				Size size = Global.g_FontMgr.GetCharSize(charJ, text.fontIdx);
		
				if (j + 1 < str_len)
				{
					if (!IsEnglish(charJ) && IsEnglish(str.charAt(j + 1)))
					{
						tempStartText = i;
						tempStartChar = j + 1;
						tempFinishText = i;
						tempFinishChar = j;
						tempXPos = xPos;
					}
				}
				else if (j + 1 == str_len)
				{
					tempFinishText = i;
					tempFinishChar = j;
					
					tempStartText = i + 1;
					tempStartChar = 0;
					
					tempXPos = xPos;
				}
			
				if (xPos + size.width > width)
				{
					boolean check = false;
					if (j > 0 && IsEnglish(str.charAt(j - 1)) && IsEnglish(charJ))
					{
						pLineData.finishText = tempFinishText;
						pLineData.finishChar = tempFinishChar;
						check = true;
					}
						
					height += pLineData.height + Global.SpaceBetweenLines;
					lineDatas.add(pLineData);
					
					pLineData = new LineData();
					
					if (check)
					{
						pLineData.startText = tempStartText;
						pLineData.startChar = tempStartChar;
						xPos = xPos - tempXPos + size.width;
					}
					else
					{
						pLineData.startText = i;
						pLineData.startChar = j;
						xPos = size.width;
					}
					pLineData.height = size.height;
					pLineData.finishText = i;
					pLineData.finishChar = j;
				}
				else 
				{
					pLineData.finishText = i;
					pLineData.finishChar = j;
					
					xPos += size.width;
					if (pLineData.height < size.height) 
						pLineData.height = size.height;

				}
			}
		}

		lineDatas.add(pLineData);
		lineCount = lineDatas.size();
		
		height += pLineData.height + Global.SpaceBetweenLines + (bottomMargin + topMargin) * 12;
		
		return height;
	}

	public boolean GetTextAtPosition(Point pos, SelectedParagraphInfo ret)
	{
		if(ret == null)
			return false;
		//Enumeration을 쓰는게 빠를까 아님 이게 빠를까?
		int size = CharacterPositionInfos.size();
		for(int i = 0 ; i < size; ++i)
		{
			CharacterPositionInfo info = CharacterPositionInfos.get(i);
			Rect rect = info.position;
			rect.bottom += Global.SpaceBetweenLines;
			if (Global.isInRect(rect, pos))
			{
				ret.textIdx = info.textIdx;
				ret.charIdx = info.charIdx;
				return true;
			}
		}
		return false;
	}

	public Point GetPositionAtText(SelectedParagraphInfo text)
	{
		if (text == null)
			return new Point(0,0);
		
		int size = CharacterPositionInfos.size();
		for(int i = 0 ; i < size; ++i)
		{
			CharacterPositionInfo info = CharacterPositionInfos.get(i);
			if (text.textIdx == info.textIdx && text.charIdx == info.charIdx)
				return new Point(info.position.left, info.position.top);
		}
		return new Point(0,0);
	}
	
	public int GetLastCharIdxOfTextIdx(int idx) 
	{
		if (texts != null && texts.size() > idx)
		{
			TextContent text = texts.get(idx);
			if (text.content == null) return -1;
			return text.content.length() - 1;
		}
		return -1;
	}

	public int GetTextsCount()
	{
		if (texts == null) return -1;
		return texts.size();
	}
	
	public boolean makeAudioButton() {
		if (objType != 2)
			return false;
		
		if (Global.g_curActivity != null && m_curActivity != Global.g_curActivity) {
			//Log.i("AUDIO", "MAKE MP3PLAY BUTTON");
			
			m_curActivity = Global.g_curActivity;
			
			final LayoutInflater inflater = 
				(LayoutInflater)Global.g_curActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			m_curButton = inflater.inflate(R.layout.mp3play, null);
		}
		
		Global.g_curMp3Play = m_curButton;
		Global.g_curMp3PlayURL = this.objFilename;
		Global.g_curMp3PlayParagraph = this;
		
		return true;
	}
	
	public float drawRect(Canvas canvas, Rect rect, int startline, int endline, boolean bJustify)
	{
		float totalHeight = 0 ;
		
		CharacterPositionInfos.clear();
		
		rect.top += topMargin * 12;
		
		
		//배경색 칠하기 
		Rect paraRect = new Rect(rect.left, rect.top, rect.right, (int) (rect.bottom - (topMargin + bottomMargin) * 12));
		
		
		//배경색칠하기 
		if (szBgColor != null)
		{
			int bgColor = Global.GetColorFromString(szBgColor, Color.WHITE);
			Paint paint = new Paint();
			paint.setColor(bgColor);
			paint.setStyle(Paint.Style.FILL);
			canvas.drawRect(paraRect, paint);
		}
		
		if (Global.g_BgColor != null)
		{
			Paint paint = new Paint();
			paint.setColor(Global.g_BgColor);
			paint.setStyle(Paint.Style.FILL);
	
			canvas.drawRect(paraRect, paint);
		}
		
		
		if (objType == 1)
		{
			Bitmap image = Global.g_CurEpubBook.GetImageFromFile(objFilename);
			
			rect.top -= 25; // 그림이 오묘하게 전반적으로다가 아래로 내려와서 찍히므로 오묘하게 좀 올림 
			
			rect.bottom = (int) (rect.top + height);
			
			float width = image.getWidth() * height / image.getHeight();
			if (width < rect.width())
			{
				rect.left += (rect.width() - width)/2;
				rect.right = (int) (rect.left + width) ;
			}
			canvas.drawBitmap(image, new Rect(0, 0, image.getWidth(), image.getHeight()), rect, null);
		
			// contentsviewer image top left height width
			//viewer.drawImage(image, rect.top, rect.left, (int)height, (int)width);
			
			return height + (topMargin + bottomMargin) * 12;
		}
		else if (objType == 2)
		{
			Log.i("AUDIO", "DRAW MP3PLAY BUTTON");
			
			//NSLog(@"Audio 일때 그리기 처리");
			rect.right = rect.left + 200;
			rect.bottom = (int)(rect.top + height);
			//playerDelegate.ShowPlayerButton(rect, this, objFilename);
			
			this.makeAudioButton();
			
			Global.g_curMp3PlayPosition = rect;
			
			Global.g_curMp3Play = m_curButton;
			
			return height;
		}
	
		
		if (endline == -1) endline = lineCount;
		
		NSRange xRange = new NSRange(rect.left, rect.width());
		float yPos = rect.top;
		
		for (int i = startline; i < endline; ++i)
		{
			LineData pLineData  = lineDatas.get(i);
			
			if (i == 0)
			{
				int indent = indentSpaceCount * Global.g_ParagraphIndent;
				xRange = new NSRange(rect.left + indent , rect.width() - indent);
			}
			else
				xRange = new NSRange(rect.left, rect.width());
			
			drawOneLine(canvas, pLineData, xRange, yPos, bJustify);
			
			totalHeight +=  pLineData.height + Global.SpaceBetweenLines;
			yPos += pLineData.height + Global.SpaceBetweenLines;
			
			
		}
		return totalHeight + (topMargin + bottomMargin) * 12; 
	}



	public void drawOneLine(Canvas canvas, LineData lineData, NSRange xRange, float yPos, boolean bJustify)
	{
		
		int TextIdx = lineData.startText;
		int startChar = lineData.startChar;
		float xPos = xRange.location;
		
		boolean bLastCharIsSpace = false;
		boolean bFirstCharIsSpace = false;
		boolean FirstCharChecked = false;
		boolean bIsLastLine = (lineData == lineDatas.get(lineCount - 1));
		
		float spaceWidth = 0;	
		int nSpaceCount = 0;
		float characterWidth = 0;

		//줄 안의 공백을 계산한다.
		boolean finish = false;
		float totalWidth = 0;
		
		while( !finish  )
		{
			if (TextIdx >= texts.size())
				break;
			
			TextContent text = texts.get(TextIdx);
			
			for (int i = startChar; i < text.content.length(); ++i)
			{			
				if(TextIdx == lineData.finishText && i == lineData.finishChar)
					break;
				//String oneChar = text.content.substring(i, i);
				//format("%c", text.content.charAt(i)); 
				char oneChar = text.content.charAt(i);
				Size size = Global.g_FontMgr.GetCharSize(oneChar, text.fontIdx);
				
				if (TextIdx < lineData.finishText || (TextIdx == lineData.finishText && i <= lineData.finishChar) )
				{
					totalWidth += size.width;
					
					if (!FirstCharChecked)
					{
						FirstCharChecked = true;
						bFirstCharIsSpace = (oneChar == ' ');//oneChar.equals(" ");
						if (lineData.startText == 0 && lineData.startChar == 0)
							bFirstCharIsSpace = false;
					}
					if (oneChar == ' ')//(oneChar.equals(" "))
					{
						++nSpaceCount;
						bLastCharIsSpace = true;
					}
					else
					{
						characterWidth += size.width;
						bLastCharIsSpace = false;
					}
				}
				else
				{
					finish = true;
					break;
				}
			}
			
			if(TextIdx == lineData.finishText)
				break;
			
			startChar = 0;		
			++TextIdx;			
		}
		
		
		bJustify = (alignment == 30);
		
		if (bJustify)
		{
			if (bLastCharIsSpace)
				--nSpaceCount;
			
			if (bFirstCharIsSpace)
				--nSpaceCount;
			
			if (nSpaceCount > 0)
				spaceWidth = (xRange.length - characterWidth) / nSpaceCount;
		}
		else
		{
			if (alignment == Global.UITextAlignmentCenter)
				xPos += (xRange.length - totalWidth) / 2; // 가운데 정렬
			if (alignment == Global.UITextAlignmentRight)
				xPos += (xRange.length - totalWidth);// 오른쪽 정렬
		}
		

		TextIdx = lineData.startText;
		startChar = lineData.startChar;	
		FirstCharChecked = false;
		
		boolean drawingFinish = false;
	
		while(!drawingFinish)
		{
			if (TextIdx >= texts.size())
				break;
			
			TextContent text = texts.get(TextIdx);
			Paint font = Global.g_FontMgr.GetPaint(text.fontIdx);			
			
			//Log.i("debug", "text : " + text.content);
			
			for (int i = startChar; i < text.content.length(); ++i)
			{
				
				if(TextIdx >= lineData.finishText && i > lineData.finishChar)
					break;
				
				String oneChar = text.content.substring(i, i+1);
				
				// 양쪽 정렬시 문장 첫번째가 공백이면 그리지 않는다
				if (!FirstCharChecked && bJustify)
				{
					FirstCharChecked = true;
					if (bFirstCharIsSpace && oneChar.equals(" "))
						continue;
				}
				
				Point pos = new Point((int)xPos, (int)yPos);
				//Size size = new Size((int)font.measureText(oneChar), (int)(font.ascent() + font.descent()));
				Size size = Global.g_FontMgr.GetCharSize(oneChar.charAt(0), text.fontIdx);
			
				//글자 배경에 색을 입히는 부분 
				int textBgColor = 0;
				
				//드래그로 선택된 글자들은 회색으로 칠한다.
				if (Global.g_SelMin != null && Global.g_SelMax != null)
				{
					int con1 = Global.g_SelMin.compare(parent.contentIdx, m_nIndex, TextIdx, i);
					int con2 = Global.g_SelMax.compare(parent.contentIdx, m_nIndex, TextIdx, i);
					
					if ((con1 == Global.NSOrderedSame || con1 == Global.NSOrderedAscending) && (con2 == Global.NSOrderedSame || con2 == Global.NSOrderedDescending))
					{
						textBgColor = Color.GRAY;
					}
				} 
				if (textBgColor == 0)
				{	
					//Memo에 해당하는 글자인지 파악하고, 맞으면 표시를 해준다.
					MemoInfo memo = Global.g_CurEpubBook.IsInMemo(parent.contentIdx, m_nIndex, TextIdx, i);
					if (memo != null)
					{
						if (memo.type == 0)
							textBgColor = Color.argb(100, 128, 0, 0);
						else if (memo.type == 2)
							textBgColor = Color.YELLOW;
						else
							textBgColor = Color.GREEN;
					}
				}
				
				if (textBgColor != 0)
				{
					//페이지 오른쪽으로 색이 넘어가는 것 방지;
					float width = size.width;
					if( pos.x + size.width > xRange.location + xRange.length)
						width -= pos.x + size.width - (xRange.location + xRange.length);
					
					Paint paint = new Paint();
					paint.setColor(textBgColor);
					paint.setStyle(Paint.Style.FILL);
					
					//canvas.drawRect(pos.x, pos.y - 1, pos.x + width, pos.y - 1 + size.height, paint);
					int h = Global.g_FontMgr.GetCharOnlyHeight('A', text.fontIdx);
					//canvas.drawRect(pos.x, pos.y - 1, pos.x + width, pos.y - 1 - size.height, paint);
					canvas.drawRect(pos.x, pos.y - 1, pos.x + width, pos.y - 1 - h, paint);
				}

				//마지막 문장은 스패이스를 그대로 냅둔다.
				if (oneChar.equals(" ") && bJustify && !bIsLastLine)
				{
					xPos += spaceWidth;
				}
				else
				{
					//if (size.width + xPos - 2.1> xRange.length + xRange.location)//소수점 0.00001 의 오차-_-
					if (TextIdx > lineData.finishText || (TextIdx == lineData.finishText && i > lineData.finishChar) )
					{
						drawingFinish = true;
						break;
					}
					else
					{
						//검색 기능으로 찾은 글자는 색을 주황색으로 칠해준다
						if (isSeachedText(TextIdx, i))
						{
							Paint paint = new Paint();
							paint.setColor(Color.argb(1, 255, 165, 0));
							paint.setStyle(Paint.Style.FILL);
							
							//canvas.drawRect(pos.x, pos.y, pos.x + size.width, pos.y + size.height, paint);
							canvas.drawRect(pos.x, pos.y - size.height, pos.x + size.width, pos.y, paint);
						}
						
						int oldColor = font.getColor();
						if (text.szTextcolor != null)
						{
							font.setColor(Global.GetColorFromString(text.szTextcolor, Color.BLACK));
						}
					
						//글자의 rect위치를 저장해 둔다
						CharacterPositionInfo info = new CharacterPositionInfo();
						//info.position = new Rect(pos.x, pos.y ,pos.x + size.width, pos.y + size.height);
						info.position = new Rect(pos.x, pos.y - size.height, pos.x + size.width, pos.y);
						info.textIdx = TextIdx;
						info.charIdx = i;
						CharacterPositionInfos.add(info);
						
						canvas.drawText(oneChar, pos.x, pos.y, font);
						
						font.setColor(oldColor);
						
						float fontsize = font.getTextSize();
						
						xPos += size.width;
					}
				}
			}
			
			if(TextIdx >= lineData.finishText)
				break;
			
			startChar = 0;
			++TextIdx;
		}
	}

	public void resetSearchedTexts()
	{
		searchTexts = null;
	}

	public void AddSearchedText(SearchedText st)
	{	
		if (searchTexts == null)
		{
			searchTexts = new ArrayList<SearchedText>();
		}

		searchTexts.add(st);
	}

	public boolean isSeachedText(int textIdx, int charIdx)
	{
		if (searchTexts == null)
			return false;
		
		for(int i = 0; i < searchTexts.size(); ++i)
		{
			SearchedText st = searchTexts.get(i);
			if (st.TextIdx == textIdx && st.CharIdx == charIdx)
				return true;
		}
		return false;
	}
}
