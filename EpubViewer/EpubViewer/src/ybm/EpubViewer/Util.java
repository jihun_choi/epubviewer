package ybm.EpubViewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.http.util.ByteArrayBuffer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;

public class Util {
	static String absolutePath;
	static Boolean Download(String DownloadURL, String FileName)
	{
             //인터넷 파일 다운로드  
        try {
     
            InputStream inputStream = (new URL(DownloadURL)).openStream();
            
            File file = new File(FileName);
            OutputStream out = new FileOutputStream(file);
            writeFile(inputStream, out);
            out.close();
        } catch(Exception e){          
        	Logger.d("Download", "File Write Error"+FileName);
        	return false;
        }
       	return true;
	}
	
    static void writeFile(InputStream is, OutputStream os) throws IOException
    {
        int BUFFER_SIZE = 4096;
    	byte[] inputBuffer = new byte[BUFFER_SIZE];
         int c = 0;
         while((c = is.read(inputBuffer)) > 0)
             os.write(inputBuffer, 0, c);
         os.flush();
    }  
    
    
	static Boolean DownloadToBuffer(String DownloadURL, ByteArrayBuffer buffer)
	{
             //인터넷 파일 다운로드  
        try {
     
            InputStream inputStream = (new URL(DownloadURL)).openStream();
 
            int BUFFER_SIZE = 4096;
        	byte[] inputBuffer = new byte[BUFFER_SIZE];
            int c = 0;
            while((c = inputStream.read(inputBuffer)) > 0)
               buffer.append(inputBuffer, 0, c);
            Logger.d("gmm", buffer.toString());
        } catch(Exception e){
        	return false;
        }
       	return true;
	}
	
	static Boolean IsInRect(Rect rect, Point pt)
	{
		return (rect.left <= pt.x && rect.right >= pt.x && rect.top <= pt.y && rect.bottom >= pt.y);
	}
	
	static double distance(Point p1, Point p2)
	{
		return Math.sqrt( (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y ) * (p1.y - p2.y) );
	}
	
	public static Boolean GetUnzipData(String filename, String target, ByteArrayBuffer buf)
	{
		//buf = new ByteArrayBuffer(5000);
        Boolean ret = false;
   		
		try {
	   		FileInputStream fis;
			fis = new FileInputStream(filename);
	        ZipInputStream zis = new ZipInputStream(fis);
	        ZipEntry ze;
	        try {
				while ((ze = zis.getNextEntry()) != null) 
				{
					if (ze.getName().compareToIgnoreCase(target) == 0)
					{
 
				        int BUFFER = 4096;
				        byte data [ ] = new byte [ BUFFER ];

						int count;
				        while ((count = zis.read(data, 0, BUFFER)) > 0)
				            buf.append(data, 0, count);
				    	zis.closeEntry();
				        ret = true;
				    	break;
					}
					zis.closeEntry();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        zis.close();	
	        fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return ret;
	}
	public static Boolean GetUnzipData2(ZipInputStream zis, String target, ByteArrayBuffer buf)
	{
		//buf = new ByteArrayBuffer(5000);
        Boolean ret = false;
        try 
        {
    	   ZipEntry ze;
			while ((ze = zis.getNextEntry()) != null) 
			{
				if (ze.getName().compareToIgnoreCase(target) == 0)
				{ 
					 int BUFFER = 4096;
				     byte data [ ] = new byte [ BUFFER ];

					 int count;
				     while ((count = zis.read(data, 0, BUFFER)) > 0)
				         buf.append(data, 0, count);
				        
			    	zis.closeEntry();
			        ret = true;
			    	break;
				}
				zis.closeEntry();
			}
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ret;
	}
	
	public static String CheckFolderValidate(String folder)
	{
		folder.replace("\\", "/");
		String subs[] = folder.split("/");

		Vector<String> newsub = new Vector<String>();
		for(int i = 0; i < subs.length; ++i)
		{
			String sub = subs[i];
			if (sub.compareTo("..") == 0)
			{
				if (newsub.size() > 0)
				{
					newsub.removeElementAt(newsub.size() - 1);
					continue;
				}
			}
			else if (sub.compareTo(".") == 0)
			{
					continue;
			}
			newsub.add(sub);
		}
		
		String result = "";
		for (int i = 0; i < newsub.size(); i++)
		{
			String sub = newsub.get(i);
			if(result.length()>0)
				result += "/";
			result += sub;
		}

		return result;	
	}
	
	public static float Float2(String val)
	{
		int i = 0, j = 0;
		int ret1 = 0 ;
		float ret2 = 0;
		for(i = 0; i < val.length(); i++)
		{	
			char ch = val.charAt(i);
			if (ch >= '0' && ch <= '9')
				ret1 = ret1 * 10 + ch - '0';
			else if (ch == '.')
				break;
			else  
				return (float)ret1;
		}
		for(j = val.length() - 1; j > i; j--)
		{
			char ch = val.charAt(j);
			if (ch >= '0' && ch <= '9')
				ret2 = ret2 / 10 + ch - '0';
		}
		return (float)ret1 + ret2 / 10;
	}
	
	public static byte[] intToByteArray(final int integer) {		
		byte[] buffer = new byte[4];
		buffer[0] = (byte)(integer & 0xff);
		buffer[1] = (byte)((integer & 0xff00) >> 8);
		buffer[2] = (byte)((integer & 0xff0000) >> 16);
		buffer[3] = (byte)((integer & 0xff000000) >> 24);
		
		//Log.i("MEMO", "int->byte[]: " + integer + " -> " + 
		//		buffer[0] + "," + buffer[1] + "," + buffer[2] + "," + buffer[3]);
		
		return buffer;
	}
	
	public static int byteArrayToInt(final byte[] bytes) {
		int integer = 0;
		integer |= (bytes[0] & (int)0xff);
		integer |= (bytes[1] & (int)0xff) << 8;
		integer |= (bytes[2] & (int)0xff) << 16;
		integer |= (bytes[3] & (int)0xff) << 24;
		
		//Log.i("MEMO", "byte[]->int: " + 
		//		bytes[0] + "," + bytes[1] + "," + bytes[2] + "," + bytes[3] + " -> " + 
		//		integer);
		
		return integer;
	}
}
