package ybm.EpubViewer.EPubBook;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ybm.EpubViewer.Util;

public class TextInfo {
	public int contentIdx;
	public int paragraphIdx;
	public int textIdx;
	public int charIdx;
	
	void Save(OutputStream out)
	{
		try {
			out.write(Util.intToByteArray(contentIdx));
			out.write(Util.intToByteArray(paragraphIdx));
			out.write(Util.intToByteArray(textIdx));
			out.write(Util.intToByteArray(charIdx));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void Load(InputStream in)
	{
		try {
			byte[] buffer = new byte[4];
			
			in.read(buffer, 0, 4);
			contentIdx = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			paragraphIdx = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			textIdx = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			charIdx = Util.byteArrayToInt(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
}
