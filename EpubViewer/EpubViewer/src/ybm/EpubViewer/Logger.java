package ybm.EpubViewer;

import android.util.Log;
/**
 * API for sending log output.
 * @see OdmUtility.SHOW_LOGS if true, then logs are appeared,  
 */
public class Logger {
 
 private static boolean SHOW_LOGS = true;
 
    private Logger() {
    }
    public static void d(String tag, String msg) {
        if (SHOW_LOGS) {
            Log.d(tag, "KJ2 : "+msg);
        }
    }
    public static void e(String tag, String msg) {
        if (SHOW_LOGS) {
            Log.e(tag, "KJ2 : "+msg);
        }
    }
    public static void i(String tag, String msg) {
        if (SHOW_LOGS) {
            Log.i(tag, "KJ2 : "+msg);
        }
    }
    public static void w(String tag, String msg) {
        if (SHOW_LOGS) {
            Log.w(tag, "KJ2 : "+msg);
        }
    }
}

