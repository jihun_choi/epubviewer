package ybm.EpubViewer;

import java.io.File;

import ybm.EpubViewer.EPubBook.EpubBook;
import ybm.EpubViewer.EPubBook.FontManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;


public class EPubViewer extends Activity {
	/** Called when the activity is first created. */
	
	static ProgressDialog m_openProgressDialog = null;
	static Handler m_openProgressHandler = null;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        
        Log.i("debug", "EPubViewer onCreate");
        
	    WindowManager wm = getWindowManager();
	    if(wm == null) return;
	    
	    final int height = getWindowManager().getDefaultDisplay().getHeight();
	 	final int width = getWindowManager().getDefaultDisplay().getWidth();
	    
	    // 책 설정
	    Intent intent = getIntent(); 
        final String bookFile = intent.getStringExtra("bookFile");
        final String bookId = intent.getStringExtra("bookId");
        
        m_openProgressDialog = ProgressDialog.show(this, "여는 중", "");
        m_openProgressHandler = new Handler();
        
        try{
	        new Thread(new Runnable() {
	        	public void run() {
	        		bookOpen(bookFile, bookId);
	                
	        		
	        	    if(height > width){
	        	    	Global.g_bShowTwoPages = false;
	        	    	
	        	    	Intent i = new Intent(EPubViewer.this, OnePageViewer.class);
	        	    	i.putExtra("tabbar", 1);
	        	    	startActivityForResult(i, 1);
	        	    	//startActivity(i);
	        	    	//finish();
	        	    }
	        	    else {// if(wm.getDefaultDisplay().getOrientation() ==  Configuration.ORIENTATION_LANDSCAPE){
	        	    	Global.g_bShowTwoPages = true;
	        	    	
	        	    	Intent i = new Intent(EPubViewer.this, TwoPageViewer.class);
	        	    	i.putExtra("tabbar", 1);
	        	    	startActivityForResult(i, 2);
	        	    	//finish();	        	    	        	 
	        	    }
	            	
	        	    m_openProgressHandler.post(new Runnable() { 
						public void run() {
							if(m_openProgressDialog.isShowing()){
								m_openProgressDialog.hide();
								m_openProgressDialog.dismiss();
							}
						}
					});
	        	}
	        }).start();
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
	}
	
	public static void setOpenProgressMessage(final String msg) {
		if (m_openProgressDialog == null || m_openProgressHandler == null)
			return;
		
		m_openProgressHandler.post(new Runnable() { 
			public void run() {     	    						
				m_openProgressDialog.setMessage(msg);
			}
		});
	}
    
    public void bookOpen(String fileToOpen, String bookId)
    {
    	//if (Global.g_CurEpubBook == null)
    	{
    	 	Global.g_CurEpubBook = new EpubBook(this);
    	 	Global.g_CurEpubBook.bookId = bookId;
    	 	
    	 	int height = getWindowManager().getDefaultDisplay().getHeight();
    	 	int width = getWindowManager().getDefaultDisplay().getWidth();
    	 	
    	 	Global.g_ScreenFrame = new Rect(0, 0, width, height);
    		Global.g_PageFrame = new Rect(0, 0, width - 110, height - 200);
    		Global.g_bShowTwoPages = false;
    	 	
    	 	//if (getWindowManager().getDefaultDisplay().getOrientation() > Configuration.ORIENTATION_UNDEFINED) { // 가로의 경우
    		if(width > height){ // 가로인 경우
        		Global.g_PageFrame = new Rect(0, 0, width/2-100, height-100); // <- 너비는 실험 중인 값
        		Global.g_bShowTwoPages = true;
    	 	}
    		
    		Global.g_FontMgr = new FontManager();
    		
//    		// 캐시 파일 삭제 (테스트용)
//    		try {
//    			File f = new File(fileToOpen + ".cache");
//    			f.delete();
//    		} catch (Exception e) {
//    			e.printStackTrace();
//    		}
    		
        	Global.g_CurEpubBook.Open(fileToOpen);
        	
        	Global.g_CurEpubBook.LoadMemos();
        	
        	// 가로 세로 전환시 보던 곳 표시하는 것을 초기화
        	Global.g_OldCurPage = -1;
        	Global.g_OldTotalPage = Global.g_CurEpubBook.GetTotalPage();
        	
        	//Global.g_CurEpubBook.ProcessPaging();
       
    /*		[self RotateOrientation:self.interfaceOrientation];
    		[g_CurEpubBook ProcessPaging];
    		[self.view setNeedsDisplay];*/
    	}
  //  	[self HideLoadingBar];
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
    	super.onActivityResult(requestCode, resultCode, data);
    	
	 	final int height = getWindowManager().getDefaultDisplay().getHeight();
	 	final int width = getWindowManager().getDefaultDisplay().getWidth();
    	
    	//Log.i("debug", "tabbar " + tabbar);
    	
    	//if(data.getBooleanExtra("rotate", false) == false){
    	//	Log.i("debug", "rotate false");
    	//	finish();
    	//}
    	
    	if(resultCode == RESULT_OK){
    		if(requestCode == 1){
    			if(data.getBooleanExtra("rotate", false) == true){

    		        m_openProgressDialog = ProgressDialog.show(this, "회전하는 중", "");
    		        
    		        final int tabbar = data.getIntExtra("tabbar", 1);
    		        
    		        try{
    			        new Thread(new Runnable() {
    			        	public void run() {    			            	
    			        		
						    	Global.g_ScreenFrame = new Rect(0, 0, width, height);
						    	Global.g_PageFrame = new Rect(0, 0, width/2-100, height-100);
						    	
						    	Global.g_bShowTwoPages = true;
						    	Global.g_CurEpubBook.ProcessPaging();
						    	
								if (Global.g_OldCurPage != -1) {
									float ratio = (float)Global.g_OldCurPage / (float)Global.g_OldTotalPage;
									Global.g_CurEpubBook.curPage = (int)(ratio * (float)Global.g_CurEpubBook.GetTotalPage());
									
									if (Global.g_CurEpubBook.curPage < 0)
										Global.g_CurEpubBook.curPage = 0;
									else if (Global.g_CurEpubBook.curPage > Global.g_CurEpubBook.GetTotalPage()) {
										Global.g_CurEpubBook.curPage = Global.g_CurEpubBook.GetTotalPage() - 1;
									}
									
									// 짝수 페이지를 왼쪽으로 함
									if (Global.g_CurEpubBook.curPage % 2 > 0)
										Global.g_CurEpubBook.curPage++;
									
									Global.g_OldTotalPage = Global.g_CurEpubBook.GetTotalPage();
								}
						    	
						    	Intent i = new Intent(EPubViewer.this, TwoPageViewer.class);
						    	i.putExtra("tabbar", tabbar);
						    	
						    	startActivityForResult(i, 2);
						    	
				        	    m_openProgressHandler.post(new Runnable() { 
									public void run() {
										if(m_openProgressDialog.isShowing()){
											m_openProgressDialog.hide();
											m_openProgressDialog.dismiss();
										}
									}
								});
			    			}
    			        }).start();
    		        }
    		        catch(Exception e){
    		        	e.printStackTrace();
    		        	finish();
    		        }
    			}
    			else
    				finish();
    		}
    		else if(requestCode == 2){
    			if(data.getBooleanExtra("rotate", false) == true){

    		        m_openProgressDialog = ProgressDialog.show(this, "회전하는 중", "");
    		        
    		        final int tabbar = data.getIntExtra("tabbar", 1);
    		        
    		        try{
    			        new Thread(new Runnable() {
    			        	public void run() {
    				
						    	Global.g_ScreenFrame = new Rect(0, 0, width, height);
						    	Global.g_PageFrame = new Rect(0, 0, width - 110, height - 200);
						    	
						    	Global.g_bShowTwoPages = false;
						    	Global.g_CurEpubBook.ProcessPaging();
						    	
								if (Global.g_OldCurPage != -1) {
									float ratio = (float)Global.g_OldCurPage / (float)Global.g_OldTotalPage;
									Global.g_CurEpubBook.curPage = (int)(ratio * Global.g_CurEpubBook.GetTotalPage());
									
									if (Global.g_CurEpubBook.curPage < 0)
										Global.g_CurEpubBook.curPage = 0;
									else if (Global.g_CurEpubBook.curPage > Global.g_CurEpubBook.GetTotalPage())
										Global.g_CurEpubBook.curPage = Global.g_CurEpubBook.GetTotalPage() - 1;
									
									Global.g_OldTotalPage = Global.g_CurEpubBook.GetTotalPage();
								}
						    	
						    	Intent i = new Intent(EPubViewer.this, OnePageViewer.class);
						    	i.putExtra("tabbar", tabbar);
						    	startActivityForResult(i, 1);
						    	
				        	    m_openProgressHandler.post(new Runnable() { 
									public void run() {
										if(m_openProgressDialog.isShowing()){
											m_openProgressDialog.hide();
											m_openProgressDialog.dismiss();
										}
									}
								});
			    			}
    			        }).start();
    		        }
    		        catch(Exception e){
    		        	e.printStackTrace();
    		        	finish();
    		        }
    			}
    			else
    				finish();
    		}
    	}
    	else
    		finish();
    }
    /*
	public void onConfigurationChanged(Configuration newConfig) {
		Log.i("debug", "가로 세로 전환");
		
		super.onConfigurationChanged(newConfig);
		
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) { // 세로 전환시 발생
			int height = getWindowManager().getDefaultDisplay().getHeight();
    	 	int width = getWindowManager().getDefaultDisplay().getWidth();
    	 	
    	 	Log.i("debug", "height : " + height + " width : " + width);
    	 	
    		Global.g_ScreenFrame = new Rect(0, 0, width, height);
    		Global.g_PageFrame = new Rect(0, 0, width-100, height-100);
    		
    		Global.g_CurEpubBook.ProcessPaging();
    		
			Intent i = new Intent(EPubViewer.this, OnePageViewer.class);
	    	startActivity(i);
	    	finish();
	    	
			Global.g_bShowTwoPages = false;  
		} else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) { // 가로 전환시 발생
			int height = getWindowManager().getDefaultDisplay().getHeight();
    	 	int width = getWindowManager().getDefaultDisplay().getWidth();
    	 	
    	 	Log.i("debug", "height : " + height + " width : " + width);
    	 	
    		Global.g_ScreenFrame = new Rect(0, 0, width, height);
    		Global.g_PageFrame = new Rect(0, 0, width / 2 - 100, height - 100); // <- 너비는 실험 중인 값
    		
    		Global.g_CurEpubBook.ProcessPaging();
    		
			Intent i = new Intent(EPubViewer.this, TwoPageViewer.class);	    	
	    	startActivity(i);
	    	finish();
	    	
			Global.g_bShowTwoPages = true;
		}
	}*/
}
