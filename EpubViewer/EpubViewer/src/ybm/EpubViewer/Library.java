package ybm.EpubViewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class Library extends Activity {

	
	//LibraryView libraryView;
	
	public ProgressDialog proDialog1;
	public Handler handler = new Handler();
	
	int []m_bookid;
	int []m_shadowid;
	int []m_overlays;
	int []m_purid;
	int m_maxBookCount; 
	
	boolean m_optionMenuOpened = false;
	BookForLibrary m_curbook = null;
	
	SharedPreferences prefs; 
	
	boolean isFirstRun = true;
	
	boolean isInternetConnected = true;
	
	/*
	 * =스레드안에서= 1) 책리스트가 없으면 책 리스트를 다운받는다. =>파일로 저장한다 2) 책리스트 파일 로딩 파싱해서
	 * bookslist를 만든다 3) 커버이미지가 없으면 다운받는다 4) 화면에 그려준다.
	 */
	void CheckPurchasedBooklist()
	{
		if (Global.g_bLogin)
		{
			String url = String.format("http://www.ybmmobile.com/appstore/epub/purchase_list.asp?user_id=%s",Global.g_LoginID);
			String str = HttpUtil.DownloadHtml(url);

			if (str != null)
			{
				for(int i = 0; i < Global.books.books.size(); ++i)
				{
					BookForLibrary book = Global.books.books.get(i);
					String bookId = String.format("\"%s\"",book.bookId);
					book.isPurchased = (str.indexOf(bookId) > 0);
				}
			}
		}
	}

	boolean IsPurchasedBook(String bookId)
	{
		for(int i = 0; i < Global.books.books.size(); ++i)
		{
			BookForLibrary book = Global.books.books.get(i);
			if (book.bookId.equalsIgnoreCase(bookId))
				return book.isPurchased;
		}
		return false;
	}
	
	void LoginOK(String userid, String pwd, boolean autologin) // 로그인 완료되는 경우 
	{
		final Context thisContext = this;
		
		String _id = NFEncode.nfencode(userid);
		String _pwd = NFEncode.nfencode(pwd);
		String loginUrl = String.format("https://certify.ybmsisa.com/brandapp/wbook/ybmlogingw.asp?id=%s&pw=%s&domain=",_id, _pwd);
		String str = HttpUtil.DownloadHtml(loginUrl);
	
		if(str != null)
		{
			final String rCode = "<ResultCode>";
			int r1 = str.indexOf(rCode) + rCode.length();
			int retCode = Integer.parseInt(str.substring(r1, r1 + 1));
			if (retCode == 1)
			{
				final String kName = "<KName>";
				int r2 = str.indexOf(kName);
				int r3 = str.indexOf("</KName>");
				Global.g_LoginName = str.substring(r2 + kName.length(), r3); 
			
				final String email = "<email>";
				r2 = str.indexOf(email);
				r3 = str.indexOf("</email>");
				Global.g_LoginEmail = str.substring(r2 + email.length(), r3); 
				
				Global.g_LoginID = userid;
				Global.g_LoginPwd = pwd;
				Global.g_bLogin = true;
				
				final Button button2 = (Button)findViewById(R.id.BS_button_login_bottom);
				button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.logout));
				
				final Button button = (Button)findViewById(R.id.BS_button_login);
				int wid = button.getWidth();
				int hgt = button.getHeight();
				
				// 로그인이 완료되어 로그아웃 버튼으로 바꿈
				button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_a));
				button.setWidth(wid);
				button.setHeight(hgt);
				
				SharedPreferences.Editor editor = prefs.edit();
				editor.clear();
				editor.putString("g_LoginID", userid);
				editor.putString("g_LoginPwd", pwd);
								
				if (autologin)
				{
					editor.putBoolean("g_bLogin", true);
					SaveID();
				}
				else
					editor.putBoolean("g_bLogin", false);

				editor.commit();
				
				CheckPurchasedBooklist();
				/*
				 *To do : 구매한 책과, 그렇지 않은 책의 정보가 들어왔으니 이에 맞게 다시 그려준다 !!!
				 * */
				
				SortPurchased();
				
				
			}
			else
			{
					AlertDialog.Builder aDialog = new AlertDialog.Builder(thisContext);
					aDialog.setTitle("Login 실패");
					aDialog.setMessage("아이디와 비밀번호를 다시 한 번 확인해주세요.");
					aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								((Library)thisContext).PopupLoginDialog();
							}
						});
	
					AlertDialog ad = aDialog.create();
					ad.show();		
			}
		}
		else
		{
			AlertDialog.Builder aDialog = new AlertDialog.Builder(thisContext);
			aDialog.setTitle("Login 실패");
			aDialog.setMessage("서버에 접속 실패 했습니다.");
			aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {}
				});

			AlertDialog ad = aDialog.create();
			ad.show();		
		}		
	}
	
	void PopupLoginDialog()
	{
		//id : ybmaster
		//pwd: 1111
		
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.login, null);//(ViewGroup) findViewById(R.id.main));
						
		AlertDialog.Builder aDialog = new AlertDialog.Builder(this);
		aDialog.setView(layout);
		
		LinearLayout loginArea = (LinearLayout)layout.findViewById(R.id.login);
		loginArea.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) 
				{
					InputMethodManager inputManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		
					EditText editText = (EditText)v.findViewById(R.id.editId);
					inputManager.hideSoftInputFromWindow(editText.getWindowToken(),0);
					
					editText = (EditText)v.findViewById(R.id.editPwd);
					inputManager.hideSoftInputFromWindow(editText.getWindowToken(),0);
				}
			});
								
		aDialog.setPositiveButton("로그인", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				String userid = ((EditText)layout.findViewById(R.id.editId)).getText().toString();
				String pwd = ((EditText)layout.findViewById(R.id.editPwd)).getText().toString();
				boolean autologin = ((CheckBox)layout.findViewById(R.id.checkAutoLogin)).isChecked();
				
				LoginOK(userid, pwd, autologin);
				if (autologin == false)
				{// 로긴 정보가 있는 파일을 지운다 
					String dirPath = getFilesDir().getAbsolutePath();
					String filename = dirPath + "/mmmmm";
					File file = new File(filename);
					file.delete();
				}
			}
		});
		aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		AlertDialog ad = aDialog.create();
		ad.show();		
	}
	
	void SaveID()
	{
		String dirPath = getFilesDir().getAbsolutePath();
		String filename = dirPath + "/mmmmm";
		try {

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(filename));
			byte[] eId = Base64Util.decode(Global.g_LoginID);//.getBytes();
			byte[] ePwd = Base64Util.decode(Global.g_LoginPwd);//.getBytes();

			out.putNextEntry(new ZipEntry("/books1"));
			out.write(Global.g_LoginID.length());
			out.write(eId, 0, eId.length);
			out.closeEntry();

			out.putNextEntry(new ZipEntry("/books2"));
			out.write(Global.g_LoginPwd.length());
			out.write(ePwd, 0, ePwd.length);
			out.closeEntry();
			out.close();

		} catch (IOException e) {
		}
	}

	void LoadID()
	{
		String dirPath = getFilesDir().getAbsolutePath();
		String filename = dirPath + "/mmmmm";
		File file = new File(filename);

		if (file.exists()) {
			try {
				FileInputStream fis = new FileInputStream(filename);

				ZipDecryptInputStream zdis = new ZipDecryptInputStream(fis, "eoqkrsktp.");
				ZipInputStream zis = new ZipInputStream(zdis);
				ZipEntry ze;
				while ((ze = zis.getNextEntry()) != null) 
				{
					if (ze.getName().compareToIgnoreCase("/books1") == 0 || ze.getName().compareToIgnoreCase("/books2") == 0) 
					{
						ByteArrayBuffer buf = new ByteArrayBuffer(5000);
						int BUFFER = 2048;
						byte data[] = new byte[BUFFER];

						int len = zis.read();
						int count;
						while ((count = zis.read(data, 0, BUFFER)) != -1)
							buf.append(data, 0, count);
						
						if (ze.getName().compareToIgnoreCase("/books1") == 0)
							Global.g_LoginID = Base64Util.encode(buf.buffer()).toString().substring(0, len);
						else
							Global.g_LoginPwd = Base64Util.encode(buf.buffer()).toString().substring(0, len);
			
					}
					zis.closeEntry();
				}
				zis.close();
				
				LoginOK(Global.g_LoginID, Global.g_LoginPwd, false);
				
			} catch (Exception e) {
				file.delete();
				Logger.d("dd", "Error in unzip books");
			}
		}
	}
	
	void Logout( MotionEvent event)
	{
		final Button button = (Button)findViewById(R.id.BS_button_login);
	
		if(event.getAction() == event.ACTION_DOWN){
			int wid = button.getWidth();
			int hgt = button.getHeight();
			button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_b));
			button.setWidth((int)(wid * 0.7));
			button.setHeight((int)(hgt * 0.7));
		}
		else if(event.getAction() == event.ACTION_UP){
			/*
			AlertDialog.Builder aDialog = new AlertDialog.Builder(this);
			aDialog.setTitle("로그 아웃");
			aDialog.setMessage("로그아웃 하시겠습니까?");
			
			aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						
						button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
						Global.g_bLogin = false;			
						Global.g_LoginName = null;
						Global.g_LoginEmail = null;
						Global.g_LoginID = null;
						Global.g_LoginPwd = null;
						
						String dirPath = getFilesDir().getAbsolutePath();
						String filename = dirPath + "/mmmmm";
						File file = new File(filename);
						file.delete();
					}
				});
			
			aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_a));
				}
			});

			AlertDialog ad = aDialog.create();
			ad.show();		
			*/
			
			PopupLogoutDialog();
		}
	}
	
	void PopupLogoutDialog()
	{
		AlertDialog.Builder aDialog = new AlertDialog.Builder(this);
		aDialog.setTitle("로그 아웃");
		aDialog.setMessage("로그아웃 하시겠습니까?");
		
		aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					final Button button = (Button)findViewById(R.id.BS_button_login);
					button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
					
					final Button button2 = (Button)findViewById(R.id.BS_button_login_bottom);
					button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.loginbutton));
					
					Global.g_bLogin = false;			
					Global.g_LoginName = null;
					Global.g_LoginEmail = null;
					Global.g_LoginID = null;
					Global.g_LoginPwd = null;
					
					String dirPath = getFilesDir().getAbsolutePath();
					String filename = dirPath + "/mmmmm";
					File file = new File(filename);
					file.delete();
					
					SharedPreferences.Editor editor = prefs.edit();
					editor.clear();
					editor.commit();
					
					int i;
					for(i=0;i<=Global.books.books.size()-1;i++){
						
						if(i >= m_maxBookCount) break; 
						
						ImageView overlay = (ImageView)findViewById(m_overlays[i]);
						ImageView purchase = (ImageView)findViewById(m_purid[i]);
						
						overlay.setVisibility(ImageView.VISIBLE);
						purchase.setVisibility(ImageView.INVISIBLE);
					}
					
				}
			});
		
		aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				final Button button = (Button)findViewById(R.id.BS_button_login);
				button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_a));
				
				final Button button2 = (Button)findViewById(R.id.BS_button_login_bottom);
				button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.logout));
			}
		});

		AlertDialog ad = aDialog.create();
		ad.show();
		
	}
	

	void initMenuButtons()
	{
		final Button button_storage = (Button)findViewById(R.id.BS_button_storage);
		button_storage.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){
					Intent myIntent = new Intent(v.getContext(), Library.class);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(myIntent);
		            overridePendingTransition(0, 0);
				}
				return false;
			}
		});
		
		final Button button_mylibrary = (Button)findViewById(R.id.BS_button_mylibrary);
		button_mylibrary.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){
					Intent myIntent = new Intent(v.getContext(), MyLibrary.class);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(myIntent);
		            overridePendingTransition(0, 0);
				}
				return false;
			}
		});
		
		final Button buttonSearch = (Button)findViewById(R.id.BS_button_booksearch);
		buttonSearch.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){
					if( Global.g_bLogin == false ) {
						new AlertDialog.Builder(Library.this)
						.setTitle("알림")
						.setMessage("로그인 하신 후, 이용하실 수 있습니다.")
						.setPositiveButton("확인", null)
						.show();
						return false;
					}
				
					Intent myIntent = new Intent(v.getContext(), SearchBook.class);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(myIntent);
					overridePendingTransition(0, 0);
				}
				
				return false;
			}
		});
		
		final Button buttonInfo = (Button)findViewById(R.id.BS_button_info);
		buttonInfo.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				Intent myIntent = new Intent(v.getContext(), UserGuide.class);
				myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent);
				overridePendingTransition(0, 0);
				
				return false;
			}
		});
	
		final Button buttonLogin = (Button)findViewById(R.id.BS_button_login_bottom);
		buttonLogin.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if (Global.g_bLogin == false)
				{
					if(event.getAction() == MotionEvent.ACTION_UP){
						// button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
						
						//login 창을 띄운
						PopupLoginDialog();
					}
				}
				else
				{
					if(event.getAction() == MotionEvent.ACTION_UP){
						PopupLogoutDialog();
					}
					//Logout(event);
				}
				return false;
			}
		});
	}


	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// 인터넷 연결 확인 코드
		/*
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		try{
			NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			//boolean isWifiAvail = ni.isAvailable();
			boolean isWifiConn = ni.isConnected();
			ni = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			//boolean isMobileAvail = ni.isAvailable();
			boolean isMobileConn = ni.isConnected();
	    		  
			if(isWifiConn==false && isMobileConn==false)
			{
				//인터넷에 연결할 수 없습니다. 연결을 확인하세요.
				AlertDialog.Builder alert_internet_status = new AlertDialog.Builder(this);
				alert_internet_status.setTitle( "인터넷연결" );          		
				alert_internet_status.setMessage( "인터넷연결을 확인하세요" );
				alert_internet_status.setPositiveButton( "닫기", new DialogInterface.OnClickListener() {
					public void onClick( DialogInterface dialog, int which) {
						dialog.dismiss();   //닫기
						finish();
					}
				});
				alert_internet_status.show();
				   
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		*/
		
		
		m_maxBookCount = 50;
		
		setContentView(R.layout.bookshelf);
		
		init();
		
		TelephonyManager telManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		Global.g_DeviceToken = telManager.getDeviceId();
		
		prefs = getSharedPreferences("PrefName", MODE_PRIVATE);		
		Global.g_bLogin = prefs.getBoolean("g_bLogin", false);
		Global.g_LoginID = prefs.getString("g_LoginID", "");
		Global.g_LoginPwd = prefs.getString("g_LoginPwd", "");
		
		initMenuButtons();
		
		final Button button = (Button)findViewById(R.id.BS_button_login);
		button.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (Global.g_bLogin == false)
				{
					if(event.getAction() == event.ACTION_DOWN){
						button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_b));
					}
					else if(event.getAction() == event.ACTION_UP){
						button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
						
						//login 창을 띄운
						PopupLoginDialog();
					}
				}
				else
				{
					Logout(event);
				}
				return false;
			}
		});
		
		final Button button_toggle = (Button)findViewById(R.id.BS_button_toggle_view);
		button_toggle.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.list_view_on));
				}
				else if(event.getAction() == MotionEvent.ACTION_UP){
					button_toggle.setBackgroundDrawable(getResources().getDrawable(R.drawable.list_view_none));
					Intent myIntent = new Intent(v.getContext(), LibraryListView.class);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(myIntent);
		            //startActivityForResult(myIntent, 0);
		            overridePendingTransition(0, 0);
				}
				return false;
			}
		});
		
		Util.absolutePath = getFilesDir().getAbsolutePath();
		
		//libraryView = new LibraryView(this);
		//setContentView(libraryView);

		// 화면 회전 대응을 위해 추가
		Resources r = Resources.getSystem();
		Configuration config = r.getConfiguration();
		onConfigurationChanged(config);

		// 책 리스트는 시작할때 불러줍니다.
		
		RelativeLayout intro = (RelativeLayout)findViewById(R.id.BS_rel_intro);
		intro.setVisibility(RelativeLayout.VISIBLE);		
		
		
		if(Global.g_bShippingVersion == true){
			Thread load = new Thread(new Runnable()	{
				public void run(){
					handler.postDelayed(new Runnable() {
						public void run(){
							LoadBooks(0);
							if( isInternetConnected == false ) return; 
							LoadCoverImages();
							
							if(Global.g_bLogin == true)
								SortPurchased();
					
							LoadID();
							
							if(Global.g_bLogin == true){
								LoginOK(Global.g_LoginID, Global.g_LoginPwd, Global.g_bLogin);			
							}
							
							RelativeLayout intro = (RelativeLayout)findViewById(R.id.BS_rel_intro);
							intro.setVisibility(RelativeLayout.INVISIBLE);	
						}
					}, 2000);
				}
			});
	
			load.start();
		}
		else{
			LoadBooks(0);
			LoadCoverImages();
			//libraryView.postInvalidate();
	
			LoadID();									
			
			if(Global.g_bLogin == true){
				LoginOK(Global.g_LoginID, Global.g_LoginPwd, Global.g_bLogin);			
			}
			
			intro = (RelativeLayout)findViewById(R.id.BS_rel_intro);
			intro.setVisibility(RelativeLayout.INVISIBLE);
		}
	}
	
	@Override
	 public void onResume() {
		super.onResume();
		
		if( isFirstRun == false )
		{
	    	final Button button = (Button)findViewById(R.id.BS_button_login);
	    	final Button button2 = (Button)findViewById(R.id.BS_button_login_bottom);
			
			
	    	if( Global.g_bLogin )
	    	{
	    		button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_a));
	    		button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.logout));	
	    	}
	    	else{
	    		button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_login_a));
	    		button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.loginbutton));
	    	}
	    	
	    	if( Global.books != null )
	    	{
		    	int size = Global.books.books.size();
		    	
				for( int i=0; i<=size-1; i++ )
				{	
					if(i >= m_maxBookCount)
						break;
					
					final BookForLibrary book = Global.books.books.get(i);
					String bookFilename = book.bookUrl.substring(book.bookUrl.lastIndexOf('/'));
					final String bookPath = Util.absolutePath + bookFilename;
					File bookFile = new File(bookPath);
					
					ImageView overlay = (ImageView)findViewById(m_overlays[i]);
					ImageView purchase = (ImageView)findViewById(m_purid[i]);
					if (bookFile.exists() && Global.g_bLogin ) 
					{
						// 다운 받았던 책은 꺼먼 오버레이를 안 보이게
						overlay.setVisibility(ImageView.INVISIBLE);
						purchase.setVisibility(ImageView.INVISIBLE);
					} else 
					{
						// 없는 책은 꺼먼 오버레이를 덧 씌움
						overlay.setVisibility(ImageView.VISIBLE);
						purchase.setVisibility(ImageView.VISIBLE);
					}
				}
			}
		}
		isFirstRun = false; 
	 }	 
	
	private void LoadBooks(int cnt) {
		if (cnt > 3)// 이 함수는 재귀호출로 성공할때까지 반복하는데 너무 많이 실행 되는것 방지
			return;
		// 책 리스트가 있는 books파일이 이미 있는가?!
		String dirPath = getFilesDir().getAbsolutePath();
		String filename = dirPath + "/books";
		File file = new File(filename);
		file.delete();
		if (file.exists()) {
			// String password = "s3o4a76ka8e3o4f3h^^;;";
			try {
				FileInputStream fis = new FileInputStream(filename);
				ZipDecryptInputStream zdis = new ZipDecryptInputStream(fis,"eoqkrsktp.");
				ZipInputStream zis = new ZipInputStream(zdis);
				ZipEntry ze;
				while ((ze = zis.getNextEntry()) != null) {
					if (ze.getName().compareToIgnoreCase("/books") == 0) {
						ByteArrayBuffer buf = new ByteArrayBuffer(5000);
						int BUFFER = 2048;
						byte data[] = new byte[BUFFER];

						int count;
						while ((count = zis.read(data, 0, BUFFER)) != -1)
							buf.append(data, 0, count);
						Global.books = new BooksForLibrary();
						Global.books.CreateWithData(buf);

					}
					zis.closeEntry();
				}
				zis.close();
				
				return;
			} catch (Exception e) {
				file.delete();
				Logger.d("dd", "Error in unzip books");
			}
		}

		// 파일이 없거나 에러가 발생하면 다시 다운 받아서 파싱한다.
		String DownloadURL = "http://www.ybmmobile.com/appstore/epub/product_list.asp";
		ByteArrayBuffer buf = new ByteArrayBuffer(512);
		
		Util.DownloadToBuffer(DownloadURL, buf);
		
		if( buf.length() == 0 )
		{
			AlertDialog alertDialog = new AlertDialog.Builder(this)
			.setTitle("알림")
			.setMessage("인터넷이 연결되어 있지 않습니다. 인터넷 연결 후 재시도 해 주세요.")
			.setNegativeButton("종료", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			}).create();
			
			alertDialog.show();
			
			isInternetConnected = false;
			return;
		}
		
		if (BooksParsing(buf)) {
			SaveBooks(filename);
			// LoadBooks(cnt++);
		}
	}

	private void SaveBooks(String filename1) {
		File file = new File(filename1);
		file.delete();

		try {

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
					filename1));
			out.putNextEntry(new ZipEntry("/books"));
			ByteArrayBuffer buf = Global.books.GetRawData();
			out.write(buf.buffer(), 0, buf.length());
			out.closeEntry();
			out.close();

		} catch (IOException e) {
		}
	}

	void SortPurchased()
	{
		int size = Global.books.books.size();
		int i, j=0;
		for(i=0;i<=size-1;i++){
			
			if(i >= m_maxBookCount)
				break;
			
			final BookForLibrary book = Global.books.books.get(i);
			String bookFilename = book.bookUrl.substring(book.bookUrl.lastIndexOf('/'));
			final String bookPath = Util.absolutePath + bookFilename;
			File bookFile = new File(bookPath);
			
			//if (IsPurchasedBook(book.bookId) == true) {
			if(bookFile.exists()){
				Global.books.books.remove(i);
				Global.books.books.add(j++, book);
			}
		}
		
		for(i=0;i<=size-1;i++){
			
			if(i >= m_maxBookCount)
				break;
			
			final BookForLibrary book = Global.books.books.get(i);
			String bookFilename = book.bookUrl.substring(book.bookUrl.lastIndexOf('/'));
			final String bookPath = Util.absolutePath + bookFilename;
			File bookFile = new File(bookPath);
			
			ImageView image = (ImageView)findViewById(m_bookid[i]);
			
			image.setImageBitmap(book.bookCoverImage);
			
			ImageView overlay = (ImageView)findViewById(m_overlays[i]);
			ImageView purchase = (ImageView)findViewById(m_purid[i]);
			if (bookFile.exists()) {
				// 다운 받았던 책은 꺼먼 오버레이를 안 보이게
				overlay.setVisibility(ImageView.INVISIBLE);
				purchase.setVisibility(ImageView.INVISIBLE);
			} else {
				// 없는 책은 꺼먼 오버레이를 덧 씌움
				overlay.setVisibility(ImageView.VISIBLE);
				purchase.setVisibility(ImageView.VISIBLE);
			}
			
			image.setOnClickListener(new View.OnClickListener() {							
				public void onClick(View v) {
					
    				if (Global.g_bShippingVersion) {
	    				if (!Global.g_bLogin) {
	    					new AlertDialog.Builder(Library.this)
	    						.setTitle("알림")
	    						.setMessage("로그인 하신 후, 이용하실 수 있습니다.")
	    						.setPositiveButton("확인", null)
	    						.show();
	    					return;
	    				}
    				}
					
					m_curbook = book;
					
					String dirPath = getFilesDir().getAbsolutePath();
					String filename = dirPath + "/recbk";
									
					try {
						File file = new File(filename);
						FileOutputStream fout = new FileOutputStream(file);
						String bookId = m_curbook.bookId;
						
						byte[] buf = bookId.getBytes();
						fout.write(buf);
						fout.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}catch (IOException e){
						e.printStackTrace();
					}
					
					OpenBook(book);
				}
			});
		}
	}
	
	private void LoadCoverImages() {
		// 이미 로그인 된 상태로 프로그램을 켰다면 로그인 버튼을 로그아웃 그림으로 바꿈
		if (Global.g_bLogin) {
			final Button button = (Button)findViewById(R.id.BS_button_login);
			int wid = button.getWidth();
			int hgt = button.getHeight();
			
			// 로그인이 완료되어 로그아웃 버튼으로 바꿈
			button.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_btn_logout_a));
			button.setWidth(wid);
			button.setHeight(hgt);
		}
		
		String dirPath = getFilesDir().getAbsolutePath();
		String filename = dirPath + "/books2";
		File file = new File(filename);
		if (!file.exists()) {
			Util.Download(Global.books.coversUrl, filename);
		}
		
		try {
			FileInputStream fis = new FileInputStream(filename);
			ZipInputStream zis = new ZipInputStream(fis); // 여긴 암호 필요 없음
			ZipEntry ze;
			int i = 0;
			while ((ze = zis.getNextEntry()) != null) {
				int size = Global.books.books.size();
				for (int g = 0; g < size; ++g) {
					final BookForLibrary book = Global.books.books.get(g);
					if (ze.getName().compareToIgnoreCase(book.bookCover) == 0) {
						ByteArrayBuffer buf = new ByteArrayBuffer(5000);
						int BUFFER = 2048;
						byte data[] = new byte[BUFFER];

						int count;
						while ((count = zis.read(data, 0, BUFFER)) > 0)
							buf.append(data, 0, count);

						Bitmap oriBm = BitmapFactory.decodeByteArray(
								buf.buffer(), 0, buf.length());
						
						// 책 표지
						ImageView image = (ImageView)findViewById(m_bookid[i]);
						
						//book.layout_Idx = i;
						book.bookCoverImage = Bitmap.createScaledBitmap(oriBm,
								130, 200, true);
						image.setImageBitmap(book.bookCoverImage);
							
						RelativeLayout shadow = (RelativeLayout)findViewById(m_shadowid[i]);
						RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)shadow.getLayoutParams();
						params.width = 130;
						params.height = 200;
						params.topMargin = 3;
						params.leftMargin = 3;
						shadow.setLayoutParams(params);
						
						// 산 책 안 산 책을 구분하는 오버레이 처리
						ImageView overlay = (ImageView)findViewById(m_overlays[i]);
						params = (RelativeLayout.LayoutParams)overlay.getLayoutParams();
						params.width = 130;
						params.height = 200;
						
						// 다운 받은 책 파일이 있는지 검사
						String bookFilename = book.bookUrl.substring(book.bookUrl.lastIndexOf('/'));
		    			final String bookPath = Util.absolutePath + bookFilename;
		    			File bookFile = new File(bookPath);
		    			
						//if (Global.g_bLogin == true && bookFile.exists()) {
							// 다운 받았던 책은 꺼먼 오버레이를 안 보이게
						//	overlay.setVisibility(ImageView.INVISIBLE);
						//} else {
							// 없는 책은 꺼먼 오버레이를 덧 씌움
							overlay.setVisibility(ImageView.VISIBLE);
						//}
						
						overlay.setLayoutParams(params);
						
						ImageView purchase = (ImageView)findViewById(m_purid[i]);
						params = (RelativeLayout.LayoutParams)purchase.getLayoutParams();
						params.width = 130;
						params.height = 200;
						purchase.setLayoutParams(params);
						purchase.setVisibility(ImageView.INVISIBLE);
						
						image.setOnClickListener(new View.OnClickListener() {							
							public void onClick(View v) {
								
			    				if (Global.g_bShippingVersion) {
				    				if (!Global.g_bLogin) {
				    					new AlertDialog.Builder(Library.this)
				    						.setTitle("알림")
				    						.setMessage("로그인 하신 후, 이용하실 수 있습니다.")
				    						.setPositiveButton("확인", null)
				    						.show();
				    					return;
				    				}
			    				}
								
								m_curbook = book;
								
								String dirPath = getFilesDir().getAbsolutePath();
								String filename = dirPath + "/recbk";
												
								try {
									File file = new File(filename);
									FileOutputStream fout = new FileOutputStream(file);
									String bookId = m_curbook.bookId;
									
									byte[] buf = bookId.getBytes();
									fout.write(buf);
									fout.close();
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								}catch (IOException e){
									e.printStackTrace();
								}
								
								//file.delete();
								
								OpenBook(book);
							}
						});
						
						if(i == m_maxBookCount-1) return; // 임시로
						
						/*
						book.bookRect = new Rect((i % 3) * 180 + 60,
								(i / 3) * 191 + 60, (i % 3) * 180 + 60 + 100,
								(i / 3) * 191 + 60 + 150);
						*/
						i++;
						break;
					}
				}

				zis.closeEntry();
			}
			zis.close();
			return;
		} catch (Exception e) {
			file.delete();
			Logger.d("dd", "Error in unzip books' covers");
		}
		LoadCoverImages();
	}

	private Boolean BooksParsing(ByteArrayBuffer buf) {
		// 파싱
		try {
			BookForLibrary currentBook = new BookForLibrary();

			XmlPullParserFactory parserCreator = XmlPullParserFactory
					.newInstance();
			XmlPullParser parser = parserCreator.newPullParser();
			String szXml = new String(buf.buffer(), "euc-kr");
			parser.setInput(new StringReader(szXml));
			Boolean bBookName = new Boolean(false);
			int parserEvent = parser.getEventType();
			String tag;
			while (parserEvent != XmlPullParser.END_DOCUMENT) {
				switch (parserEvent) {

				case XmlPullParser.TEXT:
					if (bBookName) {
						currentBook.bookName += parser.getText();
					}
					break;
				case XmlPullParser.START_TAG:
					tag = parser.getName();
					// Logger.d("시작 태그", tag);

					if (tag.compareToIgnoreCase("books") == 0) {
						Global.books = new BooksForLibrary();
					} else if (tag.compareToIgnoreCase("covers") == 0) {
						Global.books.coversUrl = parser.getAttributeValue(null, "url");
					} else if (tag.compareToIgnoreCase("book") == 0) {
						bBookName = false;
						currentBook = new BookForLibrary();
						currentBook.bookId = parser.getAttributeValue(null,
								"id");
					} else if (tag.compareToIgnoreCase("bookname") == 0) {
						bBookName = true;
						currentBook.bookName = "";
					} else if (tag.compareToIgnoreCase("cover") == 0) {
						currentBook.bookCover = parser.getAttributeValue(null,
								"src");
					} else if (tag.compareToIgnoreCase("down") == 0) {
						currentBook.bookUrl = parser.getAttributeValue(null,
								"url");
					} else if (tag.compareToIgnoreCase("price") == 0) {
						currentBook.bookPrice = parser.getAttributeValue(null,
								"cost");
					}
					break;
				case XmlPullParser.END_TAG:
					tag = parser.getName();
					// Logger.d("끝 태그", tag);
					if (tag.compareToIgnoreCase("bookname") == 0) {
						bBookName = false;
					} else if (tag.compareToIgnoreCase("book") == 0) {
						Global.books.AddBook(currentBook);
					}

					break;
				}
				parserEvent = parser.next();
			}
		} catch (Exception e) {
			Logger.d("dd", "book xml parsing error");
			return false;
		}
		return true;
	}

	//public void OpenBook(String filename) {
		// 새로운 엑티비티 실행
	//	Intent intent = new Intent(Library.this, EPubViewer.class);
	//	intent.putExtra("bookFile", filename);
	//	startActivity(intent);
	//}
	
	public static String HttpGetByString(String getURL) {
		try {
	        HttpClient client = new DefaultHttpClient(); 
	        HttpGet get = new HttpGet(getURL);
	        HttpResponse responseGet = client.execute(get);  
	        HttpEntity resEntityGet = responseGet.getEntity();  
	        
	        if (resEntityGet != null) {
	        	return EntityUtils.toString(resEntityGet);
            } else {
            	return null;
            }
		} catch (Exception e) {
		    Log.i("HTTP ERROR", e.toString());
		    return null;
		}
	}
	
	public static String HttpPostByString(String url, List<NameValuePair> data, String encoding) {
		try {
	        HttpClient client = new DefaultHttpClient();  
	        String postURL = url;
	        HttpPost post = new HttpPost(postURL);
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(data, encoding);
            post.setEntity(ent);
            HttpResponse responsePOST = client.execute(post);  
            HttpEntity resEntity = responsePOST.getEntity();  
            
            if (resEntity != null) {
                return EntityUtils.toString(resEntity, encoding);
            } else {
            	return null;
            }
	    } catch (Exception e) {
	    	Log.i("HTTP ERROR", e.toString());
	        return null;
	    }
	}
	
	boolean IsDownloadAllowed(BookForLibrary book) {
		String url = "http://www.ybmmobile.com/appstore/epub/download_list.asp?user_id=" + Global.g_LoginID + 
			"&device_id=" + Global.g_DeviceToken + "&contents_id=" + book.bookId + "&flag=";
		String str = HttpGetByString(url);
		
		String msg = "다른 기기에서 이미 인증 받은 도서입니다. 고객님의 아이디가 도용되지 않았는지 확인해주시길 바랍니다.";
		if (str != null && str.contains("TRUE")) {
			return true;
		} else {
			new AlertDialog.Builder(this)
				.setTitle("알림")
				.setMessage(msg)
				.setPositiveButton("확인", null)
				.show();
			return false;
		}
	}
	
	public void OpenBook(BookForLibrary book) // 원본은 LibraryView에 그대로 놓아둠.
	{
    	//책 파일이 있는지 검사
    	byte []bookUrl = book.bookUrl.getBytes();
    	for(int i = book.bookUrl.length() - 1; i >=0; --i)
    	{
    		if (bookUrl[i] =='/')
    		{
    			String filename = new String(bookUrl, i + 1, book.bookUrl.length() - i - 1);
    			
    			final String path = Util.absolutePath + "/" + filename;
    			File file = new File(path);
    			if (!file.exists())	
    			{
    				//Log.i("DEBUG", "책이 로컬에 없어서 다운로드");
    				
    				// 결제 체크를 하지 않으려면 g_ShippingVersion를 false로 하면 됨
    				if (Global.g_bShippingVersion) {
	    				if (!Global.g_bLogin) {
	    					new AlertDialog.Builder(this)
	    						.setTitle("알림")
	    						.setMessage("로그인 하신 후, 이용하실 수 있습니다.")
	    						.setPositiveButton("확인", null)
	    						.show();
	    					return;
	    				}
	    				
	    				if (this.IsDownloadAllowed(book) == false) { // [테스트!!!] 이거 지금 껐음
	    					return;
	    				}
	    				
	    				//구매가 안되어있나? - 이미 구매하기 버튼이 나와 있음 
	    				//if (!this.IsPurchasedBook(book.bookId) || true) { // [테스트!!!] 무조건 결제 할 수 있도록 함
	    				if (!this.IsPurchasedBook(book.bookId)) { // [테스트!!!] 무조건 결제 할 수 있도록 함
	    					final String bookId = book.bookId;
	    					final String bookPrice = book.bookPrice;
	    					final String bookName = book.bookName;
	    					
	    					new AlertDialog.Builder(this)
								.setTitle("알림")
								.setMessage("해당 도서는 한 아이디당 기기 1대로 인증됩니다. 한 번 다운로드 받으신 기기 외에 다른 기기에서는 다운로드가 불가능합니다.")
								.setPositiveButton("확인", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										// 결제 화면 띄우기
										Intent intent = new Intent(Library.this, Payment.class);
										intent.putExtra("bookId", bookId);
										intent.putExtra("bookPrice", bookPrice);
										intent.putExtra("bookName", bookName);
				    					startActivity(intent);
									}
								})
								.setNegativeButton("취소", null)
								.show();
	    					
	    					//return;
	    				}
	    				
	    				//이 디바이스 키체인으로 다운 받는게 허용되어있나?
	    				if (false && this.IsDownloadAllowed(book) == false) { // [테스트!!!] 이거 지금 껐음
	    					return;
	    				}
    				}
    				
    				proDialog1 = ProgressDialog.show(this, "", "Loading", true);
    				    				
    				final BookForLibrary final_book = book;
    				
    				new Thread(new Runnable() {
    	            	public void run() {    	                	    	                	    	      
    	            		
    	    				Util.Download(final_book.bookUrl, path);
    	    				
    	    				handler.post(new Runnable() {
    	    					public void run() {
    	    						if(proDialog1.isShowing()){
    	    							proDialog1.hide();
    	    							proDialog1.dismiss();
    	    						}
    	    					}
    	    				});
    	    				
    	    				Intent intent = new Intent(Library.this, EPubViewer.class);
    	    				intent.putExtra("bookFile", path);
    	    				startActivity(intent);
    	    				
    	    				
    	    				// 구매하자마자 보이게 함.
    	    				handler.post(new Runnable(){
    	    					public void run(){
    	    						SortPurchased();
    	    					}
    	    				});
    	    				
    	    				//ImageView overlay = (ImageView)findViewById(m_overlays[final_book.layout_Idx]);
    	    				//overlay.setVisibility(ImageView.INVISIBLE);
    	            	}
    	            }).start();
    				
    			}
    			else
    			{
    				//Log.i("DEBUG", "책을 로컬에서 읽음");
    				
    				// TEST 도중, 책을 지우는 경우. 캐시 파일도 함께 지움.
    				/*file.delete();
    				String decFilename = path + ".cache";
    				File cfile = new File(decFilename);
    				if(cfile.exists())
    					cfile.delete();*/
    				
    				Intent intent = new Intent(Library.this, EPubViewer.class);
    				intent.putExtra("bookFile", path);
    				intent.putExtra("bookId", book.bookId);
    				startActivity(intent);
    				
    			}
    			break;
    		}
    	}
    }
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) { // 세로 전환시 발생
			Global.g_bShowTwoPages = false;
		} else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) { // 가로 전환시 발생
			Global.g_bShowTwoPages = true;
		}
		
		super.onConfigurationChanged(newConfig);

		/*
		 * View LoadPage = findViewById(R.id.ID_LAYOUT);
		 * 
		 * 
		 * switch(newConfig.orientation){
		 * 
		 * case Configuration.ORIENTATION_LANDSCAPE:
		 * LoadPage.setBackgroundResource(R.drawable.back_imgw);
		 * Logger.d("Androes", "Loading Start - back_imgw"); break;
		 * 
		 * case Configuration.ORIENTATION_PORTRAIT:
		 * LoadPage.setBackgroundResource(R.drawable.back_img);
		 * Logger.d("Androes", "Loading Start - back_img"); break;
		 * 
		 * }
		 */
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		final RelativeLayout optionmenu = (RelativeLayout)findViewById(R.id.BS_rel_bottmbar);
		if (keyCode == KeyEvent.KEYCODE_MENU) { // 메뉴 버튼을 누르면 옵션 메뉴를 생성			
			if(m_optionMenuOpened == false){
				m_optionMenuOpened = true;
				optionmenu.setVisibility(RelativeLayout.VISIBLE);
				
				/*final Button button_curbook = (Button)findViewById(R.id.BS_button_curbook);
				button_curbook.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						optionmenu.setVisibility(RelativeLayout.INVISIBLE);
						m_optionMenuOpened = false;
						
						String dirPath = getFilesDir().getAbsolutePath();
						String filename = dirPath + "/recbk";
										
						try {
							//File file = new File(filename);
							//FileInputStream fin = openFileInput("recbk");	
							FileInputStream fin = new FileInputStream(filename);
							byte[] buf = new byte[101];
							
							fin.read(buf);
							String tmp = new String(buf);
							String bookId = null;
							if('0' <= tmp.charAt(1) && tmp.charAt(1) <= '9')
								bookId = tmp.substring(0, 2);
							else
								bookId = tmp.substring(0, 1);
							
							m_curbook = Global.books.findBookById(bookId);
						}
						catch(FileNotFoundException e){
							//e.printStackTrace();
							File file = new File(filename);
							return;
						}
						catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							File file = new File(filename);
							return;
						}
						
						if(m_curbook == null)
							return;
						
						OpenBook(m_curbook);
					}
				});
				
				final Button button_login = (Button)findViewById(R.id.BS_button_bottomlogin);
				if(Global.g_bLogin){
					button_login.setBackgroundDrawable(getResources().getDrawable(R.drawable.option_icon_logout));
					button_login.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							optionmenu.setVisibility(RelativeLayout.INVISIBLE);
							m_optionMenuOpened = false;
							
							PopupLogoutDialog();
						}
					});
				}
				else{
					button_login.setBackgroundDrawable(getResources().getDrawable(R.drawable.option_icon_login));
					button_login.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							optionmenu.setVisibility(RelativeLayout.INVISIBLE);
							m_optionMenuOpened = false;
							
							PopupLoginDialog();
						}
					});
				}*/
				
				
				final LinearLayout linear_back = (LinearLayout)findViewById(R.id.BS_lil_back);
				linear_back.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						optionmenu.setVisibility(RelativeLayout.INVISIBLE);
						m_optionMenuOpened = false;
					}
				});
			}
			else{
				optionmenu.setVisibility(RelativeLayout.INVISIBLE);
				m_optionMenuOpened = false;
			}
    	} 
		else if (keyCode == KeyEvent.KEYCODE_BACK) { // 옵션 메뉴가 보이는 중에 뒤로 가기를 누르면 옵션 메뉴만 숨김			
			if (m_optionMenuOpened == true) {
				optionmenu.setVisibility(RelativeLayout.INVISIBLE);
				m_optionMenuOpened = false;
        		
        		return true;
    		}
    	}
		
		return super.onKeyDown(keyCode, event);
	}
	
	
	public void init()
	{
		m_bookid = new int[m_maxBookCount+1];
		m_bookid[0] = R.id.BS_image_book1;
		m_bookid[1] = R.id.BS_image_book2;
		m_bookid[2] = R.id.BS_image_book3;
		m_bookid[3] = R.id.BS_image_book4;
		m_bookid[4] = R.id.BS_image_book5;
		m_bookid[5] = R.id.BS_image_book6;
		m_bookid[6] = R.id.BS_image_book7;
		m_bookid[7] = R.id.BS_image_book8;
		m_bookid[8] = R.id.BS_image_book9;
		m_bookid[9] = R.id.BS_image_book10;
		m_bookid[10] = R.id.BS_image_book11;
		m_bookid[11] = R.id.BS_image_book12;
		m_bookid[12] = R.id.BS_image_book13;
		m_bookid[13] = R.id.BS_image_book14;
		m_bookid[14] = R.id.BS_image_book15;
		m_bookid[15] = R.id.BS_image_book16;
		m_bookid[16] = R.id.BS_image_book17;
		m_bookid[17] = R.id.BS_image_book18;
		m_bookid[18] = R.id.BS_image_book19;
		m_bookid[19] = R.id.BS_image_book20;
		m_bookid[20] = R.id.BS_image_book21;
		m_bookid[21] = R.id.BS_image_book22;
		m_bookid[22] = R.id.BS_image_book23;
		m_bookid[23] = R.id.BS_image_book24;
		m_bookid[24] = R.id.BS_image_book25;
		m_bookid[25] = R.id.BS_image_book26;
		m_bookid[26] = R.id.BS_image_book27;
		m_bookid[27] = R.id.BS_image_book28;
		m_bookid[28] = R.id.BS_image_book29;
		m_bookid[29] = R.id.BS_image_book30;
		m_bookid[30] = R.id.BS_image_book31;
		m_bookid[31] = R.id.BS_image_book32;
		m_bookid[32] = R.id.BS_image_book33;
		m_bookid[33] = R.id.BS_image_book34;
		m_bookid[34] = R.id.BS_image_book35;
		m_bookid[35] = R.id.BS_image_book36;
		m_bookid[36] = R.id.BS_image_book37;
		m_bookid[37] = R.id.BS_image_book38;
		m_bookid[38] = R.id.BS_image_book39;
		m_bookid[39] = R.id.BS_image_book40;
		m_bookid[40] = R.id.BS_image_book41;
		m_bookid[41] = R.id.BS_image_book42;
		m_bookid[42] = R.id.BS_image_book43;
		m_bookid[43] = R.id.BS_image_book44;
		m_bookid[44] = R.id.BS_image_book45;
		m_bookid[45] = R.id.BS_image_book46;
		m_bookid[46] = R.id.BS_image_book47;
		m_bookid[47] = R.id.BS_image_book48;
		m_bookid[48] = R.id.BS_image_book49;
		m_bookid[49] = R.id.BS_image_book50;
		
		m_shadowid = new int[m_maxBookCount+1];
		m_shadowid[0] = R.id.BS_rel_book1;
		m_shadowid[1] = R.id.BS_rel_book2;
		m_shadowid[2] = R.id.BS_rel_book3;
		m_shadowid[3] = R.id.BS_rel_book4;
		m_shadowid[4] = R.id.BS_rel_book5;
		m_shadowid[5] = R.id.BS_rel_book6;
		m_shadowid[6] = R.id.BS_rel_book7;
		m_shadowid[7] = R.id.BS_rel_book8;
		m_shadowid[8] = R.id.BS_rel_book9;
		m_shadowid[9] = R.id.BS_rel_book10;
		m_shadowid[10] = R.id.BS_rel_book11;
		m_shadowid[11] = R.id.BS_rel_book12;
		m_shadowid[12] = R.id.BS_rel_book13;
		m_shadowid[13] = R.id.BS_rel_book14;
		m_shadowid[14] = R.id.BS_rel_book15;
		m_shadowid[15] = R.id.BS_rel_book16;
		m_shadowid[16] = R.id.BS_rel_book17;
		m_shadowid[17] = R.id.BS_rel_book18;
		m_shadowid[18] = R.id.BS_rel_book19;
		m_shadowid[19] = R.id.BS_rel_book20;
		m_shadowid[20] = R.id.BS_rel_book21;
		m_shadowid[21] = R.id.BS_rel_book22;
		m_shadowid[22] = R.id.BS_rel_book23;
		m_shadowid[23] = R.id.BS_rel_book24;
		m_shadowid[24] = R.id.BS_rel_book25;
		m_shadowid[25] = R.id.BS_rel_book26;
		m_shadowid[26] = R.id.BS_rel_book27;
		m_shadowid[27] = R.id.BS_rel_book28;
		m_shadowid[28] = R.id.BS_rel_book29;
		m_shadowid[29] = R.id.BS_rel_book30;
		m_shadowid[30] = R.id.BS_rel_book31;
		m_shadowid[31] = R.id.BS_rel_book32;
		m_shadowid[32] = R.id.BS_rel_book33;
		m_shadowid[33] = R.id.BS_rel_book34;
		m_shadowid[34] = R.id.BS_rel_book35;
		m_shadowid[35] = R.id.BS_rel_book36;
		m_shadowid[36] = R.id.BS_rel_book37;
		m_shadowid[37] = R.id.BS_rel_book38;
		m_shadowid[38] = R.id.BS_rel_book39;
		m_shadowid[39] = R.id.BS_rel_book40;
		m_shadowid[40] = R.id.BS_rel_book41;
		m_shadowid[41] = R.id.BS_rel_book42;
		m_shadowid[42] = R.id.BS_rel_book43;
		m_shadowid[43] = R.id.BS_rel_book44;
		m_shadowid[44] = R.id.BS_rel_book45;
		m_shadowid[45] = R.id.BS_rel_book46;
		m_shadowid[46] = R.id.BS_rel_book47;
		m_shadowid[47] = R.id.BS_rel_book48;
		m_shadowid[48] = R.id.BS_rel_book49;
		m_shadowid[49] = R.id.BS_rel_book50;
		
		m_overlays = new int[m_maxBookCount+1];
		m_overlays[0] = R.id.BS_image_book1_overlay;
		m_overlays[1] = R.id.BS_image_book2_overlay;
		m_overlays[2] = R.id.BS_image_book3_overlay;
		m_overlays[3] = R.id.BS_image_book4_overlay;
		m_overlays[4] = R.id.BS_image_book5_overlay;
		m_overlays[5] = R.id.BS_image_book6_overlay;
		m_overlays[6] = R.id.BS_image_book7_overlay;
		m_overlays[7] = R.id.BS_image_book8_overlay;
		m_overlays[8] = R.id.BS_image_book9_overlay;
		m_overlays[9] = R.id.BS_image_book10_overlay;
		m_overlays[10] = R.id.BS_image_book11_overlay;
		m_overlays[11] = R.id.BS_image_book12_overlay;
		m_overlays[12] = R.id.BS_image_book13_overlay;
		m_overlays[13] = R.id.BS_image_book14_overlay;
		m_overlays[14] = R.id.BS_image_book15_overlay;
		m_overlays[15] = R.id.BS_image_book16_overlay;
		m_overlays[16] = R.id.BS_image_book17_overlay;
		m_overlays[17] = R.id.BS_image_book18_overlay;
		m_overlays[18] = R.id.BS_image_book19_overlay;
		m_overlays[19] = R.id.BS_image_book20_overlay;
		m_overlays[20] = R.id.BS_image_book21_overlay;
		m_overlays[21] = R.id.BS_image_book22_overlay;
		m_overlays[22] = R.id.BS_image_book23_overlay;
		m_overlays[23] = R.id.BS_image_book24_overlay;
		m_overlays[24] = R.id.BS_image_book25_overlay;
		m_overlays[25] = R.id.BS_image_book26_overlay;
		m_overlays[26] = R.id.BS_image_book27_overlay;
		m_overlays[27] = R.id.BS_image_book28_overlay;
		m_overlays[28] = R.id.BS_image_book29_overlay;
		m_overlays[29] = R.id.BS_image_book30_overlay;
		m_overlays[30] = R.id.BS_image_book31_overlay;
		m_overlays[31] = R.id.BS_image_book32_overlay;
		m_overlays[32] = R.id.BS_image_book33_overlay;
		m_overlays[33] = R.id.BS_image_book34_overlay;
		m_overlays[34] = R.id.BS_image_book35_overlay;
		m_overlays[35] = R.id.BS_image_book36_overlay;
		m_overlays[36] = R.id.BS_image_book37_overlay;
		m_overlays[37] = R.id.BS_image_book38_overlay;
		m_overlays[38] = R.id.BS_image_book39_overlay;
		m_overlays[39] = R.id.BS_image_book40_overlay;
		m_overlays[40] = R.id.BS_image_book41_overlay;
		m_overlays[41] = R.id.BS_image_book42_overlay;
		m_overlays[42] = R.id.BS_image_book43_overlay;
		m_overlays[43] = R.id.BS_image_book44_overlay;
		m_overlays[44] = R.id.BS_image_book45_overlay;
		m_overlays[45] = R.id.BS_image_book46_overlay;
		m_overlays[46] = R.id.BS_image_book47_overlay;
		m_overlays[47] = R.id.BS_image_book48_overlay;
		m_overlays[48] = R.id.BS_image_book49_overlay;
		m_overlays[49] = R.id.BS_image_book50_overlay;
		
		m_purid = new int[m_maxBookCount+1];
		m_purid[0] = R.id.BS_image_pur1;
		m_purid[1] = R.id.BS_image_pur2;
		m_purid[2] = R.id.BS_image_pur3;
		m_purid[3] = R.id.BS_image_pur4;
		m_purid[4] = R.id.BS_image_pur5;
		m_purid[5] = R.id.BS_image_pur6;
		m_purid[6] = R.id.BS_image_pur7;
		m_purid[7] = R.id.BS_image_pur8;
		m_purid[8] = R.id.BS_image_pur9;
		m_purid[9] = R.id.BS_image_pur10;
		m_purid[10] = R.id.BS_image_pur11;
		m_purid[11] = R.id.BS_image_pur12;
		m_purid[12] = R.id.BS_image_pur13;
		m_purid[13] = R.id.BS_image_pur14;
		m_purid[14] = R.id.BS_image_pur15;
		m_purid[15] = R.id.BS_image_pur16;
		m_purid[16] = R.id.BS_image_pur17;
		m_purid[17] = R.id.BS_image_pur18;
		m_purid[18] = R.id.BS_image_pur19;
		m_purid[19] = R.id.BS_image_pur20;
		m_purid[20] = R.id.BS_image_pur21;
		m_purid[21] = R.id.BS_image_pur22;
		m_purid[22] = R.id.BS_image_pur23;
		m_purid[23] = R.id.BS_image_pur24;
		m_purid[24] = R.id.BS_image_pur25;
		m_purid[25] = R.id.BS_image_pur26;
		m_purid[26] = R.id.BS_image_pur27;
		m_purid[27] = R.id.BS_image_pur28;
		m_purid[28] = R.id.BS_image_pur29;
		m_purid[29] = R.id.BS_image_pur30;
		m_purid[30] = R.id.BS_image_pur31;
		m_purid[31] = R.id.BS_image_pur32;
		m_purid[32] = R.id.BS_image_pur33;
		m_purid[33] = R.id.BS_image_pur34;
		m_purid[34] = R.id.BS_image_pur35;
		m_purid[35] = R.id.BS_image_pur36;
		m_purid[36] = R.id.BS_image_pur37;
		m_purid[37] = R.id.BS_image_pur38;
		m_purid[38] = R.id.BS_image_pur39;
		m_purid[39] = R.id.BS_image_pur40;
		m_purid[40] = R.id.BS_image_pur41;
		m_purid[41] = R.id.BS_image_pur42;
		m_purid[42] = R.id.BS_image_pur43;
		m_purid[43] = R.id.BS_image_pur44;
		m_purid[44] = R.id.BS_image_pur45;
		m_purid[45] = R.id.BS_image_pur46;
		m_purid[46] = R.id.BS_image_pur47;
		m_purid[47] = R.id.BS_image_pur48;
		m_purid[48] = R.id.BS_image_pur49;
		m_purid[49] = R.id.BS_image_pur50;
		
		
		//Log.i("debug", "init end");
		
	}
}