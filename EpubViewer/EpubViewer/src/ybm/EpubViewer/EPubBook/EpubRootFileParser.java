package ybm.EpubViewer.EPubBook;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.http.util.ByteArrayBuffer;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import ybm.EpubViewer.Logger;

public class EpubRootFileParser {
	String rootfile;
	public EpubRootFileParser(byte[] buf, EpubSpine spine, ArrayList<EpubItem> items)
    {
    	//�Ľ�
       try{
       	XmlPullParserFactory parserCreator = XmlPullParserFactory.newInstance();
       	XmlPullParser parser = parserCreator.newPullParser();
       	String szXml = new String(buf, "euc-kr");
       	parser.setInput(new StringReader(szXml));
       	int parserEvent = parser.getEventType();
        String tag;
  	   	while (parserEvent != XmlPullParser.END_DOCUMENT ){
       		  switch(parserEvent)
       		  {
                  case XmlPullParser.START_TAG:
                    tag = parser.getName();
                	
                	if (tag.compareToIgnoreCase("item") == 0)
                	{
                		EpubItem item = new EpubItem();
                		
                		item.itemId = parser.getAttributeValue(null, "id"); 
                		item.href = parser.getAttributeValue(null, "href"); 
                		item.media_type = parser.getAttributeValue(null, "media-type"); 
                		
                		items.add(item);
                	}

                	if (tag.compareToIgnoreCase("spine") == 0)
                	{
                		spine.toc = parser.getAttributeValue(null, "toc");
                	}
                	
                	if (tag.compareToIgnoreCase("itemref") == 0)
                	{
                		spine.AddItemRef(parser.getAttributeValue(null, "idref"));
                	}
                	 
               break;	        		
       		}
       		parserEvent = parser.next();
       	}
       }catch( Exception e ){
       	Logger.d("dd", "epub container parsing error");
       }
   }
}

