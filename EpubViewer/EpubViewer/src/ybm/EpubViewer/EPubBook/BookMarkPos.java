package ybm.EpubViewer.EPubBook;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ybm.EpubViewer.Util;

public class BookMarkPos extends TextInfo {
	public int day;
	public int year;
	public int month;
	public int hour;
	public int pageNo;
	
	void Save(OutputStream out)
	{
		super.Save(out);
		try {
			out.write(Util.intToByteArray(year));
			out.write(Util.intToByteArray(month));
			out.write(Util.intToByteArray(day));
			out.write(Util.intToByteArray(hour));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void Load(InputStream in)
	{
		super.Load(in);
		try {
			byte[] buffer = new byte[4];
			
			in.read(buffer, 0, 4);
			year = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			month = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			day = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			hour = Util.byteArrayToInt(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
}
