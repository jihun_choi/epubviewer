package ybm.EpubViewer.EPubBook;

public class EpubItem {
	String itemId;
	String href;
	String media_type;

	public Boolean isHtml()
	{
		if (media_type.indexOf("html") != -1)
			return true;
		if (media_type.indexOf("xml") != -1)
			return true;
		return false;
	}

}
