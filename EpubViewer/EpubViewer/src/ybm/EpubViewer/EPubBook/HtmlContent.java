package ybm.EpubViewer.EPubBook;

import java.util.ArrayList;
import java.util.HashMap;

import ybm.EpubViewer.Global;
import ybm.EpubViewer.Logger;
import ybm.EpubViewer.SearchView;
import android.graphics.Rect;
import android.util.Log;

public class HtmlContent {
	public int contentIdx;
	String itemId;
	String szHeadTitle;
	public ArrayList<Paragraph> paragraphs;
	public ArrayList<Page> Pages;
	public Rect PageRect;
	
	public String szBgColor;
	
	HashMap<String, Integer> idToPage; //단락에 있는 id Element와 실제 Page를 매치시켜놓는다.

	void SetBodyStyleCSS(HashMap<String, String> dic)
	{	
		szBgColor = dic.get("background-color");
	}
	
	void CalculatePage(Rect rect)
	{
		Logger.d("CalculatePage", ((Integer)contentIdx).toString());
		float width = (float)(rect.width() - Global.g_LeftMargin - Global.g_RightMargin);
		
		PageRect = rect;
	 	PageRect.left = (int)Global.g_LeftMargin;
		PageRect.right = (int) (PageRect.left + width);
		PageRect.bottom = (int) (PageRect.bottom - (Global.g_TopMargin + Global.g_BottomMargin));
		
	 	float curHeight = PageRect.top;
	 	
	 	Pages = new ArrayList<Page>(100);
	 	idToPage = new HashMap<String, Integer>();
	
		Page page = new Page();
		
		page.StartParaIdx = 0;
		page.StartLine = 0;
		page.bYmargin = true;
		
		int paraStartLine = 0;
		int size = paragraphs.size();

		for(int i = 0; i< size; i++)
		{
	
			Paragraph para = paragraphs.get(i);
			
			if (para.paraId != null)
			{
				idToPage.put(para.paraId, Pages.size());
			}
			
			if (para.NextPage) //다음 페이지 태그
			{
				Log.i("CALC_LINE", "NEXTPAGE");
				
				if (page.StartParaIdx == -1)
					page.StartParaIdx = i;
				
				page.EndParaIdx = i;
				page.EndLine = -2;
				page.Height = curHeight;
				page.bYmargin = false;
				
				Pages.add(page);
				
				//	NSLog(@"next page %d  height :%f",[Pages count], curHeight);
				
				//다음 페이지는 다음 단락
				page = new Page();
				page.StartParaIdx = i + 1;
				page.StartLine = 0;
				page.bYmargin = true;
				
				curHeight = PageRect.top;
				
				continue;
			}
			
	

			float height = para.CalculateLines(width);
			paraStartLine = 0;
	
			while (height > 0) //한 단락이 몇 페이지에 걸칠 수도 있다
			{
				
				if ((para.isImage() && (curHeight + height <= PageRect.height()))|| 
						(para.isImage() != true && 
								curHeight + height - Global.SpaceBetweenLines < PageRect.height())) // 본 단락이 한 페이지 안에 다 들어오는 경우. 그냥 끝낸다.
				{
					curHeight += height;
					
					break;
				}
				else // 한 페이지를 넘어가는 경우나 페이지에 딱 맞춰지는 경우
				{ 
					//이번 페이지에는 몇줄 까지 넣어야 하는 걸까?
					float linesHeights[] = new float[1]; //단락이 차지하는 높이
					int line = para.GetLineNoFrom(paraStartLine, PageRect.height() - curHeight, linesHeights); //남은 공간에 몇 라인이나 들어가나..
					float linesHeight = linesHeights[0];
					paraStartLine = line;
					
					//몇 줄까지 들어가나 계산
					//한줄도 안 들어가는 거라면 끝나는 단락 = 이전 단락
					//한줄은 들어가면 끝나는 단락 = 현재 단락

					
					//끝단락이 시작 단락 보다 작은 경우  : 끝단락 = 시작 단락
					boolean bImage = false;
					if (line == 0 || linesHeight == 0) //한줄도 안들어 가나?
					{
						page.EndParaIdx = i - 1; //그럼 끝단락은 이전단락이지//
						page.EndLine = -1;//끝까지 그리자
						bImage = para.isImage();//다음단락이 이미지였으면 마진을 주지 말자
					}
					else //한줄 이상들어가면
					{
						page.EndParaIdx = i;
						page.EndLine = line; //일단 주어진 라인까지... 
						height -= linesHeight; //단락의 높이에서 이용한 라인의 높이 만큼 빼준다
					}

					//	NSLog(@"curheight : %f linesHeight :%f",curHeight,linesHeight);
					
					//한줄도 안들어가는 경우
					if (page.EndParaIdx < page.StartParaIdx) 
						page.EndParaIdx = page.StartParaIdx;
					
					//마지막 단락이 이미지가 아니라면
					Paragraph endpara = paragraphs.get(page.EndParaIdx);
					if (endpara.paraId != null)
					{
						idToPage.put(endpara.paraId, Pages.size());
					}
					
					if (endpara.isImage() == false)
						linesHeight -= Global.SpaceBetweenLines;
					
					
					curHeight += linesHeight;
					page.Height = curHeight;
					if (bImage)
						page.bYmargin = false;
					Pages.add(page);
					//NSLog(@"page %d  height :%f",[Pages count], curHeight);
					// 새로운 페이지를 만듬.
					
					curHeight = PageRect.top;
					
					page = new Page();
				
					page.StartParaIdx = i;
					page.StartLine = line;
					page.bYmargin = true;
	
				}
				
			}
			
		}
		//마지막장 처리
		// ..>?
		page.EndParaIdx = paragraphs.size() - 1;
		page.EndLine = -1;
		page.bYmargin = false;
		
		//마지막 단락이 이미지가 아니라면
		if ((int)page.EndParaIdx > paragraphs.size())
		{
			Paragraph endpara = paragraphs.get(page.EndParaIdx);
			if (endpara.paraId != null)
				idToPage.put(endpara.paraId, Pages.size());

			if (endpara.isImage() == false)
				curHeight -= Global.SpaceBetweenLines;
		}
		
		page.Height = curHeight;
		
		Pages.add(page);
	}

	public Paragraph GetParagraphAtIndex(int index)
	{
		for(int i = 0 ; i < paragraphs.size(); i++)
		{
			Paragraph para = paragraphs.get(i);
			if (para.m_nIndex == index)
				return para	;	
		}
		return null;
	}

	public int PageAtId(String _itemId)
	{
		if (_itemId == null)
			return 0;
		
		return idToPage.get(_itemId);
	}

	int HasParagraph(Paragraph para)
	{
		for (int  i=0; i< paragraphs.size(); ++i)
		{
			Paragraph haspara = paragraphs.get(i);
			if (haspara == para)
				return i;
		}		
		return -1;
	}

	int PageOfParagraph(Paragraph para)
	{
		for (int  i=0; i<paragraphs.size(); ++i)
		{
			Paragraph haspara = paragraphs.get(i);
			if (haspara == para)
			{
				for (int j = 0; j < Pages.size(); ++j)
				{
					Page page = Pages.get(j);
					if (page.StartParaIdx <= i && i <= page.EndParaIdx)
						return j; 
				}
				break;
			}
		}	
		return -1;
	}
	
//SearchControllerForPopover class가 아직 안만들어짐 
	//public void search(String str, SearchView searchController)
	public void search(String str)
	{	
		 int curpage = 0;
		 for(int i = 0; i < paragraphs.size(); ++i)
		 { 
			 Paragraph para = paragraphs.get(i);
		 
			 if (para.NextPage == true ) 
				 continue;
			 
			 if (para.objType > 0)
				 continue;
			 
			 for (int l = 0; l < para.texts.size(); ++l)
			 {
				TextContent text = (TextContent)para.texts.get(l);
				
				for (int k = 0; k < text.content.length(); ++k)
				{
					if ( k + str.length() >= text.content.length())
						break;
		 
					String strings = text.content.substring(k, k + str.length());
		 
					if (strings.equalsIgnoreCase(str) == true)
					{	
						// expanededLength -> expanededEnd
						int expanededEnd = k + str.length() + 10;
						if ( expanededEnd > text.content.length())
							expanededEnd = text.content.length();						
						
						int lineIdx = para.GetLineFromTextIdx(l, k);
						int pageIdx = -1;
						
						while (curpage < Pages.size())
						{							
							Page page = Pages.get(curpage);
							
							if ( page.StartParaIdx == i && page.StartLine <= lineIdx 
									|| page.StartParaIdx < i )
							{
								if ( page.EndParaIdx == i && (page.EndLine < 0 || 
										page.EndLine > lineIdx)  || page.EndParaIdx > i )
								{	
									pageIdx = curpage;
									break;
								}
							}
							++curpage;
							
						}
						
						SearchedText st = new SearchedText();
						st.TextIdx = l;
						for( int ll = k; ll < k + str.length() ; ++ll)
						{
							st.CharIdx = ll;
							para.AddSearchedText(st);
						}
						
						SearchResult ret = new SearchResult();
						ret.content = this;
						ret.resStr = text.content.substring(k, expanededEnd);
						
						ret.pageNoOfContent = pageIdx;
						SearchView.m_Result.add(ret);
						
						//[[searchController m_Result] addObject:[NSData dataWithBytes:&ret length:sizeof(SearchResult)]];
					}
					
				}
				 
			 }
		 }
	}	
}




