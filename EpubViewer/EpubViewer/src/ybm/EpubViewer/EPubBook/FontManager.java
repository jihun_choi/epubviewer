package ybm.EpubViewer.EPubBook;

import java.util.ArrayList;
import java.util.HashMap;

import ybm.EpubViewer.Global;
import ybm.EpubViewer.Util;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.Log;

public class FontManager {

	ArrayList<FontContext> m_Fonts;// 실제로 조합된 폰트인 FontContext의 List
	ArrayList<String> m_FontStack;
	ArrayList<Integer> m_SizeStack;
	FontNode m_pFontSizeNode;
	HashMap<Integer, Paint> m_Paints;
	ArrayList<HashMap<String, Size>> m_cachedCharSizes;

	public FontManager() {
		m_Fonts = new ArrayList<FontContext>(50);
		m_FontStack = new ArrayList<String>();
		m_SizeStack = new ArrayList<Integer>();
		m_Paints = new HashMap<Integer, Paint>(50);
		m_cachedCharSizes = new ArrayList<HashMap<String, Size>>(50);
		
		FontQue("ArialMT");
		SizeQue(12);

		GetCurrentFontId();

		m_pFontSizeNode = null;
	}

	int GetFontId(FontContext font) {
		int count = -1;
		for (count = m_Fonts.size() - 1; count >= 0; --count) {
			FontContext pFont = m_Fonts.get(count);
			if (pFont.font.compareToIgnoreCase(font.font) == 0
					&& pFont.size == font.size)
				break;
		}
		return count;// 없으면 -1이 출력 되나?
	}

	int GetCurrentFontId() {
		return AddFont(m_FontStack.get(m_FontStack.size() - 1),
				m_SizeStack.get(m_SizeStack.size() - 1));
	}

	int AddFont(String font, int size) {
		FontContext fontcontext = new FontContext();
		fontcontext.font = font;
		fontcontext.size = size;
		fontcontext.temp_size = size;

		fontcontext.bold = false; // BOLD는 지원하지 않음!

		int fontid = GetFontId(fontcontext);
		if (fontid == -1) {
			m_Fonts.add(fontcontext);
			fontid = m_Fonts.size() - 1;
		}

		return fontid;
	}

	void SetStyleCSS(HashMap<String, String> css) {
		String szSize = css.get("font-size");
		int newsize = 12;

		if (szSize != null) {
			if (szSize.endsWith("em"))
				newsize = (int) (Util.Float2(szSize) * 12);
			else if (szSize.endsWith("px"))
				newsize = (int) Util.Float2(szSize);
			else if (szSize.endsWith("pt"))
				newsize = (int) Util.Float2(szSize);
			else if (szSize.endsWith("%"))
				newsize = (int) (Util.Float2(szSize) / 100 * 12);
			else if (szSize.indexOf("large") != -1)
				newsize = 20;
			else if (szSize.indexOf("normal") != -1
					|| szSize.indexOf("medium") != -1)
				newsize = 12;
			else if (szSize.indexOf("small") != -1)
				newsize = 10;
			SizeQue(newsize);
		}
	}

	void SizeQue(int size) {
		m_SizeStack.add(size);
	}

	void FontQue(String font) {
		m_FontStack.add(font);
	}

	void SizeDeque() {
		if (m_SizeStack.size() == 1)
			return;
		m_SizeStack.remove(m_SizeStack.size() - 1);
	}

	void FontDeque() {
		if (m_FontStack.size() == 1)
			return;
		m_FontStack.remove(m_FontStack.size() - 1);
	}

	public void UpdateFontList() {
		if (m_pFontSizeNode != null)
			ReleaseNode(m_pFontSizeNode);
		m_pFontSizeNode = null;

		for (int i = 0; i < m_Fonts.size(); ++i) {
			FontContext font = m_Fonts.get(i);
			if (Global.g_bSameFontSize == true) {
				font.temp_size = Global.g_FontSize;
			} else {
				font.temp_size = (font.size * Global.g_FontRate) / 100;
			}
		}
	}

	FontContext GetFont(int idx) {
		if (idx >= m_Fonts.size()) {
			// NSLog(@"GetFont : Range Error %d/%d", idx, m_Fonts.size());
			idx = m_Fonts.size() - 1;
		}

		if (idx < 0) {
			// NSLog(@"GetFont : Range Error %d/%d", idx, m_Fonts.size());
			idx = 0;
		}
		return m_Fonts.get(idx);
	}
	
	Paint GetPaint(int idx)
	{
		Paint paint = m_Paints.get(idx);
		if (paint != null)
			return paint;
		
		FontContext font = GetFont(idx);
		String fontType;// = Global.g_FontName;
		if (Global.g_FontFace == null)
			fontType = "ArialMT";
		else 
			fontType = Global.g_FontFace;
		
		paint = new Paint();
		Typeface typeface = Typeface.create(fontType, Typeface.NORMAL);
		paint.setAntiAlias(true);
		//paint.setTextSize(font.temp_size);
		paint.setTextSize((font.size * Global.g_FontRate) / 100); // 업데이트 되기 전에 호출 되면 안되므로 이렇게 직접 계산
		paint.setTypeface(typeface);
		
		m_Paints.put(idx, paint);
		return paint;
	}
	
	public void ClearCache() {
		m_Paints = new HashMap<Integer, Paint>(50);
		m_cachedCharSizes = new ArrayList<HashMap<String, Size>>(50);
	}

	// 슈퍼 슈퍼 느림
	TextPaint m_textPaint = new TextPaint();
	
	ybm.EpubViewer.EPubBook.Size GetCharSize(char _oneChar, int fontId) {
		// 캐시 배열을 fontID 만큼 늘림 (이 캐시는 g_FontRate가 바뀔 때마다 리셋해야 함!)
		if (m_cachedCharSizes.size() <= fontId) {
			for (int i = m_cachedCharSizes.size(); i <= fontId; i++)
				m_cachedCharSizes.add(new HashMap<String, Size>());
		}
		
		// 계산할 글자
		String text = "";
		text += _oneChar;
		
		//Log.i("debug", "getcharsize " + _oneChar);
		
		// 줄 간격 조정시에 전부 다시 계산을 피하기 위해 줄 간격은 여기에서 계산
		if (m_cachedCharSizes.get(fontId).containsKey(text)) {
			int fontSize = (m_Fonts.get(fontId).size * Global.g_FontRate) / 100;
			Size size = m_cachedCharSizes.get(fontId).get(text);
			Size lineHeightedSize = new Size(size.width, (int)Math.ceil(fontSize * Global.g_LineHeight));
			return lineHeightedSize;
		}
		
		FontContext font = m_Fonts.get(fontId);
		String fontType;// = Global.g_FontName;
		if (Global.g_FontFace == null)
			fontType = "ArialMT";
		else 
			fontType = Global.g_FontFace;
		
		Typeface typeface = Typeface.create(fontType, Typeface.NORMAL);
		int fontSize = (font.size * Global.g_FontRate) / 100;
		m_textPaint.setTextSize(fontSize); // 업데이트 되기 전에 호출 되면 안되므로 이렇게 직접 계산
		m_textPaint.setTypeface(typeface);
		
		int width = (int)Math.ceil(m_textPaint.measureText(text));
		Size size = new Size(width, fontSize);
		
		m_cachedCharSizes.get(fontId).put(text, size);
		
		return new Size(size.width, (int)Math.ceil(fontSize * Global.g_LineHeight));
	}
	
	int GetCharOnlyHeight(char _oneChar, int fontId) {
		FontContext font = m_Fonts.get(fontId);
		String fontType;// = Global.g_FontName;
		if (Global.g_FontFace == null)
			fontType = "ArialMT";
		else 
			fontType = Global.g_FontFace;
		
		Typeface typeface = Typeface.create(fontType, Typeface.NORMAL);
		int fontSize = (font.size * Global.g_FontRate) / 100;
		m_textPaint.setTextSize(fontSize); // 업데이트 되기 전에 호출 되면 안되므로 이렇게 직접 계산
		m_textPaint.setTypeface(typeface);
		Rect rt = new Rect();
		m_textPaint.getTextBounds(_oneChar + "", 0, 1, rt);
		
		return rt.height();
	}
	
	// 임시로.. 만듬.
	/*
	int getStrSize(String str, int fontId)
	{
		FontContext font = m_Fonts.get(fontId);
		String fontType;// = Global.g_FontName;
		if (Global.g_FontFace == null)
			fontType = "ArialMT";
		else 
			fontType = Global.g_FontFace;
		
		Typeface typeface = Typeface.create(fontType, Typeface.NORMAL);
		m_textPaint.setTextSize(font.temp_size);
		m_textPaint.setTypeface(typeface);
		
		Rect bounds = new Rect();
		m_textPaint.getTextBounds(str, 0, str.length(), bounds);
		
		return bounds.width();
	}*/
	
	/*
		Size[] sizePerFontId = null;

		if (m_pFontSizeNode == null) {
			sizePerFontId = new Size[m_Fonts.size()];
			for (int i = 0; i < m_Fonts.size(); ++i)
			{
				sizePerFontId[i] = new Size(-1, -1);			
			}
			m_pFontSizeNode = CreateEmptyNode(_oneChar, sizePerFontId);
		} else
			sizePerFontId = GetSizeFromNode(m_pFontSizeNode, _oneChar);

	
		
		if (sizePerFontId == null) // 처음 들어온 문자이면
		{
			// 폰트용 구조체를 하나 만들고
			sizePerFontId = new Size[m_Fonts.size()];
			for (int i = 0; i < m_Fonts.size(); ++i) {
				sizePerFontId[i] = new Size(-1, 0);
			}
			// 현재 폰트에 대한 사이즈를 구해서
			FontContext fontcontext = GetFont(fontId);

			Paint paint = new Paint();
			Typeface typeface = Typeface.create(fontcontext.font,
					Typeface.NORMAL);
			paint.setAntiAlias(true);
			paint.setTextSize(fontcontext.temp_size);
			paint.setTypeface(typeface);

			String oneChar = String.format("%c", _oneChar);
			sizePerFontId[fontId].width = (int) paint.measureText(oneChar);
			sizePerFontId[fontId].height = (int) (paint.ascent() + paint
					.descent());
			// Dictionary에 넣자
			InsertNode(m_pFontSizeNode, _oneChar, sizePerFontId);
		} else if (sizePerFontId[fontId].width == -1) 
		{
			FontContext fontcontext = GetFont(fontId);
			Paint paint = new Paint();
			Typeface typeface = Typeface.create(fontcontext.font,
					Typeface.NORMAL);
			paint.setAntiAlias(true);
			paint.setTextSize(fontcontext.temp_size);
			paint.setTypeface(typeface);
			
			String oneChar = String.format("%c", _oneChar);
			sizePerFontId[fontId].width = (int) paint.measureText(oneChar);
			sizePerFontId[fontId].height = (int) (paint.ascent() + paint.descent());
		}

		return sizePerFontId[fontId];
	}*/

	// /////////////////////Font Binary Tree/////////////////////
	FontNode CreateEmptyNode(char key, Size sizes[]) {
		FontNode node = new FontNode();
		node.key = key;
		node.sizes = sizes;
		node.left = null;
		node.right = null;
		return node;
	}

	boolean InsertNode(FontNode pNode, char key, Size sizes[]) {
		for (;;) {
			if (key < pNode.key )
			{
				if (pNode.left == null) {
					pNode.left = CreateEmptyNode(key, sizes);
					return true;
				}
				pNode = pNode.left;
				continue;
			}
			else if (pNode.key < key) 
			{
				if (pNode.right == null)
				{
					pNode.right = CreateEmptyNode(key, sizes);
					return true;
				}
				pNode = pNode.right;
				continue;
			}
			return false;
		}
	}

	Size[] GetSizeFromNode(FontNode pNode, char key) {
		for (;;) {
			if (key == pNode.key) {
				return pNode.sizes;
			} else if (key < pNode.key) {
				if (pNode.left == null)
					return null;
				pNode = pNode.left;
			} else if (pNode.key < key) {
				if (pNode.right == null)
					return null;
				pNode = pNode.right;
			}
		}
	}

	void ReleaseNode(FontNode pNode) {
		if (pNode == null)
			return;
		if (pNode.left != null)
			ReleaseNode(pNode.left);
		if (pNode.right != null)
			ReleaseNode(pNode.right);
		pNode = null;
	}

}
