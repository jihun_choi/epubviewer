package ybm.EpubViewer.EPubBook;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.GregorianCalendar;

import android.util.Log;


import ybm.EpubViewer.Global;
import ybm.EpubViewer.Util;

public class MemoInfo {
	public SelectedParagraphInfo start = null;
	public SelectedParagraphInfo end = null;
	public int type; //0:�޸� 1:�����޸� 2:������ 
	public int day;
	public int year;
	public int month;
	public int hour;
	
	public String content = null;
	
	void Save(OutputStream out)
	{
		start.Save(out);
		end.Save(out);
		try {
			out.write(Util.intToByteArray(year));
			out.write(Util.intToByteArray(month));
			out.write(Util.intToByteArray(day));
			out.write(Util.intToByteArray(hour));
			out.write(Util.intToByteArray(type));
			int len = (content != null) ? content.getBytes().length : 0;
			out.write(Util.intToByteArray(len));
			if (len > 0)
			{
				//Log.i("MEMO", "write len=" + len);
				//Log.i("MEMO", content);
				
				out.write(content.getBytes());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void Load(InputStream in)
	{
		start = new SelectedParagraphInfo();
		end = new SelectedParagraphInfo();
		start.Load(in);
		end.Load(in);
	
		int len;
		try {
			byte[] buffer = new byte[4];
			
			in.read(buffer, 0, 4);
			year = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			month = Util.byteArrayToInt(buffer);			
			in.read(buffer, 0, 4);
			day = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			hour = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			type = Util.byteArrayToInt(buffer);
			in.read(buffer, 0, 4);
			len = Util.byteArrayToInt(buffer);
			if (len > 0)
			{
				//Log.i("MEMO", "read len=" + len);
				
				byte[] buf = new byte[len + 2];
				in.read(buf, 0, len);
				buf[len] = 0;
				content = new String(buf);
				
				//Log.i("MEMO", content);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	void setMemo(String memo)
	{
		if (memo != null)
			content = new String(memo);
	}

	public void set(int _type, SelectedParagraphInfo _start, SelectedParagraphInfo _end, String str)
	{
		type = _type;
		start = new SelectedParagraphInfo(_start);
		end = new SelectedParagraphInfo(_end);
	
		if (str != null)
			content = new String(str);
	
		GregorianCalendar today = new GregorianCalendar ( );

		year = today.get ( today.YEAR );
		month = today.get ( today.MONTH ) + 1;
		hour = today.get ( today.HOUR );
		day = today.get ( today.DATE );
	}

	Boolean IsInMemo(MemoInfo memo)
	{
		int startRet = memo.start.compare(start);
		int endRet = memo.end.compare(end);
		
		if (startRet == Global.NSOrderedAscending || startRet == Global.NSOrderedSame)
			if (endRet == Global.NSOrderedDescending || endRet == Global.NSOrderedSame)
				return true;
		return false;
	}

}
