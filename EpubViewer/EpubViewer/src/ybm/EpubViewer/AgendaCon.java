package ybm.EpubViewer;

import java.util.Vector;

import ybm.EpubViewer.EPubBook.EpubNavPoint;
import ybm.EpubViewer.EPubBook.HtmlContent;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AgendaCon extends Activity {

	TableLayout m_table;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    setContentView(R.layout.agenda);	    
	    
	    m_table = (TableLayout)findViewById(R.id.AG_table);
	    m_table.setStretchAllColumns(true);
	    
	    final Button button_back = (Button)findViewById(R.id.AG_button_back);
	    button_back.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					button_back.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_kreturn_b));
				}
				else{
					button_back.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_kreturn_a));
					finish();
				}
				return false;
			}
		});
	    
	    final Button button_bookmark = (Button)findViewById(R.id.AG_button_bookmark);
	    button_bookmark.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent i = new Intent(AgendaCon.this, AgendaBookmark.class);
				startActivity(i);
				overridePendingTransition(R.anim.noanim, R.anim.noanim);
				finish();
			}
		});

	    InsertToc();
	}
	
	void InsertToc()
	{
		Vector<EpubNavPoint> navs = Global.g_CurEpubBook.Toc.navPoints;		
		
		for(int i=0;i<=navs.size()-1;i++){
			EpubNavPoint nav = navs.get(i);
			HtmlContent content = Global.g_CurEpubBook.GetHtmlContentOfHref(nav.srcFile);
			int startpage = Global.g_CurEpubBook.GetStartPageOfContent(content);
			int page = startpage + content.PageAtId(nav.srcAnchor);
			addTableRow(nav.label, page + "", page);
		}
	}
	
	void addTableRow(String text, String tag, final int page)
	{
	    RelativeLayout rel = new RelativeLayout(this);
	    RelativeLayout.LayoutParams params;
	    
	    TextView word = new TextView(this);
	    // word.setText(text);
	    word.setTextSize(17);
	    word.setTextColor(Color.BLACK);
	    
	    if(text.length() > 30){
		    int length = text.length();
		    String text1, text2 = text;
		    text = "";
		    while(length > 30){
		    	int idx = text2.indexOf(' ');
		    	int idx_next = text2.indexOf(' ', idx+1);
		    	while(idx_next < 30){
		    		if( idx_next == -1 ) break;
		    		idx = idx_next;
		    		idx_next = text2.indexOf(' ', idx+1);
		    	}
		    	
	    		text1 = text2.substring(0, idx);
	    		text2 = text2.substring(idx, text2.length());
	    		if(text == "")
	    			text = text1;
	    		else
	    			text = text + "\n" + text1;    		
	    		
	    		length = text2.length();
		    }
		    
		    if(text2.length() > 0)
		    	text = text + "\n" + text2;
	    }
	    
	    word.setText(text);
	    
	    
	    params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
	    
	    rel.addView(word, params);
	    
	    TextView pos = new TextView(this);
	    pos.setText(tag);
	    pos.setTextSize(17);
	    pos.setTextColor(Color.BLUE);
	    
	    params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

	    rel.addView(pos, params);

	    TableRow tr = new TableRow(this);
	    
	    tr.addView(rel, new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
	    
	    m_table.addView(tr, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
	    
	    // 클릭시 이동?
	    rel.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Global.g_CurEpubBook.curPage = page;
				
				if(Global.g_bShowTwoPages == true){
					if(Global.g_CurEpubBook.curPage % 2 > 0)
						Global.g_CurEpubBook.curPage--;
				}
				
				//Intent i = new Intent(AgendaCon.this, OnePageViewer.class);
				//startActivity(i);
				finish();
			}
		});
	}
}
