package ybm.EpubViewer.EPubBook;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.util.ByteArrayBuffer;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import ybm.EpubViewer.Global;
import ybm.EpubViewer.Logger;
import ybm.EpubViewer.Util;

public class EpubHtmlFileParser extends DefaultHandler {

	String rootfolder;
	String fullpath;// 현재 여는 파일의 fullpath
	HashMap<String, HashMap<String, String>> stylecss;
	HtmlContent html = null;

	TextContent text = new TextContent();

	String curTag;
	Paragraph _paragraph = null;
	ArrayList<Paragraph> paragraphs = null;

	// 이전 style보관
	ArrayList<HashMap<String, String>> cssQue;
	ArrayList<String> HasCssQue;
	ArrayList<String> colorQue;
	ArrayList<String> bgcolorQue;
	ArrayList<String> alignQue;

	boolean isInHeadTag;

	StringBuilder styleInHtml;// html head에 있는 style의 내용
	StringBuilder textContext = null;// html head에 있는 style의 내용
	ArrayList<String> tagTree;// body -> p ->span ...etc

	SAXParserFactory factory;
	SAXParser parser;
	XMLReader reader;
	
	String szFullFolder = null;
	
	static long timeFirstFiltering = 0;
	static long time1 = 0;
	static long time2 = 0;
	static long time3 = 0;

	static long timespan = 0;
	static long timep = 0;
	static long timestyle = 0;

	public EpubHtmlFileParser()
	{
		textContext = new StringBuilder(1000);
		cssQue = new ArrayList<HashMap<String, String>>(5);
		colorQue = new ArrayList<String>(5);
		bgcolorQue = new ArrayList<String>(5);
		HasCssQue = new ArrayList<String>(5);
		alignQue = new ArrayList<String>(5);
		tagTree = new ArrayList<String>(5);
		styleInHtml = new StringBuilder(1000);		

		_paragraph = null;
		text.szTextcolor = null;
		isInHeadTag = false;

		factory = SAXParserFactory.newInstance();
		try 
		{
			parser = factory.newSAXParser();
			reader = parser.getXMLReader();
			reader.setContentHandler(this);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			Logger.d("error", " HtmlFileParser error!!!!!");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			Logger.d("error", " HtmlFileParser error!!!!!");
		}
	
	}

	private void clear()
	{
		cssQue.clear();
		colorQue.clear();
		bgcolorQue.clear();
		HasCssQue.clear();
		alignQue.clear(); 
		tagTree.clear();
		if (styleInHtml.length() > 0)
			styleInHtml.delete(0, styleInHtml.length() - 1);
		if (textContext.length() > 0)
			textContext.delete(0, textContext.length() - 1);
	
		_paragraph = null;
		text.szTextcolor = null;
		isInHeadTag = false;
	}
	
	public HtmlContent Parser(byte[] buf, String itemId) {
		// 새로운 html parsing을 위한 설정
		html = new HtmlContent();
		paragraphs = new ArrayList<Paragraph>(20);
		
		////////////////////////////////
		colorQue.add("black");
		bgcolorQue.add("white");
		alignQue.add("left");

		try {
			InputStream istream = new ByteArrayInputStream(buf);// szXml.getBytes());			
			reader.parse(new InputSource(istream));
		} catch (SAXException e) {
			Logger.d("error", " HtmlFileParser error");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.d("error", " HtmlFileParser error");
		}

		int size = paragraphs.size();
		for(int idx = 0; idx < size; idx++)
		{
			Paragraph para = paragraphs.get(idx);
			para.parent = html;
			para.m_nIndex = idx;
		}
		html.paragraphs = paragraphs;
		html.itemId = itemId;
		
		clear();
		// //////////////////////////////

		return html;
	}

	String GetFullFolder() 
	{
		if (szFullFolder != null)
			return szFullFolder;
	
		fullpath.replace("\\", "/");
		String subs[] = fullpath.split("/");
		StringBuilder ret = new StringBuilder(100);
		for (int i = 0; i < subs.length - 1; ++i) {
			ret.append(subs[i]).append("/");
		}
		if (szFullFolder == null)
			szFullFolder = ret.toString();
		
		return ret.toString();
	}

	void ReleaseText(TextContent text1) {
		text1.content = null;
		text1.szTextcolor = null;
	}

	void applyStyle(HashMap<String, String> css, String style) 
	{
		// 만일 style class가 있는데 내부에 또 style정의가 있으면 css를 바꿔준다..
		HashMap<String, String> css2 = null;
		
		// 원본이 수정되는 것을 막기 위해 일단 복사를 하고 복사본을 수정한다
		if (style != null)
		{
			css2 = new HashMap<String, String>(10);;
			if (css != null)
				css2.putAll(css);
			styleModify(css2, style);
		}

		// NSLog(@"1 css : %d",[css count]);
		Global.g_FontMgr.SetStyleCSS(css2 != null ? css2 : css);
		SetStyleCSS(css2 != null ? css2 : css);
		cssQue.add(css2 != null ? css2 : css);
		HasCssQue.add("Y");
		css2 = null;
		
	}

	void styleModify(HashMap<String, String> css2, String style) 
	{
		style = style.replace(" ", "");
		int p1 = 0, p2 = 0, p3 = 0, p4 = 0;

		int start = 1;
		while (start > 0 && p1 < style.length()) {
			start = style.indexOf(':', start) + 1;
			if (start == 0)
				break;
			p2 = start - 1;
			p3 = start;
			start = style.indexOf(';', start) + 1;
			p4 = start - 1;
			if (start == 0)
				break;
			String key = style.substring(p1, p2);
			String val = style.substring(p3, p4);

			if (css2.get(key) != null)
				css2.remove(key);
			css2.put(key, val);

			p1 = start;
		}
	}

	void CssDeque() {
		if (cssQue.size() == 0)
			return;
		String has = HasCssQue.get(HasCssQue.size() - 1);
		if (has.contentEquals("Y") == true) {
			HashMap<String, String> css = cssQue.remove(cssQue.size() - 1);
			if (css != null) 
			{
				String szColor = css.get("color");
				String szBgColor = css.get("background-color");
				if (szColor != null) 
				{
					colorQue.remove(colorQue.size() - 1);
					if (colorQue.size() > 0) {
						text.szTextcolor = szColor;
					} else
						text.szTextcolor = null;
				}
				if (szBgColor != null) {
					bgcolorQue.remove(bgcolorQue.size() - 1);
					if (_paragraph != null) {
						if (bgcolorQue.size() > 0) {
							_paragraph.szBgColor = szBgColor;
						} else
							_paragraph.szBgColor = null;
					}
				}
				if (css.get("text-align") != null) {
					alignQue.remove(alignQue.size() - 1);
					String align = alignQue.get(alignQue.size() - 1);
					// NSLog(align);

					if (align != null && _paragraph != null) {
						if (align.compareTo("center") == 0) {
							_paragraph.alignment = Global.UITextAlignmentCenter;
						} else if (align.compareTo("left") == 0) {
							_paragraph.alignment = Global.UITextAlignmentLeft;
						}
						if (align.compareTo("right") == 0) {
							_paragraph.alignment = Global.UITextAlignmentRight;
						} else if (align.compareTo("justify") == 0)
							_paragraph.alignment = 30;
					}
				}
			}
		}
		HasCssQue.remove(HasCssQue.size() - 1);
	}

	void SetStyleCSS(HashMap<String, String> css) {
		String szColor = css.get("color");
		String szBgColor = css.get("background-color");
		if (szColor != null) {
			colorQue.add(szColor);
			text.szTextcolor = szColor;
		}
		if (szBgColor != null) {
			bgcolorQue.add(szBgColor);
			_paragraph.szBgColor = szBgColor;
		}

		String align = css.get("text-align");
		if (align != null) {
			if (align.compareTo("center") == 0) {
				_paragraph.alignment = Global.UITextAlignmentCenter;
			} else if (align.compareTo("left") == 0) {
				_paragraph.alignment = Global.UITextAlignmentLeft;
			} else if (align.compareTo("right") == 0) {
				_paragraph.alignment = Global.UITextAlignmentRight;
			} else if (align.compareTo("justify") == 0)
				_paragraph.alignment = 30;

			alignQue.add(align);
		}

		float textsize = 1;
		String szSize = css.get("font-size");
		if (szSize != null) {
			if (szSize.indexOf("em") != -1)
				textsize = Util.Float2(szSize);
			else if (szSize.indexOf("px") != -1)
				textsize = Util.Float2(szSize);
			else if (szSize.indexOf("pt") != -1)
				textsize = Util.Float2(szSize);  
			else if (szSize.indexOf("%") != -1)
				textsize =  Util.Float2(szSize)/100;//Float.parseFloat(szSize.replace("%", "")) / 100;
			else if (szSize.indexOf("large") != -1)
				textsize = 20;
			else if (szSize.indexOf("normal") != -1
					|| szSize.indexOf("medium") != -1)
				textsize = 12;
			else if (szSize.indexOf("small") != -1)
				textsize = 10;
		}

		String bottomMargin = css.get("margin-bottom");
		if (bottomMargin != null) {
			float size = Util.Float2(bottomMargin);
			if (bottomMargin.indexOf("em") != -1)
				size *= textsize;
/*			if (bottomMargin.indexOf("em") != -1)
				size = Util.Float2(bottomMargin) * textsize;
			else if (bottomMargin.indexOf("px") != -1)
				size = Util.Float2(bottomMargin);
			else if (bottomMargin.indexOf("pt") != -1)
				size = Util.Float2(bottomMargin);
*/
			_paragraph.bottomMargin = size;

		}
		String topMargin = css.get("margin-top");
		if (topMargin != null) {
			float size = Util.Float2(topMargin);
			if (topMargin.indexOf("em") != -1)
				size *= textsize;
			/*if (topMargin.indexOf("em") != -1)
				size = Float.parseFloat(topMargin.replace("em", "")) * textsize;
			else if (topMargin.indexOf("px") != -1)
				size = Float.parseFloat(topMargin.replace("px", ""));
			else if (topMargin.indexOf("pt") != -1)
				size = Float.parseFloat(topMargin.replace("pt", ""));
			 */
			_paragraph.topMargin = size;

		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		
		String buf = new String(ch);
		buf = buf.substring(start, start + length); // 실제 데이터 영역
	
		if (curTag != null && curTag.equalsIgnoreCase("style")) {
			styleInHtml.append(buf);
			return;
		}

		// Logger.d("html-char", ((Integer)textContext.length()).toString());

		if (_paragraph == null) {
			if (ch[start] != '\n' && ch[start] == '\r') {
				text.szTextcolor = "black";
				if (isInHeadTag) {
					text.fontIdx = Global.g_FontMgr.GetCurrentFontId();
					textContext.append(buf);
				} 
				else 
				{
					_paragraph = new Paragraph();
		
					text.fontIdx = -1;
					_paragraph.indentSpaceCount = 0;
				}
			}
		}

		if (_paragraph != null) {
			if (text.fontIdx == -1)
				text.fontIdx = Global.g_FontMgr.GetCurrentFontId();

			textContext.append(buf);// string;
		}

	}

	@Override
	public void endElement(String uri, String localName, String name)
			throws SAXException {
		super.endElement(uri, localName, name);


		tagTree.remove(tagTree.size() - 1);
		curTag = null;

		// Logger.d("html-end", localName);
		if (localName.equalsIgnoreCase("p") || localName.equalsIgnoreCase("h1")
				|| localName.equalsIgnoreCase("h2")
				|| localName.equalsIgnoreCase("h3")) {
			if (_paragraph != null) {
				if (text.fontIdx == -1)
					text.fontIdx = 0;

				_paragraph.AddText(text, textContext);
				textContext.delete(0, textContext.length());// = new StringBuilder(textContext.length());
				text.szTextcolor = null;

				paragraphs.add(_paragraph);
				_paragraph = null;
			}
			CssDeque();
		}
		if (localName.equalsIgnoreCase("span")) {
			String tag = tagTree.get(tagTree.size() - 1);
			if (_paragraph != null) {
				if (text.fontIdx == -1)
					text.fontIdx = 0;

				_paragraph.AddText(text, textContext);
				textContext.delete(0, textContext.length());//new StringBuilder(textContext.length());
				text.szTextcolor = null;
				// text.content = "";
				if (tag.equalsIgnoreCase("p") == false) {
					paragraphs.add(_paragraph);
					_paragraph = null;
				} else {
					// text.content = "";
					text.fontIdx = -1;
					text.szTextcolor = "black";
				}
			}
			CssDeque();
			;
		}
		if (localName.equalsIgnoreCase("head")) {
			isInHeadTag = false;
			if (textContext.length() > 0) {
				html.szHeadTitle = textContext.toString();
				textContext.delete(0, textContext.length());// = new StringBuilder(1000);
				text.szTextcolor = null;
			}
		}

		if (localName.equalsIgnoreCase("style")) {
			// NSLog(styleInHtml);
			if (stylecss != null)
				stylecss.putAll(Global.g_CurEpubBook
						.GetStyleCSSFromString(styleInHtml));
			else
				stylecss = Global.g_CurEpubBook
						.GetStyleCSSFromString(styleInHtml);

			styleInHtml.delete(0, styleInHtml.length());// = new StringBuilder(styleInHtml.length());
		}
	
	}

	@Override
	public void startElement(String uri, String localName, String name,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, name, attributes);
	
		// Logger.d("html-start", localName);
		tagTree.add(localName);
		curTag = localName;

		// css 파일을 읽는다
		if (localName.equalsIgnoreCase("href")) {
			if (_paragraph != null)
				_paragraph.linkHtml = attributes.getValue("id");
		} else if (localName.equalsIgnoreCase("link")) {
			String type = attributes.getValue("type");
			if (type != null) {
				if (type.indexOf("text/css") != -1) {
					String styleFileName = attributes.getValue("href");
					String cssPath = Util.CheckFolderValidate(GetFullFolder()
							+ styleFileName);
					stylecss = Global.g_CurEpubBook.GetStyleCSS(cssPath);
				}
			}
		} else if (localName.equalsIgnoreCase("style")) {
			styleInHtml.delete(0, styleInHtml.length());// = new StringBuilder(styleInHtml.length());
		}

		else if (localName.equalsIgnoreCase("body")) {
			String classid = attributes.getValue("class");
			if (classid != null) {
				// _key = "body." + classid;
				String _key = new StringBuilder(30).append("body.")
						.append(classid).toString();
				HashMap<String, String> css = stylecss.get(_key);
				if (css != null) {
					html.SetBodyStyleCSS(css);
					Global.g_FontMgr.SetStyleCSS(css);
				}
			}
		}

		else if (localName.equalsIgnoreCase("span")) 
		{
		
			String classid = attributes.getValue("class");
			String style = attributes.getValue("style");

			if (classid != null) 
			{
					// String _key = localName + "." + classid;
				String _key = new StringBuilder(30).append(localName)
						.append(".").append(classid).toString();

				HashMap<String, String> css = stylecss.get(_key);
				if (css != null)
					applyStyle(css, style);
				else if (style != null)
					applyStyle(null, style);
				else
					HasCssQue.add("N");// 여기
			}
			else if (style != null)// class는 없는데 style은 있는 경우
			{
				applyStyle(null, style);
			}
			else
				HasCssQue.add("N");

			if (_paragraph == null) {
				_paragraph = new Paragraph();
				text.fontIdx = -1;
				text.szTextcolor = "black";
			}

		}

		else if (localName.equalsIgnoreCase("p")
				|| localName.equalsIgnoreCase("h1")
				|| localName.equalsIgnoreCase("h2")
				|| localName.equalsIgnoreCase("h3")) {
			if (_paragraph == null)
				_paragraph = new Paragraph();

			// text.content = "";
			text.fontIdx = -1;
			text.szTextcolor = "black";
		

			String classid = attributes.getValue("class");
			String style = attributes.getValue("style");
		
			if (classid != null) {
				// String _key = localName + "." + classid;
				String _key = new StringBuilder(30).append(localName)
						.append(".").append(classid).toString();
				HashMap<String, String> css = stylecss.get(_key);
				// NSLog(@"p에 들어옴 %@ css :%d style:[%@]",_key, css, style);
				if (css != null)
					applyStyle(css, style);
				else if (style != null)
					applyStyle(null, style);
				else
					HasCssQue.add("N");
			} else if (style != null) // class는 없는데 style은 있는 경우
				applyStyle(null, style);
			else {// 클래스 아이디가 없어도 css가 먹는 경우

				String _key = localName;
				HashMap<String, String> css = stylecss.get(_key);
				if (css != null)
					applyStyle(css, style);
				else
					HasCssQue.add("N");
			}

			String paraid = attributes.getValue("id");
			if (paraid != null) {
				_paragraph.paraId = paraid;
			}
			
		} else if (localName.equalsIgnoreCase("img")
				|| localName.equalsIgnoreCase("audio")
				|| localName.equalsIgnoreCase("video")) {
			String srcfile = attributes.getValue("src");

			if (srcfile != null) {
				boolean bCreate = false;

				if (_paragraph != null) {
					if (text.fontIdx == -1)
						text.fontIdx = 0;

					_paragraph.AddText(text, textContext);
					textContext.delete(0, textContext.length());// = new StringBuilder(textContext.length());
					paragraphs.add(_paragraph);
					_paragraph = null;
				}

				if (_paragraph == null) {
					_paragraph = new Paragraph();
					bCreate = true;
				}

				text.szTextcolor = null;

				if (localName.equalsIgnoreCase("img")) {
					String imgPath = Util.CheckFolderValidate(GetFullFolder()
							+ srcfile);
					_paragraph.objFilename = imgPath;
					_paragraph.objType = 1;
				} else if (localName.equalsIgnoreCase("audio")) {
					_paragraph.objFilename = srcfile;
					_paragraph.objType = 2;
				} else if (localName.equalsIgnoreCase("video")) {
					_paragraph.objFilename = srcfile;
					_paragraph.objType = 3;
				}
				if (bCreate) {
					paragraphs.add(_paragraph);
					_paragraph = null;
				}
			}
		} else if (localName.equalsIgnoreCase("head")) {
			isInHeadTag = true;
		}
	}
}
