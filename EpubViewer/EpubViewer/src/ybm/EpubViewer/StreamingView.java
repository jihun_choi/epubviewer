package ybm.EpubViewer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.client.methods.HttpGet;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;


public class StreamingView
{
	static Context m_context;
	static Activity m_activity;
	
    //static MediaPlayer m_mp;
    static int m_totalBytesRead = 0;
    static int m_totalFileLength = 0;
    static File m_filedown = null;
    static File m_fileplay = null;
    static int m_curPos = 0;
    
    static Thread m_updateProgress = null;
    static Thread m_copyFile = null;
    
    static boolean m_isPlaying = false;
    static int m_playerState = 0;
    
    static Handler m_handler;
    static File tempfile = null;
    
    static String m_completePath = null; 
    static boolean m_fileExist = false;
	
	public static View OpenStreamingMenu(Context context, Handler handler, String url, String completeFileName)
	{	
		Activity activity = (Activity)context;
		m_context = context;
		m_activity = activity;
		
		final View m_topbar_menu;
		
		final LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		final RelativeLayout back = (RelativeLayout)activity.findViewById(R.id.MM_rel_back);
		
		if(Global.g_curMp3Player != null){
			//Global.g_curMp3Player.pause();		
			stopThread();
			Global.g_curMp3Player.stop();
			Global.g_curMp3Player.release();
			Global.g_curMp3Player = null;
		    m_updateProgress = null;
		    m_copyFile = null;
	
		}

		m_completePath = m_activity.getFilesDir().getAbsolutePath() + "/" + completeFileName + ".mp3";
		
		if(Global.g_curMp3Bar == null){
			m_topbar_menu = inflater.inflate(R.layout.streaming, null);
			back.addView(m_topbar_menu);
			Global.g_curMp3Bar = m_topbar_menu;
			
			Global.g_curMp3SeekBar = (SeekBar)m_activity.findViewById(R.id.ST_seekbar);
			Global.g_curMp3PlayButton = (Button)m_activity.findViewById(R.id.ST_button_play);
			Global.g_curMp3RemoveButton = (Button)m_activity.findViewById(R.id.ST_button_remove);
		}
		else
			m_topbar_menu = Global.g_curMp3Bar;
		
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)m_topbar_menu.getLayoutParams();
		params.topMargin = 30;
		params.leftMargin = 30;
		
    	m_handler = handler;
        Global.g_curMp3Player = null;
		
        ConnectHtml(url);
        
        // final Button button_remove = (Button)m_activity.findViewById(R.id.ST_button_remove);
        Global.g_curMp3RemoveButton.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					Global.g_curMp3RemoveButton.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_x_b));
				}
				else{
					stopThread();
					Global.g_curMp3Bar.clearFocus();
					back.removeView(Global.g_curMp3Bar);
					Global.g_curMp3Bar = null;			
				}
				
				return false;
			}
		});
        
    //final Button button_play = (Button)m_activity.findViewById(R.id.ST_button_play);
        
        // mp3 ÇÃ·¹ÀÌ ¹öÆ°À» ´©¸£ÀÚ¸¶ÀÚ ½ºÆ®¸®¹ÖÀ» ½ÃÀÛÇÔ.
        Global.g_curMp3PlayButton.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_stop));
		m_playerState = 0;
		m_isPlaying = true;
        copyfile();
        updateProgress();
        m_copyFile.start();
        m_updateProgress.start();

    	
        Global.g_curMp3PlayButton.setOnTouchListener(new View.OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					if(m_isPlaying == false){
						Global.g_curMp3PlayButton.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_play_b));
					}
					else{
						Global.g_curMp3PlayButton.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_stop_b));
					}
				}
				else{
					if(m_isPlaying == false){
						Global.g_curMp3PlayButton.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_stop));
						m_playerState = 0;
						m_isPlaying = true;
				        copyfile();
				        updateProgress();
				        if(m_totalFileLength < m_totalBytesRead){
				        	Global.g_curMp3Player.seekTo(m_curPos);								
				        	Global.g_curMp3Player.start();
				        }
				        else
				        	m_copyFile.start();

				        m_updateProgress.start();
					}
					else{
						Global.g_curMp3PlayButton.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_play));
						m_isPlaying = false;
						if(Global.g_curMp3Player != null && Global.g_curMp3Player.isPlaying())
							Global.g_curMp3Player.pause();
					}
					
				}
				
				return false;
			}
		});
    	
		// final SeekBar bar = (SeekBar)m_activity.findViewById(R.id.ST_seekbar);
		Global.g_curMp3SeekBar.setMax(400);
		Global.g_curMp3SeekBar.setProgress(0);
		Global.g_curMp3SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				if(Global.g_curMp3Player != null){
					int prog = seekBar.getProgress();
					int t = (int)((double)prog * (double)Global.g_curMp3Player.getDuration() /400.0 );
					m_curPos = t;
					Global.g_curMp3Player.seekTo(t);
				}
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				
			}
		});
        
		return m_topbar_menu;
	}
	
	public static void stopThread()
	{
    	m_playerState = 3;
    	if(Global.g_curMp3Player != null && Global.g_curMp3Player.isPlaying()){
    		Global.g_curMp3Player.pause();
    		//m_mp.release();
    	}
    	
    	m_isPlaying = false;
    	m_curPos = 0;
        m_totalBytesRead = 0;
        m_totalFileLength = 0;
    	
    	if(m_filedown != null)
    		m_filedown.delete();
    	
    	if(m_fileplay != null)
    		m_fileplay.delete();
    	
        m_filedown = null;
        m_fileplay = null;
	}
	
    public static void ConnectHtml(final String fileUrl)
    {
    	URL myFileUrl = null;
    	HttpGet httpRequest = null;
    	final Thread play;
    	
    	try {
    		myFileUrl = new URL(fileUrl);
    	}
    	catch(MalformedURLException e){
    		e.printStackTrace();
    	}
    	
    	try{
    		httpRequest = new HttpGet(myFileUrl.toURI());
    	}
    	catch (URISyntaxException e){
    		e.printStackTrace();
    	}
    	
    	try{		
    		m_filedown = new File(m_completePath);
    		if(m_filedown.exists()){
    			m_fileExist = true;
    			m_totalBytesRead = 300000;
    			m_totalFileLength = 300000;
    			return;
    		}
    		
    		URLConnection con = myFileUrl.openConnection();
    		con.connect();
    		final InputStream stream = con.getInputStream();    		
    		if(stream == null){
    			Log.i("debug", "Unable to create InputStream");
    			return;
    		}
    		
    		m_totalFileLength = con.getContentLength();

    		m_filedown = File.createTempFile("down_", ".tmp");
    		m_filedown.deleteOnExit();
    		final FileOutputStream out = new FileOutputStream(m_filedown);
    		
    		Thread download = new Thread(new Runnable(){
    			public void run(){
    	    		byte []buf = new byte[16384];
    	    		int numread;
    	    		m_totalBytesRead = 0;
    	    		try{
			    		do{
			    			numread = stream.read(buf);
			    			if(numread <= 0){
			    				break;
			    			}
			    			else{
			    				out.write(buf, 0, numread);
			    				m_totalBytesRead += numread;
			    			}

			    		}while(m_totalBytesRead < m_totalFileLength);
			    		
			    		stream.close();
			    		
			    		
			    		// TODO : Ã©ÅÍº° mp3 ÆÄÀÏ·Î ÀúÀå.
						File completeFile = new File(m_completePath);
						
		    			BufferedInputStream reader = null;
		    			BufferedOutputStream writer = null;
						reader = new BufferedInputStream( new FileInputStream(m_filedown) );
						writer = new BufferedOutputStream( new FileOutputStream(completeFile, false));
					
				        byte[]  buff = new byte[8192];
				        int numChars;
				        while ( (numChars = reader.read(  buff, 0, buff.length ) ) != -1) {
				        	writer.write( buff, 0, numChars );
		      		    }
						
                    	writer.close();
                        reader.close();
    	    		}
    	    		catch(IOException e){
    	    			e.printStackTrace();
    	    		}    	    		
    			}
    		});
    		download.start();
    		
    	}
    	catch (IOException e)
    	{
    		e.printStackTrace();
    	}

    }
    
    
    public static void copyfile()
    {
    	m_copyFile = new Thread(new Runnable(){
    		public void run(){
    			
    			while(m_totalFileLength >= m_totalBytesRead && m_playerState < 3){
	    			
    				if((Global.g_curMp3Player != null && Global.g_curMp3Player.getDuration() - Global.g_curMp3Player.getCurrentPosition() < 10000) ||
    						(Global.g_curMp3Player == null && m_totalBytesRead > 100000) || (m_totalFileLength == m_totalBytesRead)){
		    			BufferedInputStream reader = null;
		    			BufferedOutputStream writer = null;
		    			
		    			tempfile = null;
		    			
		    			if(m_fileExist == false){
		    			try {
		    				
		    				tempfile = File.createTempFile("play_", ".tmp");
		    				tempfile.deleteOnExit();
							reader = new BufferedInputStream( new FileInputStream(m_filedown) );
							writer = new BufferedOutputStream( new FileOutputStream(tempfile, false));
						
					        byte[]  buff = new byte[8192];
					        int numChars;
					        while ( (numChars = reader.read(  buff, 0, buff.length ) ) != -1) {
					        	writer.write( buff, 0, numChars );
			      		    }
			            } catch( IOException ex ) {
							ex.printStackTrace();
			            } finally {
			                try {
			                    if ( reader != null ){                    	
			                    	writer.close();
			                        reader.close();
			                    }
			                } catch( IOException ex ){
							    ex.printStackTrace(); 
							}
			            }
		    			}
		    			else{
		    				tempfile = m_filedown;
		    			}
			            
			            
			            if(m_totalFileLength <= m_totalBytesRead)
			            	m_totalBytesRead = m_totalFileLength+1;
			            		            
			            
			            Thread play = new Thread(new Runnable(){
			    			public void run(){
								
			    				m_playerState = 1;
								if(Global.g_curMp3Player != null && Global.g_curMp3Player.isPlaying()){								
									m_curPos = Global.g_curMp3Player.getCurrentPosition();
									Global.g_curMp3Player.pause();
									//m_mp.release();
								}
								
								final MediaPlayer player = new MediaPlayer();
								
								try{
									
									FileInputStream playin = new FileInputStream(tempfile.getAbsolutePath());
									player.reset();
									player.setDataSource(playin.getFD());
									player.prepare();
								}
								catch(IOException e){
									e.printStackTrace();
								}
								catch(IllegalStateException e){
									e.printStackTrace();
								}
								
								player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
									public void onCompletion(MediaPlayer mp) {
										m_playerState = 0;
										m_isPlaying = false;										
										//Global.g_curMp3PlayButton = (Button)m_activity.findViewById(R.id.ST_button_play);
										Global.g_curMp3PlayButton.setBackgroundDrawable(m_activity.getResources().getDrawable(R.drawable.btn_play));
										//final SeekBar bar = (SeekBar)m_activity.findViewById(R.id.ST_seekbar);
										Global.g_curMp3SeekBar.setProgress(0);
										//bar.setProgress(0);
										
										player.pause();
										player.seekTo(0);
										m_curPos = 0;
									}
								});
								
								Global.g_curMp3Player = player;
								
								Global.g_curMp3Player.seekTo(m_curPos);
								Global.g_curMp3Player.start();
								
								m_playerState = 0;
								
								m_fileplay.delete();
								m_fileplay = tempfile;
			    			}
			    		});
			            play.start();
			            
			            m_fileplay = tempfile;
		            
    				}
    				
    				try {
						Thread.sleep(100);
					} catch (InterruptedException e) {						
						e.printStackTrace();
						break;
					}
    				
    			}
    		}
    	});
    }
    
    
    public static void updateProgress()
    {
    	m_updateProgress = new Thread (new Runnable() {
			public void run(){
				
				while(m_isPlaying == true && m_playerState < 3){			
										
					try{
						Thread.sleep(1000);
					}
					catch(InterruptedException e){
						break;
					}
					
					if(Global.g_curMp3Player != null && Global.g_curMp3Player.isPlaying()) {
						m_handler.post(new Runnable() {
							public void run(){
								
								if(m_playerState == 0){
									m_curPos = Global.g_curMp3Player.getCurrentPosition();
									double t = (Global.g_curMp3Player.getCurrentPosition() * 400) / 
										(Global.g_curMp3Player.getDuration());
									
									//final SeekBar bar = (SeekBar)m_activity.findViewById(R.id.ST_seekbar);
									//bar.setProgress((int)t);
									Global.g_curMp3SeekBar.setProgress((int)t);
								}
							}
						});
						
					}
					
				}
			}
		}); 
    	
    }
}